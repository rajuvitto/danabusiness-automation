package testCases;

import java.io.File;
import java.util.Scanner;
import java.io.FileNotFoundException;
import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import io.restassured.RestAssured;
import io.restassured.http.Method;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import testBase.TestBase;

public class TestSuite_AboutMe extends TestBase {
	final static Logger logger = Logger.getLogger(TestSuite_AboutMe.class);
	String baseUrl = "";
	String responseStatusSuccess ="";
	String responseStatusFailed = "";
	String responseStatusUserNotLoggedIn = "USER_NOT_LOGGED_IN";
	long userId;
	Integer userBalance;
	String requestId, sessionId, userPhone, userPassword, userEmail, userName = "";
	String endPointAboutMe = "/member/user/viewProfile";
	File getUserId, getSessionId, getRequestId, getUserPhone, getUserPassword, getUserEmail, getUserBalance, getUserName;
	Scanner userIdReader, sessionIdReader, requestIdReader, userPhoneReader, userPasswordReader, userEmailReader, userBalanceReader, userNameReader;
	
	@BeforeClass
	@Parameters({"baseUrl", "responseStatusSuccess", "responseStatusFailed"})
	void startTest(String definedBaseUrl, String definedResponseStatusSuccess, String definedResponseStatusFailed) {
		baseUrl = definedBaseUrl;
		responseStatusSuccess = definedResponseStatusSuccess;
		responseStatusFailed = definedResponseStatusFailed;
		logger.info("[ --- Started TestSuite_AboutMe --- ]");
		
		try {
			getUserId = new File("./assets/generated-data/userId.txt");
			getSessionId = new File("./assets/generated-data/sessionId.txt");
			getUserPhone = new File("./assets/generated-data/userPhone.txt");
			getUserPassword = new File("./assets/generated-data/userPassword.txt");
			getUserEmail = new File("./assets/generated-data/userEmail.txt");
			getUserBalance = new File("./assets/generated-data/userBalance.txt");
			getUserName = new File("./assets/generated-data/userName.txt");
			userIdReader = new Scanner(getUserId);
			sessionIdReader = new Scanner(getSessionId);
			userPhoneReader = new Scanner(getUserPhone);
			userPasswordReader = new Scanner(getUserPassword);
			userEmailReader = new Scanner(getUserEmail);
			userBalanceReader = new Scanner(getUserBalance);
			userNameReader = new Scanner(getUserName);
			while (userIdReader.hasNextLine()) {
				String data = userIdReader.nextLine();
				userId = Long.parseLong(data);
			}
			
			while (sessionIdReader.hasNextLine()) {
				String data = sessionIdReader.nextLine();
				sessionId = data;
			}
			
			while (userPhoneReader.hasNextLine()) {
				String data = userPhoneReader.nextLine();
				userPhone = data;
			}
			
			while (userPasswordReader.hasNextLine()) {
				String data = userPasswordReader.nextLine();
				userPassword = data;
			}
			
			while (userEmailReader.hasNextLine()) {
				String data = userEmailReader.nextLine();
				userEmail = data;
			}
			
			while (userBalanceReader.hasNextLine()) {
				String data = userBalanceReader.nextLine();
				userBalance = Integer.parseInt(data);
			}
			
			while (userNameReader.hasNextLine()) {
				String data = userNameReader.nextLine();
				userName = data;
			}
		}catch (FileNotFoundException e) {
			System.out.println("An error occurred.");
		      e.printStackTrace();
		}
	}
	
	@Test
	@Parameters({"testCaseId"})
	void AboutMe_TSC_001(String testId) throws InterruptedException, ParseException{
		
		logger.info("[ --- Started "+testId+" --- ]");
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.GET, endPointAboutMe);
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		String responseBody = response.getBody().asString();
		String responseCode = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Response Body : "+responseBody);
		logger.info("Response Code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION
		if(testId.equals("AboutMe_TSC_001")) {
			Assert.assertEquals(responseDescription, responseStatusSuccess);
			Assert.assertEquals(responseCode, "300");
			
			JSONObject tempData = new JSONObject();
			tempData = (JSONObject) json.get("userData");
			Assert.assertEquals(tempData.get("name").toString(), userName);
			Assert.assertEquals(tempData.get("phone").toString(), userPhone);
			Assert.assertEquals(tempData.get("email").toString(), userEmail);
			Assert.assertEquals(tempData.get("balance").toString(), userBalance.toString());
		}else if(testId.equals("AboutMe_TSC_002")) {
			Assert.assertEquals(responseDescription, responseStatusUserNotLoggedIn);
			Assert.assertEquals(responseCode, "301");
		}
		logger.info("[ --- Finished "+testId+" --- ]");
		Thread.sleep(5);
	}
	
	@AfterClass
	void tearDown() {
		logger.info("[ --- Finished TestSuite_AboutMe --- ]");
		System.out.println();
	}
}