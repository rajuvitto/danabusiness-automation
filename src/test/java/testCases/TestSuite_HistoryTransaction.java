package testCases;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import testBase.TestBase;

public class TestSuite_HistoryTransaction extends TestBase {
	final static Logger logger = Logger.getLogger(TestSuite_HistoryTransaction.class);
	String baseUrl = "";
	String responseStatusSuccess ="";
	String responseStatusFailed = "";
	String endpointHistoryTransaction = "/payment/trx/history";
	long userId, transactionHistoryId;
	String sessionId = "";
	File getUserId, getSessionId, getTransactionHistoryId;
	Scanner userIdReader, sessionIdReader, transactionHistoryReader;
	
	
	@BeforeClass
	@Parameters({"baseUrl", "responseStatusSuccess", "responseStatusFailed"})
	void startTest(String definedBaseUrl, String definedResponseStatusSuccess, String definedResponseStatusFailed) {
		baseUrl = definedBaseUrl;
		responseStatusSuccess = definedResponseStatusSuccess;
		responseStatusFailed = definedResponseStatusFailed;
		logger.info("[ --- Started TestSuite_HistoryTransaction --- ]");
		
		try {
			getUserId = new File("./assets/generated-data/userId.txt");
			getSessionId = new File("./assets/generated-data/sessionId.txt");
			getTransactionHistoryId = new File("./assets/generated-data/transactionHistoryId.txt");
			userIdReader = new Scanner(getUserId);
			sessionIdReader = new Scanner(getSessionId);
			transactionHistoryReader = new Scanner(getTransactionHistoryId);
			
			while (userIdReader.hasNextLine()) {
				String data = userIdReader.nextLine();
				userId = Long.parseLong(data);
			}
			
			while (sessionIdReader.hasNextLine()) {
				String data = sessionIdReader.nextLine();
				sessionId = data;
			}
			
			while (transactionHistoryReader.hasNextLine()) {
				String data = transactionHistoryReader.nextLine();
				transactionHistoryId = Long.parseLong(data);
			}
		}catch (FileNotFoundException e) {
			System.out.println("An error occurred.");
		      e.printStackTrace();
		}
	}
	
	@SuppressWarnings("unchecked")
	@Parameters({"testCaseId"})
	@Test
	void HistoryTransaction_TSC_001_002(String testId) throws InterruptedException, ParseException{
//		- User already registered
//		- User logged in
		
		logger.info("[ --- Started HistoryTransaction_TSC_"+testId+" --- ]");	
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.GET, endpointHistoryTransaction);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		String responseBody = response.getBody().asString();
		String responseCode = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Response Body : " + responseBody);
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		
		
		// ASSERTION
		Assert.assertEquals(responseDescription, "Success");
		Assert.assertEquals(responseCode.toString(), "300");

		Thread.sleep(5);
		logger.info("[ --- Finished HistoryTransaction_TSC_"+testId+" --- ]");
	}
	
	@SuppressWarnings("unchecked")
	@Parameters({"testCaseId"})
	@Test
	void HistoryTransaction_TSC_003_004(String testId) throws InterruptedException, ParseException{
//		- User already registered
//		- User logged in
		
		logger.info("[ --- Started HistoryTransaction_TSC_"+testId+" --- ]");	
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.GET, endpointHistoryTransaction);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		String responseBody = response.getBody().asString();
		String responseCode = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Response Body : " + responseBody);
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		
		
		// ASSERTION
		Assert.assertEquals(responseDescription, "USER_NOT_LOGGED_IN");
		Assert.assertEquals(responseCode.toString(), "301");

		Thread.sleep(5);
		logger.info("[ --- Finished HistoryTransaction_TSC_"+testId+" --- ]");
	}
	
	
	
	@AfterClass
	void tearDown() {
		logger.info("[ --- Finished TestSuite_HistoryTransaction --- ]");
		System.out.println();
	}
}
