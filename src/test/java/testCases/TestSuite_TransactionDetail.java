package testCases;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import io.restassured.RestAssured;
import io.restassured.http.Method;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import testBase.TestBase;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class TestSuite_TransactionDetail extends TestBase {
	final static Logger logger = Logger.getLogger(TestSuite_TransactionDetail.class);
	String baseUrl = "";
	String responseStatusSuccess ="";
	String responseStatusFailed = "";
	String endpointTransactionDetail = "/payment/trx/detail";
	File getTransactionHistoryId, getSessionId, getUserId;
	Scanner transactionHistoryIdReader, sessionIdReader, userIdReader;;
	String userId = "";
	Integer transactionHistoryId;
	String sessionId = "";
	String randomTransactionHistoryId = getNegativeRandomNumber();
	String responseStatusTransactionNotComplete = "Transaction not completed";
	String responseStatusTransactionNotFound = "Transaction not found";
	String responseStatusUserNotLoggedIn = "USER_NOT_LOGGED_IN";

	@BeforeClass
	@Parameters({"baseUrl", "responseStatusSuccess", "responseStatusFailed"})
	void startTest(String definedBaseUrl, @Optional("Optional Parameter")  String definedResponseStatusSuccess, String definedResponseStatusFailed) {
		baseUrl = definedBaseUrl;
		responseStatusSuccess = definedResponseStatusSuccess;
		responseStatusFailed = definedResponseStatusFailed;
		logger.info("[ --- Started TransactionDetail_TSC_001 --- ]");

		try {
			getTransactionHistoryId = new File("./assets/generated-data/transactionHistoryId.txt");
			getSessionId = new File("./assets/generated-data/sessionId.txt");
			getUserId = new File("./assets/generated-data/userId.txt");

			transactionHistoryIdReader = new Scanner(getTransactionHistoryId);
			sessionIdReader = new Scanner(getSessionId);
			userIdReader = new Scanner(getUserId);
			
			while (transactionHistoryIdReader.hasNextLine()) {
				String data = transactionHistoryIdReader.nextLine();
				transactionHistoryId = Integer.parseInt(data);
			}
			
			while (sessionIdReader.hasNextLine()) {
				String data = sessionIdReader.nextLine();
				sessionId = data;
			}
			
			while (userIdReader.hasNextLine()) {
				String data = userIdReader.nextLine();
				userId = data;
			}

		}catch (FileNotFoundException e) {
			System.out.println("An error occurred.");
		      e.printStackTrace();
		}
	}
	
	@Test
	@Parameters({"testCaseId"})
	void TransactionDetail(String idTestCase) throws InterruptedException, ParseException{
		
		logger.info("[ --- Started "+idTestCase+" --- ]");	
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		httpRequest.cookie("JSESSIONID", sessionId);
		if(idTestCase.equals("TransactionDetail_TSC_001") || idTestCase.equals("TransactionDetail_TSC_002") || idTestCase.equals("TransactionDetail_TSC_004") || idTestCase.equals("TransactionDetail_TSC_005")) {
			httpRequest.params("transactionHistoryId", transactionHistoryId);
			logger.info("Transaction History ID : " + transactionHistoryId);
		}else if(idTestCase.equals("TransactionDetail_TSC_003") || idTestCase.equals("TransactionDetail_TSC_006")) {
			httpRequest.params("transactionHistoryId", randomTransactionHistoryId);
			logger.info("Transaction History ID : " + randomTransactionHistoryId);
		}
		
		httpRequest.header("Content-Type", "application/json");
		response = httpRequest.request(Method.GET, endpointTransactionDetail);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		String responseBody = response.getBody().asString();
		String responseCode = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Response Body : " + responseBody);
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);

		if(idTestCase.equals("TransactionDetail_TSC_001")) {
			// ASSERTION
			Assert.assertEquals(responseDescription, responseStatusSuccess);
			Assert.assertEquals(responseCode, "300");
			JSONObject tempData = new JSONObject();
			tempData = (JSONObject) json.get("transactionHistory");
			Assert.assertEquals(tempData.get("totalPayment").toString(), "25000");
			if(tempData.get("totalPayment").toString().equals("25000")) {
				logger.info("Check Total Payment : " + "SUCCESS");
			}else {
				logger.info("Check Total Payment : " + "FAILED");
			}
			logger.info("Total Payment : Rp."+tempData.get("totalPayment").toString());
			
			Assert.assertEquals(tempData.get("merchantId").toString(), "1");
			if(tempData.get("merchantId").toString().equals("1")) {
				logger.info("Check Merchant ID : " + "SUCCESS");
			}else {
				logger.info("Check Merchant ID : " + "FAILED");
			}
			logger.info("Merchant ID : "+tempData.get("merchantId").toString());
			
			Assert.assertEquals(tempData.get("userId").toString(), userId);
			if(tempData.get("userId").toString().equals(userId)) {
				logger.info("Check User ID : " + "SUCCESS");
			}else {
				logger.info("Check User ID : " + "FAILED");
			}
			logger.info("User ID : "+tempData.get("userId").toString());
			
			Assert.assertEquals(tempData.get("merchantName").toString(), "Fremilt");
			if(tempData.get("merchantName").toString().equals("Fremilt")) {
				logger.info("Check Merchant Name : " + "SUCCESS");
			}else {
				logger.info("Check Merchant Name : " + "FAILED");
			}
			logger.info("Merchant Name : "+tempData.get("merchantName").toString());
			
			Assert.assertEquals(tempData.get("isCompleted").toString(), "true");
			if(tempData.get("merchantName").toString().equals("Fremilt")) {
				logger.info("Check Is Transaction Complete : " + "SUCCESS");
			}else {
				logger.info("Check Is Transaction Complete : " + "FAILED");
			}
			logger.info("Transaction Complete Status : "+tempData.get("isCompleted").toString());
			
			JSONObject tempDataDetailTrx = new JSONObject();
			tempDataDetailTrx = (JSONObject) json.get("transactionDetail");
			Assert.assertEquals(tempDataDetailTrx.get("paymentMethod").toString(), "Dana");
			if(tempDataDetailTrx.get("paymentMethod").toString().equals("Dana")) {
				logger.info("Check Payment Method : " + "SUCCESS");
			}else {
				logger.info("Check Payment Method : " + "FAILED");
			}
			logger.info("Payment Method : "+tempDataDetailTrx.get("paymentMethod").toString());
			
		
			if(responseCode.toString().equals("300")) {
				logger.info("=== TRANSACTION DETAIL SUCCESS ===");
				
			}else {
				logger.info("=== TRANSACTION DETAIL FAILED ===");
			}
		}else if(idTestCase.equals("TransactionDetail_TSC_002")) {
			Assert.assertEquals(responseDescription, responseStatusTransactionNotComplete);
			Assert.assertEquals(responseCode, "102");
			
			if(responseCode.toString().equals("102")) {
				logger.info("=== TRANSACTION DETAIL SUCCESS ===");
				
			}else {
				logger.info("=== TRANSACTION DETAIL FAILED ===");
			}
		}else if(idTestCase.equals("TransactionDetail_TSC_003")){
			Assert.assertEquals(responseDescription, responseStatusTransactionNotFound);
			Assert.assertEquals(responseCode, "101");
		}else if(idTestCase.equals("TransactionDetail_TSC_004") || idTestCase.equals("TransactionDetail_TSC_005") || idTestCase.equals("TransactionDetail_TSC_006")){
			Assert.assertEquals(responseDescription, responseStatusUserNotLoggedIn);
			Assert.assertEquals(responseCode, "301");
		}
		
		
		Thread.sleep(5);
		logger.info("[ --- Finished "+idTestCase+" --- ]");	
	}
	
	@AfterClass
	void tearDown() {
		System.out.println();
	}
}