package testCases;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import testBase.TestBase;

public class TestSuite_ConfirmPayment extends TestBase{
	final static Logger logger = Logger.getLogger(TestSuite_ConfirmPayment.class);
	String baseUrl = "";
	String responseStatusSuccess ="";
	String responseStatusFailed = "";
	String endpointConfirmPayment = "/payment/pay/confirm";
	String endpointViewProfile = "/member/user/viewProfile";
	String endpointGetUser = "/user/viewProfile";
	String endpointLogout = "/member/user/logout";
	String endpointTransaction = "/trx/detail";
	long userId, transactionHistoryId, transactionHistoryId2;
	String sessionId = "";
	File getUserId, getSessionId, getTransactionHistoryId, getTransactionHistoryId2, getRequestId, getRequestId2, getUserBalanceAfterTopup;
	Scanner userIdReader, sessionIdReader, transactionHistoryReader,  transactionHistoryReader2, requestIdReader, requestIdReader2, userBalanceAfterTopupReader;
	String saveRequestId="";
	String requestId="";
	String requestId2="";
	Boolean status = false;
	Integer userBalanceAfterTopup = 0;
	Integer balanceAfterConfirm  = 0;
	Integer balanceBefore  = 0;
	Integer total = 0;
	
	@BeforeClass
	@Parameters({"baseUrl", "responseStatusSuccess", "responseStatusFailed"})
	void startTest(String definedBaseUrl, @Optional("Optional Parameter")  String definedResponseStatusSuccess, String definedResponseStatusFailed) throws InterruptedException, ParseException{
		baseUrl = definedBaseUrl;
		responseStatusSuccess = definedResponseStatusSuccess;
		responseStatusFailed = definedResponseStatusFailed;
		logger.info("[ --- Started TestSuite_ConfirmPayment --- ]");
		
		try {
			getUserId = new File("./assets/generated-data/userId.txt");
			getSessionId = new File("./assets/generated-data/sessionId.txt");
			getTransactionHistoryId = new File("./assets/generated-data/transactionHistoryId.txt");
			getTransactionHistoryId2 = new File("./assets/generated-data/transactionHistoryId2.txt");
			getRequestId = new File("./assets/generated-data/requestId.txt");
			getUserBalanceAfterTopup = new File("./assets/generated-data/userBalanceAfterTopup.txt");
			userIdReader = new Scanner(getUserId);
			sessionIdReader = new Scanner(getSessionId);
			transactionHistoryReader = new Scanner(getTransactionHistoryId);
			transactionHistoryReader2 = new Scanner(getTransactionHistoryId2);
			requestIdReader = new Scanner(getRequestId);
			userBalanceAfterTopupReader = new Scanner(getUserBalanceAfterTopup);
			
			while (userIdReader.hasNextLine()) {
				String data = userIdReader.nextLine();
				userId = Long.parseLong(data);
			}
			
			while (sessionIdReader.hasNextLine()) {
				String data = sessionIdReader.nextLine();
				sessionId = data;
			}
			
			while (transactionHistoryReader.hasNextLine()) {
				String data = transactionHistoryReader.nextLine();
				transactionHistoryId = Long.parseLong(data);
			}
			
			while (transactionHistoryReader2.hasNextLine()) {
				String data = transactionHistoryReader2.nextLine();
				transactionHistoryId2 = Long.parseLong(data);
			}
			
			while (requestIdReader.hasNextLine()) {
				String data = requestIdReader.nextLine();
				requestId = data;
			}
			
			while (userBalanceAfterTopupReader.hasNextLine()) {
				String data = userBalanceAfterTopupReader.nextLine();
				userBalanceAfterTopup = Integer.parseInt(data);
			}
			
		}catch (FileNotFoundException e) {
			System.out.println("An error occurred.");
		      e.printStackTrace();
		}
		
		RestAssured.baseURI = "https://dana-bussiness-member.herokuapp.com/v1";
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		JSONObject RequestParams = new JSONObject();
		RequestParams = new JSONObject();
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.params("id", String.valueOf(userId));
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointGetUser);
		
		// MANIPULATING RESPONSE DATA
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		JSONObject tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceBefore = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance before : " + balanceBefore);
		
	}
	
	@SuppressWarnings("unchecked")
	@Parameters({"testCaseId"})
	@Test
	void ConfirmPayment(String testId)throws InterruptedException, ParseException{
//		requestId is filled and unique
		logger.info("[ --- Started ConfirmPayment--- ]");
		
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("transactionHistoryId", transactionHistoryId2);
		RequestParams.put("paymentMethod", "Dana");
		RequestParams.put("requestId", saveRequestId=generateRequestId());
		
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointConfirmPayment);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		JSONObject tempData = new JSONObject();
		tempData = (JSONObject) json.get("transactionHistory");
		
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		String responseisCompleted = tempData.get("isCompleted").toString();
		String totalPayment = tempData.get("totalPayment").toString();
		
		logger.info("Response Body : " + responseBody);
		logger.info("Response Status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		logger.info("Response isCompleted : " + responseisCompleted);
		
		
		if(responseStatus.toString().equals("300")) {
			// ASSERTION
			Assert.assertEquals(responseDescription, responseStatusSuccess);
			Assert.assertEquals(responseStatus, "300");
			Assert.assertEquals(responseisCompleted, "true");
			
			// Save request id to file requestId.txt
			try {
				FileWriter generateRequestId = new FileWriter("./assets/generated-data/requestId2.txt");
				generateRequestId.write(saveRequestId);
				generateRequestId.close();
			} catch (IOException e) {
			    System.err.format("IOException: %s%n", e);
			}
			
			RestAssured.baseURI = baseUrl;
			httpRequest = RestAssured.given();
			
			// PROVIDE JSON DATA AT REQUEST BODY
			RequestParams = new JSONObject();
			RequestParams.put("userId", userId);
			
			// SET COOKIE, REQUEST HEADER AND REQUEST BODY
			httpRequest.cookie("JSESSIONID", sessionId);
			httpRequest.header("Content-Type", "application/json");
			httpRequest.body(RequestParams.toJSONString());
			
			// SET REQUEST METHOD AND API ENDPOINT
			response = httpRequest.request(Method.GET, endpointViewProfile);
			
			// MANIPULATING RESPONSE DATA
			parser = new JSONParser();
			json = (JSONObject) parser.parse(response.getBody().asString());
			
			tempData = new JSONObject();
			tempData = (JSONObject) json.get("userData");
			balanceAfterConfirm = Integer.parseInt(tempData.get("balance").toString());
			logger.info("Balance after confirm payment : " + balanceAfterConfirm);
			total = balanceBefore-balanceAfterConfirm;
			Assert.assertEquals((total).toString(), totalPayment.toString());
			Assert.assertEquals(true, balanceAfterConfirm<balanceBefore);
			
			
		}else if(responseStatus.toString().equals("105")) {
			// ASSERTION
			Assert.assertEquals(responseDescription, "Payment refund");
			Assert.assertEquals(responseStatus, "105");
			Assert.assertEquals(responseisCompleted, "true");
						
			// Save request id to file requestId.txt
			try {
				FileWriter generateRequestId = new FileWriter("./assets/generated-data/requestId2.txt");
				generateRequestId.write(saveRequestId);
				generateRequestId.close();
			} catch (IOException e) {
				System.err.format("IOException: %s%n", e);
			}
						
			RestAssured.baseURI = baseUrl;
			httpRequest = RestAssured.given();
						
			// PROVIDE JSON DATA AT REQUEST BODY
			RequestParams = new JSONObject();
			RequestParams.put("userId", userId);
						
			// SET COOKIE, REQUEST HEADER AND REQUEST BODY
			httpRequest.cookie("JSESSIONID", sessionId);
			httpRequest.header("Content-Type", "application/json");
			httpRequest.body(RequestParams.toJSONString());
						
			// SET REQUEST METHOD AND API ENDPOINT
			response = httpRequest.request(Method.GET, endpointViewProfile);
						
			// MANIPULATING RESPONSE DATA
			parser = new JSONParser();
			json = (JSONObject) parser.parse(response.getBody().asString());
						
			tempData = new JSONObject();
			tempData = (JSONObject) json.get("userData");
			balanceAfterConfirm = Integer.parseInt(tempData.get("balance").toString());
			logger.info("Balance after confirm payment : " + balanceAfterConfirm);
			Assert.assertEquals(balanceAfterConfirm, balanceBefore);
		}else if(responseStatus.toString().equals("104")) {
			// ASSERTION
			Assert.assertEquals(responseDescription, "Payment failed");
			Assert.assertEquals(responseStatus, "104");
			Assert.assertEquals(responseisCompleted, "true");
									
			// Save request id to file requestId.txt
			try {
				FileWriter generateRequestId = new FileWriter("./assets/generated-data/requestId2.txt");
				generateRequestId.write(saveRequestId);
				generateRequestId.close();
			} catch (IOException e) {
				System.err.format("IOException: %s%n", e);
			}
									
			RestAssured.baseURI = baseUrl;
			httpRequest = RestAssured.given();
									
			// PROVIDE JSON DATA AT REQUEST BODY
			RequestParams = new JSONObject();
			RequestParams.put("userId", userId);
									
			// SET COOKIE, REQUEST HEADER AND REQUEST BODY
			httpRequest.cookie("JSESSIONID", sessionId);
			httpRequest.header("Content-Type", "application/json");
			httpRequest.body(RequestParams.toJSONString());
									
			// SET REQUEST METHOD AND API ENDPOINT
			response = httpRequest.request(Method.GET, endpointViewProfile);
									
			// MANIPULATING RESPONSE DATA
			parser = new JSONParser();
			json = (JSONObject) parser.parse(response.getBody().asString());
									
			tempData = new JSONObject();
			tempData = (JSONObject) json.get("userData");
			balanceAfterConfirm = Integer.parseInt(tempData.get("balance").toString());
			logger.info("Balance after confirm payment : " + balanceAfterConfirm);
			Assert.assertEquals(balanceAfterConfirm, balanceBefore);
		}
		
		Thread.sleep(5);
		logger.info("[ --- Finished ConfirmPayment--- ]");

	}

	@SuppressWarnings("unchecked")
	@Test
	void ConfirmPayment_TSC_001()throws InterruptedException, ParseException{
//		User already registered
//		User Logged in
//		Enough Balance
//		requestId is filled and unique
//		transaction history id with success transaction
//		Confirm payment under 3 min
		logger.info("[ --- Started ConfirmPayment_TSC_001 --- ]");
		
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("transactionHistoryId", transactionHistoryId);
		RequestParams.put("paymentMethod", "Dana");
		RequestParams.put("requestId", saveRequestId=generateRequestId());
		
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointConfirmPayment);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		JSONObject tempData = new JSONObject();
		tempData = (JSONObject) json.get("transactionHistory");
		
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		String responseisCompleted = tempData.get("isCompleted").toString();
		String totalPayment = tempData.get("totalPayment").toString();
		
		logger.info("Response Body : " + responseBody);
		logger.info("Response Status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		logger.info("Response isCompleted : " + responseisCompleted);
		
		// ASSERTION
		Assert.assertEquals(responseDescription, responseStatusSuccess);
		Assert.assertEquals(responseStatus, "300");
		Assert.assertEquals(responseisCompleted, "true");
		
		if(responseStatus.toString().equals("300")) {
			// Save request id to file requestId.txt
			try {
				FileWriter generateRequestId = new FileWriter("./assets/generated-data/requestId.txt");
				generateRequestId.write(saveRequestId);
				generateRequestId.close();
			} catch (IOException e) {
			    System.err.format("IOException: %s%n", e);
			}
			
			RestAssured.baseURI = baseUrl;
			httpRequest = RestAssured.given();
			
			// PROVIDE JSON DATA AT REQUEST BODY
			RequestParams = new JSONObject();
			RequestParams.put("userId", userId);
			
			// SET COOKIE, REQUEST HEADER AND REQUEST BODY
			httpRequest.cookie("JSESSIONID", sessionId);
			httpRequest.header("Content-Type", "application/json");
			httpRequest.body(RequestParams.toJSONString());
			
			// SET REQUEST METHOD AND API ENDPOINT
			response = httpRequest.request(Method.GET, endpointViewProfile);
			
			// MANIPULATING RESPONSE DATA
			parser = new JSONParser();
			json = (JSONObject) parser.parse(response.getBody().asString());
			
			tempData = new JSONObject();
			tempData = (JSONObject) json.get("userData");
			balanceAfterConfirm = Integer.parseInt(tempData.get("balance").toString());
			logger.info("Balance after confirm payment : " + balanceAfterConfirm);
			total = userBalanceAfterTopup-balanceAfterConfirm;
			Assert.assertEquals((total).toString(), totalPayment.toString());
			Assert.assertEquals(true, balanceAfterConfirm<userBalanceAfterTopup);
			
			//Check transaction in database
			RestAssured.baseURI = "https://danabusiness-payment.herokuapp.com";
			httpRequest = RestAssured.given();
			
			JSONObject RequestParamsCheck = new JSONObject();
			
			httpRequest.params("transactionHistoryId", transactionHistoryId);
			httpRequest.header("Content-Type", "application/json");
			httpRequest.body(RequestParamsCheck.toJSONString());
			response = httpRequest.request(Method.GET, endpointTransaction);
			
			JSONParser parserCheck = new JSONParser();
			JSONObject jsonCheck = (JSONObject) parserCheck.parse(response.getBody().asString());
			tempData = new JSONObject();
			tempData = (JSONObject) jsonCheck.get("transactionHistory");
			
			String responseBodyCheck = response.getBody().asString();
			logger.info("Response Body : " + responseBodyCheck);
			
			String responseCodeCheck = jsonCheck.get("code").toString();
			String responseDescriptionCheck = jsonCheck.get("description").toString();
			String responseTransactionIdCheck = tempData.get("id").toString();
			
			logger.info("Response code : " + responseCodeCheck);
			logger.info("Response Description : " + responseDescriptionCheck);
			
			
			// ASSERTION
			Assert.assertEquals(responseDescriptionCheck, "Success");
			Assert.assertEquals(responseCodeCheck, "100");
			Assert.assertEquals(responseTransactionIdCheck, String.valueOf(transactionHistoryId));
			
		}
		
		Thread.sleep(5);
		logger.info("[ --- Finished ConfirmPayment_TSC_001 --- ]");

	}
	
	@SuppressWarnings("unchecked")
	@Test
	void ConfirmPayment_TSC_002()throws InterruptedException, ParseException{
//		User already registered
//		User Logged in
//		Enough Balance
//		requestId is filled and unique
//		transaction history id with pending transaction
//		Confirm payment under 3 min
		logger.info("[ --- Started ConfirmPayment_TSC_002 --- ]");
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("transactionHistoryId", transactionHistoryId);
		RequestParams.put("paymentMethod", "Dana");
		RequestParams.put("requestId", generateRequestId());
		
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointConfirmPayment);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		JSONObject tempData = new JSONObject();
		tempData = (JSONObject) json.get("transactionHistory");
		
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		String responseisCompleted = tempData.get("isCompleted").toString();
		
		logger.info("Response Body : " + responseBody);
		logger.info("Response Status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		logger.info("Response isCompleted : " + responseisCompleted);
		
		// ASSERTION
		Assert.assertEquals(responseDescription, "Payment refund");
		Assert.assertEquals(responseStatus, "105");
		Assert.assertEquals(responseisCompleted, "true");
		
		if(responseStatus.toString().equals("105")) {
			
			RestAssured.baseURI = baseUrl;
			httpRequest = RestAssured.given();
			
			// PROVIDE JSON DATA AT REQUEST BODY
			RequestParams = new JSONObject();
			RequestParams.put("userId", userId);
			
			// SET COOKIE, REQUEST HEADER AND REQUEST BODY
			httpRequest.cookie("JSESSIONID", sessionId);
			httpRequest.header("Content-Type", "application/json");
			httpRequest.body(RequestParams.toJSONString());
			
			// SET REQUEST METHOD AND API ENDPOINT
			response = httpRequest.request(Method.GET, endpointViewProfile);
			
			// MANIPULATING RESPONSE DATA
			parser = new JSONParser();
			json = (JSONObject) parser.parse(response.getBody().asString());
			
			tempData = new JSONObject();
			tempData = (JSONObject) json.get("userData");
			balanceAfterConfirm = Integer.parseInt(tempData.get("balance").toString());
			logger.info("Balance after confirm payment : " + balanceAfterConfirm);
			Assert.assertEquals(balanceAfterConfirm, userBalanceAfterTopup);
		
			//Check transaction in database
			RestAssured.baseURI = "https://danabusiness-payment.herokuapp.com";
			httpRequest = RestAssured.given();
			
			JSONObject RequestParamsCheck = new JSONObject();
			
			httpRequest.params("transactionHistoryId", transactionHistoryId);
			httpRequest.header("Content-Type", "application/json");
			httpRequest.body(RequestParamsCheck.toJSONString());
			response = httpRequest.request(Method.GET, endpointTransaction);
			
			JSONParser parserCheck = new JSONParser();
			JSONObject jsonCheck = (JSONObject) parserCheck.parse(response.getBody().asString());
			tempData = new JSONObject();
			tempData = (JSONObject) jsonCheck.get("transactionHistory");
			
			String responseBodyCheck = response.getBody().asString();
			logger.info("Response Body : " + responseBodyCheck);
			
			String responseCodeCheck = jsonCheck.get("code").toString();
			String responseDescriptionCheck = jsonCheck.get("description").toString();
			String responseTransactionIdCheck = tempData.get("id").toString();
			
			logger.info("Response code : " + responseCodeCheck);
			logger.info("Response Description : " + responseDescriptionCheck);
			
			
			// ASSERTION
			Assert.assertEquals(responseDescriptionCheck, "Success");
			Assert.assertEquals(responseCodeCheck, "100");
			Assert.assertEquals(responseTransactionIdCheck, String.valueOf(transactionHistoryId));
			
		}
		
		Thread.sleep(5);
		logger.info("[ --- Finished ConfirmPayment_TSC_002 --- ]");

	}
	
	@SuppressWarnings("unchecked")
	@Test
	void ConfirmPayment_TSC_003()throws InterruptedException, ParseException{
//		User already registered
//		User Logged in
//		Enough Balance
//		requestId is filled and unique
//		transaction history id with refund transaction
//		Confirm payment under 3 min
		logger.info("[ --- Started ConfirmPayment_TSC_003 --- ]");
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("transactionHistoryId", transactionHistoryId);
		RequestParams.put("paymentMethod", "Dana");
		RequestParams.put("requestId", generateRequestId());
		
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointConfirmPayment);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		JSONObject tempData = new JSONObject();
		tempData = (JSONObject) json.get("transactionHistory");
		
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		String responseisCompleted = tempData.get("isCompleted").toString();
		
		logger.info("Response Body : " + responseBody);
		logger.info("Response Status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		logger.info("Response isCompleted : " + responseisCompleted);
		
		// ASSERTION
		Assert.assertEquals(responseDescription, "Payment failed");
		Assert.assertEquals(responseStatus, "104");
		Assert.assertEquals(responseisCompleted, "true");
		
		if(responseStatus.toString().equals("104")) {
			
			RestAssured.baseURI = baseUrl;
			httpRequest = RestAssured.given();
			
			// PROVIDE JSON DATA AT REQUEST BODY
			RequestParams = new JSONObject();
			RequestParams.put("userId", userId);
			
			// SET COOKIE, REQUEST HEADER AND REQUEST BODY
			httpRequest.cookie("JSESSIONID", sessionId);
			httpRequest.header("Content-Type", "application/json");
			httpRequest.body(RequestParams.toJSONString());
			
			// SET REQUEST METHOD AND API ENDPOINT
			response = httpRequest.request(Method.GET, endpointViewProfile);
			
			// MANIPULATING RESPONSE DATA
			parser = new JSONParser();
			json = (JSONObject) parser.parse(response.getBody().asString());
			
			tempData = new JSONObject();
			tempData = (JSONObject) json.get("userData");
			balanceAfterConfirm = Integer.parseInt(tempData.get("balance").toString());
			logger.info("Balance after confirm payment : " + balanceAfterConfirm);
			Assert.assertEquals(balanceAfterConfirm, userBalanceAfterTopup);
		
			//Check transaction in database
			RestAssured.baseURI = "https://danabusiness-payment.herokuapp.com";
			httpRequest = RestAssured.given();
			
			JSONObject RequestParamsCheck = new JSONObject();
			
			httpRequest.params("transactionHistoryId", transactionHistoryId);
			httpRequest.header("Content-Type", "application/json");
			httpRequest.body(RequestParamsCheck.toJSONString());
			response = httpRequest.request(Method.GET, endpointTransaction);
			
			JSONParser parserCheck = new JSONParser();
			JSONObject jsonCheck = (JSONObject) parserCheck.parse(response.getBody().asString());
			tempData = new JSONObject();
			tempData = (JSONObject) jsonCheck.get("transactionHistory");
			
			String responseBodyCheck = response.getBody().asString();
			logger.info("Response Body : " + responseBodyCheck);
			
			String responseCodeCheck = jsonCheck.get("code").toString();
			String responseDescriptionCheck = jsonCheck.get("description").toString();
			String responseTransactionIdCheck = tempData.get("id").toString();
			
			logger.info("Response code : " + responseCodeCheck);
			logger.info("Response Description : " + responseDescriptionCheck);
			
			
			// ASSERTION
			Assert.assertEquals(responseDescriptionCheck, "Success");
			Assert.assertEquals(responseCodeCheck, "100");
			Assert.assertEquals(responseTransactionIdCheck, String.valueOf(transactionHistoryId));
			
			
		}
		
		
		Thread.sleep(5);
		logger.info("[ --- Finished ConfirmPayment_TSC_003 --- ]");

	}
	
	@SuppressWarnings("unchecked")
	@Parameters({"testCaseId"})
	@Test
	void ConfirmPayment_TSC_004_005_006(String testId)throws InterruptedException, ParseException{
//		User already registered
//		User Logged in
//		Enough Balance
//		requestId is filled but not unique
//		Confirm payment under 3 min
		logger.info("[ --- Started ConfirmPayment_TSC_"+testId+" --- ]");
		
		try {
			getRequestId2 = new File("./assets/generated-data/requestId2.txt");
			requestIdReader2 = new Scanner(getRequestId2);
			while (requestIdReader2.hasNextLine()) {
				String data = requestIdReader2.nextLine();
				requestId2 = data;
			}
		}catch (FileNotFoundException e) {
			System.out.println("An error occurred.");
		      e.printStackTrace();
		}
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("transactionHistoryId", transactionHistoryId);
		RequestParams.put("paymentMethod", "Dana");
		RequestParams.put("requestId", requestId2);
		
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointConfirmPayment);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		JSONObject tempData = new JSONObject();
		tempData = (JSONObject) json.get("transactionHistory");
		
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		String responseisCompleted = tempData.get("isCompleted").toString();
		
		logger.info("Response Body : " + responseBody);
		logger.info("Response Status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		logger.info("Response isCompleted : " + responseisCompleted);
		
		
		if(responseStatus.toString().equals("300")) {
			// ASSERTION
			Assert.assertEquals(responseDescription, responseStatusSuccess);
			Assert.assertEquals(responseisCompleted, "true");
			Assert.assertEquals(responseStatus, "300");
			
			RestAssured.baseURI = baseUrl;
			httpRequest = RestAssured.given();
			
			// PROVIDE JSON DATA AT REQUEST BODY
			RequestParams = new JSONObject();
			RequestParams.put("userId", userId);
			
			// SET COOKIE, REQUEST HEADER AND REQUEST BODY
			httpRequest.cookie("JSESSIONID", sessionId);
			httpRequest.header("Content-Type", "application/json");
			httpRequest.body(RequestParams.toJSONString());
			
			// SET REQUEST METHOD AND API ENDPOINT
			response = httpRequest.request(Method.GET, endpointViewProfile);
			
			// MANIPULATING RESPONSE DATA
			parser = new JSONParser();
			json = (JSONObject) parser.parse(response.getBody().asString());
			
			tempData = new JSONObject();
			tempData = (JSONObject) json.get("userData");
			balanceAfterConfirm = Integer.parseInt(tempData.get("balance").toString());
			logger.info("Balance after confirm payment : " + balanceAfterConfirm);
			Assert.assertEquals(true, balanceAfterConfirm<balanceBefore);
			
		}else if(responseStatus.toString().equals("105")) {
			// ASSERTION
			Assert.assertEquals(responseDescription, "Payment refund");
			Assert.assertEquals(responseisCompleted, "true");
			Assert.assertEquals(responseStatus, "105");
						
			RestAssured.baseURI = baseUrl;
			httpRequest = RestAssured.given();
						
			// PROVIDE JSON DATA AT REQUEST BODY
			RequestParams = new JSONObject();
			RequestParams.put("userId", userId);
						
			// SET COOKIE, REQUEST HEADER AND REQUEST BODY
			httpRequest.cookie("JSESSIONID", sessionId);
			httpRequest.header("Content-Type", "application/json");
			httpRequest.body(RequestParams.toJSONString());
						
			// SET REQUEST METHOD AND API ENDPOINT
			response = httpRequest.request(Method.GET, endpointViewProfile);
						
			// MANIPULATING RESPONSE DATA
			parser = new JSONParser();
			json = (JSONObject) parser.parse(response.getBody().asString());
						
			tempData = new JSONObject();
			tempData = (JSONObject) json.get("userData");
			balanceAfterConfirm = Integer.parseInt(tempData.get("balance").toString());
			logger.info("Balance after confirm payment : " + balanceAfterConfirm);
			
			Assert.assertEquals(balanceAfterConfirm, balanceBefore);
		}else if(responseStatus.toString().equals("104")){
			// ASSERTION
			Assert.assertEquals(responseDescription, "Payment failed");
			Assert.assertEquals(responseisCompleted, "true");
			Assert.assertEquals(responseStatus, "104");
						
			RestAssured.baseURI = baseUrl;
			httpRequest = RestAssured.given();
						
			// PROVIDE JSON DATA AT REQUEST BODY
			RequestParams = new JSONObject();
			RequestParams.put("userId", userId);
						
			// SET COOKIE, REQUEST HEADER AND REQUEST BODY
			httpRequest.cookie("JSESSIONID", sessionId);
			httpRequest.header("Content-Type", "application/json");
			httpRequest.body(RequestParams.toJSONString());
						
			// SET REQUEST METHOD AND API ENDPOINT
			response = httpRequest.request(Method.GET, endpointViewProfile);
						
			// MANIPULATING RESPONSE DATA
			parser = new JSONParser();
			json = (JSONObject) parser.parse(response.getBody().asString());
						
			tempData = new JSONObject();
			tempData = (JSONObject) json.get("userData");
			balanceAfterConfirm = Integer.parseInt(tempData.get("balance").toString());
			logger.info("Balance after confirm payment : " + balanceAfterConfirm);
			
			Assert.assertEquals(balanceAfterConfirm, balanceBefore);
			
		}
		
		Thread.sleep(5);
		logger.info("[ --- Finished ConfirmPayment_TSC_"+testId+" --- ]");

	}
	
	@SuppressWarnings("unchecked")
	@Parameters({"testCaseId"})
	@Test
	void ConfirmPayment_TSC_007_008_009(String testId)throws InterruptedException, ParseException{
//		User already registered
//		User Logged in
//		Enough Balance
//		requestId is null
//		Confirm payment under 3 min
		logger.info("[ --- Started ConfirmPayment_TSC_"+testId+"--- ]");
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("transactionHistoryId", transactionHistoryId);
		RequestParams.put("paymentMethod", "Dana");
		RequestParams.put("requestId", null);
		
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointConfirmPayment);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		JSONObject tempData = new JSONObject();
		tempData = (JSONObject) json.get("transactionHistory");
		
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Response Body : " + responseBody);
		logger.info("Response Status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION
		Assert.assertEquals(responseDescription, "RequestBody attributes shouldn't be null");
		Assert.assertEquals(responseStatus, "106");
		
		if(responseStatus.toString().equals("106")) {
			
			RestAssured.baseURI = baseUrl;
			httpRequest = RestAssured.given();
			
			// PROVIDE JSON DATA AT REQUEST BODY
			RequestParams = new JSONObject();
			RequestParams.put("userId", userId);
			
			// SET COOKIE, REQUEST HEADER AND REQUEST BODY
			httpRequest.cookie("JSESSIONID", sessionId);
			httpRequest.header("Content-Type", "application/json");
			httpRequest.body(RequestParams.toJSONString());
			
			// SET REQUEST METHOD AND API ENDPOINT
			response = httpRequest.request(Method.GET, endpointViewProfile);
			
			// MANIPULATING RESPONSE DATA
			parser = new JSONParser();
			json = (JSONObject) parser.parse(response.getBody().asString());
			
			tempData = new JSONObject();
			tempData = (JSONObject) json.get("userData");
			balanceAfterConfirm = Integer.parseInt(tempData.get("balance").toString());
			logger.info("Balance after confirm payment : " + balanceAfterConfirm);
			Assert.assertEquals(balanceAfterConfirm, userBalanceAfterTopup);
		}
		
		Thread.sleep(5);
		logger.info("[ --- Finished ConfirmPayment_TSC_"+testId+"--- ]");

	}
	
	@SuppressWarnings("unchecked")
	@Parameters({"testCaseId"})
	@Test
	void ConfirmPayment_TSC_010_011_012(String testId)throws InterruptedException, ParseException{
//		User already registered
//		User Logged in
//		Not Enough Balance
//		requestId is filled and unique
//		Confirm payment under 3 min
		logger.info("[ --- Started ConfirmPayment_TSC_"+testId+"--- ]");
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("transactionHistoryId", transactionHistoryId);
		RequestParams.put("paymentMethod", "Dana");
		RequestParams.put("requestId", generateRequestId());
		
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointConfirmPayment);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		JSONObject tempData = new JSONObject();
		tempData = (JSONObject) json.get("transactionHistory");
		
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Response Body : " + responseBody);
		logger.info("Response Status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		
		if(responseStatus.toString().equals("202")) {
			// ASSERTION
			Assert.assertEquals(responseDescription, "Insufficient balance");
			Assert.assertEquals(responseStatus, "202");
			
			RestAssured.baseURI = baseUrl;
			httpRequest = RestAssured.given();
			
			// PROVIDE JSON DATA AT REQUEST BODY
			RequestParams = new JSONObject();
			RequestParams.put("userId", userId);
			
			// SET COOKIE, REQUEST HEADER AND REQUEST BODY
			httpRequest.cookie("JSESSIONID", sessionId);
			httpRequest.header("Content-Type", "application/json");
			httpRequest.body(RequestParams.toJSONString());
			
			// SET REQUEST METHOD AND API ENDPOINT
			response = httpRequest.request(Method.GET, endpointViewProfile);
			
			// MANIPULATING RESPONSE DATA
			parser = new JSONParser();
			json = (JSONObject) parser.parse(response.getBody().asString());
			
			tempData = new JSONObject();
			tempData = (JSONObject) json.get("userData");
			balanceAfterConfirm = Integer.parseInt(tempData.get("balance").toString());
			logger.info("Balance after confirm payment : " + balanceAfterConfirm);
			Assert.assertEquals(balanceAfterConfirm, balanceBefore);
		}else
		if(responseStatus.toString().equals("105")) {
			String responseisCompleted = tempData.get("isCompleted").toString();
			
			Assert.assertEquals(responseDescription, "Payment refund");
			Assert.assertEquals(responseStatus, "105");
			Assert.assertEquals(responseisCompleted, "true");
			
			RestAssured.baseURI = baseUrl;
			httpRequest = RestAssured.given();
			
			// PROVIDE JSON DATA AT REQUEST BODY
			RequestParams = new JSONObject();
			RequestParams.put("userId", userId);
			
			// SET COOKIE, REQUEST HEADER AND REQUEST BODY
			httpRequest.cookie("JSESSIONID", sessionId);
			httpRequest.header("Content-Type", "application/json");
			httpRequest.body(RequestParams.toJSONString());
			
			// SET REQUEST METHOD AND API ENDPOINT
			response = httpRequest.request(Method.GET, endpointViewProfile);
			
			// MANIPULATING RESPONSE DATA
			parser = new JSONParser();
			json = (JSONObject) parser.parse(response.getBody().asString());
			
			tempData = new JSONObject();
			tempData = (JSONObject) json.get("userData");
			balanceAfterConfirm = Integer.parseInt(tempData.get("balance").toString());
			logger.info("Balance after confirm payment : " + balanceAfterConfirm);
			Assert.assertEquals(balanceAfterConfirm, balanceBefore);
		}else
		if(responseStatus.toString().equals("104")) {
			String responseisCompleted = tempData.get("isCompleted").toString();
			
			Assert.assertEquals(responseDescription, "Payment failed");
			Assert.assertEquals(responseStatus, "104");
			Assert.assertEquals(responseisCompleted, "true");
			
			RestAssured.baseURI = baseUrl;
			httpRequest = RestAssured.given();
			
			// PROVIDE JSON DATA AT REQUEST BODY
			RequestParams = new JSONObject();
			RequestParams.put("userId", userId);
			
			// SET COOKIE, REQUEST HEADER AND REQUEST BODY
			httpRequest.cookie("JSESSIONID", sessionId);
			httpRequest.header("Content-Type", "application/json");
			httpRequest.body(RequestParams.toJSONString());
			
			// SET REQUEST METHOD AND API ENDPOINT
			response = httpRequest.request(Method.GET, endpointViewProfile);
			
			// MANIPULATING RESPONSE DATA
			parser = new JSONParser();
			json = (JSONObject) parser.parse(response.getBody().asString());
			
			tempData = new JSONObject();
			tempData = (JSONObject) json.get("userData");
			balanceAfterConfirm = Integer.parseInt(tempData.get("balance").toString());
			logger.info("Balance after confirm payment : " + balanceAfterConfirm);
			Assert.assertEquals(balanceAfterConfirm, balanceBefore);
		}
		
		
		Thread.sleep(5);
		logger.info("[ --- Finished ConfirmPayment_TSC_"+testId+"--- ]");

	}
	
	@SuppressWarnings("unchecked")
	@Parameters({"testCaseId"})
	@Test
	void ConfirmPayment_TSC_013_014_015(String testId)throws InterruptedException, ParseException{
//		User already registered
//		User Logged in
//		Not Enough Balance
//		requestId is filled but not unique
//		Confirm payment under 3 min
		
		logger.info("[ --- Started ConfirmPayment_TSC_"+testId+" --- ]");
		
		try {
			getRequestId2 = new File("./assets/generated-data/requestId2.txt");
			requestIdReader2 = new Scanner(getRequestId2);
			while (requestIdReader2.hasNextLine()) {
				String data = requestIdReader2.nextLine();
				requestId2 = data;
			}
		}catch (FileNotFoundException e) {
			System.out.println("An error occurred.");
		      e.printStackTrace();
		}
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("transactionHistoryId", transactionHistoryId);
		RequestParams.put("paymentMethod", "Dana");
		RequestParams.put("requestId", requestId2);
		
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointConfirmPayment);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		JSONObject tempData = new JSONObject();
		tempData = (JSONObject) json.get("transactionHistory");
		
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		String responseisCompleted = tempData.get("isCompleted").toString();
		
		logger.info("Response Body : " + responseBody);
		logger.info("Response Status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		logger.info("Response isCompleted : " + responseisCompleted);
		
		
		if(responseStatus.toString().equals("300")) {
			// ASSERTION
			Assert.assertEquals(responseDescription, responseStatusSuccess);
			Assert.assertEquals(responseisCompleted, "true");
			Assert.assertEquals(responseStatus, "300");
			
			RestAssured.baseURI = baseUrl;
			httpRequest = RestAssured.given();
			
			// PROVIDE JSON DATA AT REQUEST BODY
			RequestParams = new JSONObject();
			RequestParams.put("userId", userId);
			
			// SET COOKIE, REQUEST HEADER AND REQUEST BODY
			httpRequest.cookie("JSESSIONID", sessionId);
			httpRequest.header("Content-Type", "application/json");
			httpRequest.body(RequestParams.toJSONString());
			
			// SET REQUEST METHOD AND API ENDPOINT
			response = httpRequest.request(Method.GET, endpointViewProfile);
			
			// MANIPULATING RESPONSE DATA
			parser = new JSONParser();
			json = (JSONObject) parser.parse(response.getBody().asString());
			
			tempData = new JSONObject();
			tempData = (JSONObject) json.get("userData");
			balanceAfterConfirm = Integer.parseInt(tempData.get("balance").toString());
			logger.info("Balance after confirm payment : " + balanceAfterConfirm);
			Assert.assertEquals(true, balanceAfterConfirm<balanceBefore);
			
		}else if(responseStatus.toString().equals("105")) {
			// ASSERTION
			Assert.assertEquals(responseDescription, "Payment refund");
			Assert.assertEquals(responseisCompleted, "true");
			Assert.assertEquals(responseStatus, "105");
						
			RestAssured.baseURI = baseUrl;
			httpRequest = RestAssured.given();
						
			// PROVIDE JSON DATA AT REQUEST BODY
			RequestParams = new JSONObject();
			RequestParams.put("userId", userId);
						
			// SET COOKIE, REQUEST HEADER AND REQUEST BODY
			httpRequest.cookie("JSESSIONID", sessionId);
			httpRequest.header("Content-Type", "application/json");
			httpRequest.body(RequestParams.toJSONString());
						
			// SET REQUEST METHOD AND API ENDPOINT
			response = httpRequest.request(Method.GET, endpointViewProfile);
						
			// MANIPULATING RESPONSE DATA
			parser = new JSONParser();
			json = (JSONObject) parser.parse(response.getBody().asString());
						
			tempData = new JSONObject();
			tempData = (JSONObject) json.get("userData");
			balanceAfterConfirm = Integer.parseInt(tempData.get("balance").toString());
			logger.info("Balance after confirm payment : " + balanceAfterConfirm);
			
			Assert.assertEquals(balanceAfterConfirm, balanceBefore);
		}else if(responseStatus.toString().equals("104")){
			// ASSERTION
			Assert.assertEquals(responseDescription, "Payment failed");
			Assert.assertEquals(responseisCompleted, "true");
			Assert.assertEquals(responseStatus, "104");
						
			RestAssured.baseURI = baseUrl;
			httpRequest = RestAssured.given();
						
			// PROVIDE JSON DATA AT REQUEST BODY
			RequestParams = new JSONObject();
			RequestParams.put("userId", userId);
						
			// SET COOKIE, REQUEST HEADER AND REQUEST BODY
			httpRequest.cookie("JSESSIONID", sessionId);
			httpRequest.header("Content-Type", "application/json");
			httpRequest.body(RequestParams.toJSONString());
						
			// SET REQUEST METHOD AND API ENDPOINT
			response = httpRequest.request(Method.GET, endpointViewProfile);
						
			// MANIPULATING RESPONSE DATA
			parser = new JSONParser();
			json = (JSONObject) parser.parse(response.getBody().asString());
						
			tempData = new JSONObject();
			tempData = (JSONObject) json.get("userData");
			balanceAfterConfirm = Integer.parseInt(tempData.get("balance").toString());
			logger.info("Balance after confirm payment : " + balanceAfterConfirm);
			
			Assert.assertEquals(balanceAfterConfirm, balanceBefore);
			
		}
		Thread.sleep(5);
		logger.info("[ --- Finished ConfirmPayment_TSC_"+testId+"--- ]");

	}
	
	@SuppressWarnings("unchecked")
	@Parameters({"testCaseId"})
	@Test
	void ConfirmPayment_TSC_016_017_018(String testId)throws InterruptedException, ParseException{
//		User already registered
//		User Logged in
//		Not Enough Balance
//		requestId is null
//		Confirm payment under 3 min
		logger.info("[ --- Started ConfirmPayment_TSC_"+testId+"--- ]");
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("transactionHistoryId", transactionHistoryId);
		RequestParams.put("paymentMethod", "Dana");
		RequestParams.put("requestId", null);
		
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointConfirmPayment);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		JSONObject tempData = new JSONObject();
		tempData = (JSONObject) json.get("transactionHistory");
		
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Response Body : " + responseBody);
		logger.info("Response Status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION
		Assert.assertEquals(responseDescription, "RequestBody attributes shouldn't be null");
		Assert.assertEquals(responseStatus, "106");
		
		if(responseStatus.toString().equals("106")) {
			
			RestAssured.baseURI = baseUrl;
			httpRequest = RestAssured.given();
			
			// PROVIDE JSON DATA AT REQUEST BODY
			RequestParams = new JSONObject();
			RequestParams.put("userId", userId);
			
			// SET COOKIE, REQUEST HEADER AND REQUEST BODY
			httpRequest.cookie("JSESSIONID", sessionId);
			httpRequest.header("Content-Type", "application/json");
			httpRequest.body(RequestParams.toJSONString());
			
			// SET REQUEST METHOD AND API ENDPOINT
			response = httpRequest.request(Method.GET, endpointViewProfile);
			
			// MANIPULATING RESPONSE DATA
			parser = new JSONParser();
			json = (JSONObject) parser.parse(response.getBody().asString());
			
			tempData = new JSONObject();
			tempData = (JSONObject) json.get("userData");
			balanceAfterConfirm = Integer.parseInt(tempData.get("balance").toString());
			logger.info("Balance after confirm payment : " + balanceAfterConfirm);
			Assert.assertEquals(balanceAfterConfirm, balanceBefore);
		}
		
		
		Thread.sleep(5);
		logger.info("[ --- Finished ConfirmPayment_TSC_"+testId+"--- ]");

	}
	
	@SuppressWarnings("unchecked")
	@Parameters({"testCaseId"})
	@Test
	void ConfirmPayment_TSC_019_020_021(String testId)throws InterruptedException, ParseException{
//		User already registered
//		User not Logged in
//		Enough Balance
//		requestId is filled and unique
//		Confirm payment under 3 min
		logger.info("[ --- Started ConfirmPayment_TSC_"+testId+"--- ]");
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("transactionHistoryId", transactionHistoryId);
		RequestParams.put("paymentMethod", "Dana");
		RequestParams.put("requestId", generateRequestId());
		
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointConfirmPayment);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Response Body : " + responseBody);
		logger.info("Response Status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION
		Assert.assertEquals(responseDescription, "USER_NOT_LOGGED_IN");
		Assert.assertEquals(responseStatus, "301");
		
		if(responseStatus.toString().equals("301")) {
			
			RestAssured.baseURI = "https://dana-bussiness-member.herokuapp.com/v1";
			httpRequest = RestAssured.given();
			
			// PROVIDE JSON DATA AT REQUEST BODY
			RequestParams = new JSONObject();
			
			// SET COOKIE, REQUEST HEADER AND REQUEST BODY
			httpRequest.params("id", String.valueOf(userId));
			httpRequest.header("Content-Type", "application/json");
			httpRequest.body(RequestParams.toJSONString());
			
			// SET REQUEST METHOD AND API ENDPOINT
			response = httpRequest.request(Method.GET, endpointGetUser);
			
			// MANIPULATING RESPONSE DATA
			parser = new JSONParser();
			json = (JSONObject) parser.parse(response.getBody().asString());
			
			JSONObject tempData = new JSONObject();
			tempData = (JSONObject) json.get("userData");
			balanceAfterConfirm = Integer.parseInt(tempData.get("balance").toString());
			logger.info("Balance after confirm payment : " + balanceAfterConfirm);
			Assert.assertEquals(balanceAfterConfirm, balanceBefore);
		}
		
		Thread.sleep(5);
		logger.info("[ --- Finished ConfirmPayment_TSC_"+testId+"--- ]");

	}
	
	@SuppressWarnings("unchecked")
	@Parameters({"testCaseId"})
	@Test
	void ConfirmPayment_TSC_022_023_024(String testId)throws InterruptedException, ParseException{
//		User already registered
//		User not Logged in
//		Enough Balance
//		requestId is filled but not unique
//		Confirm payment under 3 min
		
		logger.info("Logged out user with ID : " + userId + " and JSESSION ID " + sessionId);
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParamsLogout = new JSONObject();
		
		RequestParamsLogout.put("userId", userId);
		
		httpRequest.cookie("JSESSIONID", sessionId);
		
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParamsLogout.toJSONString());
		response = httpRequest.request(Method.POST, endpointLogout);
		
		JSONParser parserLogout = new JSONParser();
		JSONObject jsonLogout = (JSONObject) parserLogout.parse(response.getBody().asString());
		String responseHeaderLogout = response.getHeader("Set-Cookie");
		String responseBodyLogout = response.getBody().asString();
		logger.info("Response Body : " + responseBodyLogout);
		
		String responseCodeLogout = jsonLogout.get("code").toString();
		String responseDescriptionLogout = jsonLogout.get("description").toString();
		
		logger.info("Response Header : " + responseHeaderLogout);
		logger.info("Response code : " + responseCodeLogout);
		logger.info("Response Description : " + responseDescriptionLogout);
		
		
		// ASSERTION
		Assert.assertEquals(responseDescriptionLogout, "SUCCESS");
		Assert.assertEquals(responseCodeLogout.toString(), "300");
		
		
		logger.info("[ --- Started ConfirmPayment_TSC_"+testId+"--- ]");
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("transactionHistoryId", transactionHistoryId);
		RequestParams.put("paymentMethod", "Dana");
		RequestParams.put("requestId", requestId);
		
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointConfirmPayment);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Response Body : " + responseBody);
		logger.info("Response Status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION
		Assert.assertEquals(responseDescription, "USER_NOT_LOGGED_IN");
		Assert.assertEquals(responseStatus, "301");
		
		Thread.sleep(5);
		logger.info("[ --- Finished ConfirmPayment_TSC_"+testId+"--- ]");

	}
	
	@SuppressWarnings("unchecked")
	@Parameters({"testCaseId"})
	@Test
	void ConfirmPayment_TSC_025_026_027(String testId)throws InterruptedException, ParseException{
//		User already registered
//		User not Logged in
//		Enough Balance
//		requestId is null
//		Confirm payment under 3 min
		logger.info("[ --- Started ConfirmPayment_TSC_"+testId+"--- ]");
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("transactionHistoryId", transactionHistoryId);
		RequestParams.put("paymentMethod", "Dana");
		RequestParams.put("requestId", null);
		
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointConfirmPayment);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Response Body : " + responseBody);
		logger.info("Response Status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION
		Assert.assertEquals(responseDescription, "USER_NOT_LOGGED_IN");
		Assert.assertEquals(responseStatus, "301");
		
		if(responseStatus.toString().equals("301")) {
			
			RestAssured.baseURI = "https://dana-bussiness-member.herokuapp.com/v1";
			httpRequest = RestAssured.given();
			
			// PROVIDE JSON DATA AT REQUEST BODY
			RequestParams = new JSONObject();
			
			// SET COOKIE, REQUEST HEADER AND REQUEST BODY
			httpRequest.params("id", String.valueOf(userId));
			httpRequest.header("Content-Type", "application/json");
			httpRequest.body(RequestParams.toJSONString());
			
			// SET REQUEST METHOD AND API ENDPOINT
			response = httpRequest.request(Method.GET, endpointGetUser);
			
			// MANIPULATING RESPONSE DATA
			parser = new JSONParser();
			json = (JSONObject) parser.parse(response.getBody().asString());
			
			JSONObject tempData = new JSONObject();
			tempData = (JSONObject) json.get("userData");
			balanceAfterConfirm = Integer.parseInt(tempData.get("balance").toString());
			logger.info("Balance after confirm payment : " + balanceAfterConfirm);
			Assert.assertEquals(balanceAfterConfirm, balanceBefore);
		}
		
		Thread.sleep(5);
		logger.info("[ --- Finished ConfirmPayment_TSC_"+testId+"--- ]");

	}
	
	@SuppressWarnings("unchecked")
	@Parameters({"testCaseId"})
	@Test
	void ConfirmPayment_TSC_028_029_030(String testId)throws InterruptedException, ParseException{
//		User already registered
//		User not Logged in
//		Not Enough Balance
//		requestId is filled and unique
//		Confirm payment under 3 min
		logger.info("[ --- Started ConfirmPayment_TSC_"+testId+"--- ]");
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("transactionHistoryId", transactionHistoryId);
		RequestParams.put("paymentMethod", "Dana");
		RequestParams.put("requestId", generateRequestId());
		
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointConfirmPayment);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Response Body : " + responseBody);
		logger.info("Response Status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION
		Assert.assertEquals(responseDescription, "USER_NOT_LOGGED_IN");
		Assert.assertEquals(responseStatus, "301");
		
		if(responseStatus.toString().equals("301")) {
			
			RestAssured.baseURI = "https://dana-bussiness-member.herokuapp.com/v1";
			httpRequest = RestAssured.given();
			
			// PROVIDE JSON DATA AT REQUEST BODY
			RequestParams = new JSONObject();
			
			// SET COOKIE, REQUEST HEADER AND REQUEST BODY
			httpRequest.params("id", String.valueOf(userId));
			httpRequest.header("Content-Type", "application/json");
			httpRequest.body(RequestParams.toJSONString());
			
			// SET REQUEST METHOD AND API ENDPOINT
			response = httpRequest.request(Method.GET, endpointGetUser);
			
			// MANIPULATING RESPONSE DATA
			parser = new JSONParser();
			json = (JSONObject) parser.parse(response.getBody().asString());
			
			JSONObject tempData = new JSONObject();
			tempData = (JSONObject) json.get("userData");
			balanceAfterConfirm = Integer.parseInt(tempData.get("balance").toString());
			logger.info("Balance after confirm payment : " + balanceAfterConfirm);
			Assert.assertEquals(balanceAfterConfirm, balanceBefore);
		}			
		
		Thread.sleep(5);
		logger.info("[ --- Finished ConfirmPayment_TSC_"+testId+"--- ]");

	}
	
	@SuppressWarnings("unchecked")
	@Parameters({"testCaseId"})
	@Test
	void ConfirmPayment_TSC_031_032_033(String testId)throws InterruptedException, ParseException{
//		User already registered
//		User not Logged in
//		Not Enough Balance
//		requestId is filled and unique
//		Confirm payment under 3 min
		logger.info("[ --- Started ConfirmPayment_TSC_"+testId+"--- ]");
		
		logger.info("Logged out user with ID : " + userId + " and JSESSION ID " + sessionId);
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParamsLogout = new JSONObject();
		
		RequestParamsLogout.put("userId", userId);
		
		httpRequest.cookie("JSESSIONID", sessionId);
		
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParamsLogout.toJSONString());
		response = httpRequest.request(Method.POST, endpointLogout);
		
		JSONParser parserLogout = new JSONParser();
		JSONObject jsonLogout = (JSONObject) parserLogout.parse(response.getBody().asString());
		String responseHeaderLogout = response.getHeader("Set-Cookie");
		String responseBodyLogout = response.getBody().asString();
		logger.info("Response Body : " + responseBodyLogout);
		
		String responseCodeLogout = jsonLogout.get("code").toString();
		String responseDescriptionLogout = jsonLogout.get("description").toString();
		
		logger.info("Response Header : " + responseHeaderLogout);
		logger.info("Response code : " + responseCodeLogout);
		logger.info("Response Description : " + responseDescriptionLogout);
		
		
		// ASSERTION
		Assert.assertEquals(responseDescriptionLogout, "SUCCESS");
		Assert.assertEquals(responseCodeLogout.toString(), "300");
		
		
		logger.info("[ --- Started ConfirmPayment_TSC_"+testId+"--- ]");
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("transactionHistoryId", transactionHistoryId);
		RequestParams.put("paymentMethod", "Dana");
		RequestParams.put("requestId", requestId);
		
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointConfirmPayment);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Response Body : " + responseBody);
		logger.info("Response Status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION
		Assert.assertEquals(responseDescription, "USER_NOT_LOGGED_IN");
		Assert.assertEquals(responseStatus, "301");
		
		
		Thread.sleep(5);
		logger.info("[ --- Finished ConfirmPayment_TSC_"+testId+"--- ]");

	}
	
	@SuppressWarnings("unchecked")
	@Parameters({"testCaseId"})
	@Test
	void ConfirmPayment_TSC_034_035_036(String testId)throws InterruptedException, ParseException{
//		User already registered
//		User not Logged in
//		Not Enough Balance
//		requestId is null
//		Confirm payment under 3 min
		logger.info("[ --- Started ConfirmPayment_TSC_"+testId+"--- ]");
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("transactionHistoryId", transactionHistoryId);
		RequestParams.put("paymentMethod", "Dana");
		RequestParams.put("requestId", null);
		
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointConfirmPayment);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Response Body : " + responseBody);
		logger.info("Response Status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION
		Assert.assertEquals(responseDescription, "USER_NOT_LOGGED_IN");
		Assert.assertEquals(responseStatus, "301");
		
		if(responseStatus.toString().equals("301")) {
			
			RestAssured.baseURI = "https://dana-bussiness-member.herokuapp.com/v1";
			httpRequest = RestAssured.given();
			
			// PROVIDE JSON DATA AT REQUEST BODY
			RequestParams = new JSONObject();
			
			// SET COOKIE, REQUEST HEADER AND REQUEST BODY
			httpRequest.params("id", String.valueOf(userId));
			httpRequest.header("Content-Type", "application/json");
			httpRequest.body(RequestParams.toJSONString());
			
			// SET REQUEST METHOD AND API ENDPOINT
			response = httpRequest.request(Method.GET, endpointGetUser);
			
			// MANIPULATING RESPONSE DATA
			parser = new JSONParser();
			json = (JSONObject) parser.parse(response.getBody().asString());
			
			JSONObject tempData = new JSONObject();
			tempData = (JSONObject) json.get("userData");
			balanceAfterConfirm = Integer.parseInt(tempData.get("balance").toString());
			logger.info("Balance after confirm payment : " + balanceAfterConfirm);
			Assert.assertEquals(balanceAfterConfirm, balanceBefore);
		}
		
		Thread.sleep(5);
		logger.info("[ --- Finished ConfirmPayment_TSC_"+testId+"--- ]");

	}
	
	@SuppressWarnings("unchecked")
	@Parameters({"testCaseId"})
	@Test
	void ConfirmPayment_TSC_037_038_039(String testId)throws InterruptedException, ParseException{
//		User already registered
//		User Logged in
//		Enough Balance
//		requestId is filled and unique
//		transaction history id with success transaction
//		Confirm payment 5 min(300000 ms)
		logger.info("[ --- Started ConfirmPayment_TSC_"+testId+" --- ]");
		
		Thread.sleep(300000);
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("transactionHistoryId", transactionHistoryId);
		RequestParams.put("paymentMethod", "Dana");
		RequestParams.put("requestId", generateRequestId());
		
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointConfirmPayment);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		JSONObject tempData = new JSONObject();
		tempData = (JSONObject) json.get("transactionHistory");
		JSONObject tempData2 = new JSONObject();
		tempData2 = (JSONObject) json.get("transactionDetail");
		
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		String responseisCompleted = tempData.get("isCompleted").toString();
		String responseid = tempData.get("id").toString();
		String responsetransactionResult = tempData2.get("transactionResult").toString();
		
		logger.info("Response Body : " + responseBody);
		logger.info("Response Status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		logger.info("Response isCompleted : " + responseisCompleted);
		
		// ASSERTION
		Assert.assertEquals(responseDescription, "Transaction already completed");
		Assert.assertEquals(responseStatus, "108");
		Assert.assertEquals(responseisCompleted, "true");
		Assert.assertEquals(responseid, String.valueOf(transactionHistoryId));
		Assert.assertEquals(responsetransactionResult, "2");
		
		if(responseStatus.toString().equals("108")) {
			
			RestAssured.baseURI = "https://dana-bussiness-member.herokuapp.com/v1";
			httpRequest = RestAssured.given();
			
			// PROVIDE JSON DATA AT REQUEST BODY
			RequestParams = new JSONObject();
			
			// SET COOKIE, REQUEST HEADER AND REQUEST BODY
			httpRequest.params("id", String.valueOf(userId));
			httpRequest.header("Content-Type", "application/json");
			httpRequest.body(RequestParams.toJSONString());
			
			// SET REQUEST METHOD AND API ENDPOINT
			response = httpRequest.request(Method.GET, endpointGetUser);
			
			// MANIPULATING RESPONSE DATA
			parser = new JSONParser();
			json = (JSONObject) parser.parse(response.getBody().asString());
			
			JSONObject tempData3 = new JSONObject();
			tempData3 = (JSONObject) json.get("userData");
			balanceAfterConfirm = Integer.parseInt(tempData3.get("balance").toString());
			logger.info("Balance after confirm payment : " + balanceAfterConfirm);
			Assert.assertEquals(balanceAfterConfirm, balanceBefore);
		}	
		
		Thread.sleep(5);
		logger.info("[ --- Finished ConfirmPayment_TSC_"+testId+" --- ]");

	}
	
	@SuppressWarnings("unchecked")
	@Parameters({"testCaseId"})
	@Test
	void ConfirmPayment_TSC_040_041_042(String testId)throws InterruptedException, ParseException{
//		User already registered
//		User Logged in
//		Enough Balance
//		requestId is filled but not unique
//		Confirm payment 5 min
		logger.info("[ --- Started ConfirmPayment_TSC_"+testId+" --- ]");
		
		Thread.sleep(300000);
		
		try {
			getRequestId2 = new File("./assets/generated-data/requestId2.txt");
			requestIdReader2 = new Scanner(getRequestId2);
			while (requestIdReader2.hasNextLine()) {
				String data = requestIdReader2.nextLine();
				requestId2 = data;
			}
		}catch (FileNotFoundException e) {
			System.out.println("An error occurred.");
		      e.printStackTrace();
		}
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("transactionHistoryId", transactionHistoryId);
		RequestParams.put("paymentMethod", "Dana");
		RequestParams.put("requestId", requestId2);
		
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointConfirmPayment);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		JSONObject tempData = new JSONObject();
		tempData = (JSONObject) json.get("transactionHistory");
		
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		String responseisCompleted = tempData.get("isCompleted").toString();
		
		logger.info("Response Body : " + responseBody);
		logger.info("Response Status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		logger.info("Response isCompleted : " + responseisCompleted);
		
		
		if(responseStatus.toString().equals("300")) {
			// ASSERTION
			Assert.assertEquals(responseDescription, responseStatusSuccess);
			Assert.assertEquals(responseisCompleted, "true");
			Assert.assertEquals(responseStatus, "300");
			
			RestAssured.baseURI = baseUrl;
			httpRequest = RestAssured.given();
			
			// PROVIDE JSON DATA AT REQUEST BODY
			RequestParams = new JSONObject();
			RequestParams.put("userId", userId);
			
			// SET COOKIE, REQUEST HEADER AND REQUEST BODY
			httpRequest.cookie("JSESSIONID", sessionId);
			httpRequest.header("Content-Type", "application/json");
			httpRequest.body(RequestParams.toJSONString());
			
			// SET REQUEST METHOD AND API ENDPOINT
			response = httpRequest.request(Method.GET, endpointViewProfile);
			
			// MANIPULATING RESPONSE DATA
			parser = new JSONParser();
			json = (JSONObject) parser.parse(response.getBody().asString());
			
			tempData = new JSONObject();
			tempData = (JSONObject) json.get("userData");
			balanceAfterConfirm = Integer.parseInt(tempData.get("balance").toString());
			logger.info("Balance after confirm payment : " + balanceAfterConfirm);
			Assert.assertEquals(true, balanceAfterConfirm<balanceBefore);
			
		}else if(responseStatus.toString().equals("105")) {
			// ASSERTION
			Assert.assertEquals(responseDescription, "Payment refund");
			Assert.assertEquals(responseisCompleted, "true");
			Assert.assertEquals(responseStatus, "105");
						
			RestAssured.baseURI = baseUrl;
			httpRequest = RestAssured.given();
						
			// PROVIDE JSON DATA AT REQUEST BODY
			RequestParams = new JSONObject();
			RequestParams.put("userId", userId);
						
			// SET COOKIE, REQUEST HEADER AND REQUEST BODY
			httpRequest.cookie("JSESSIONID", sessionId);
			httpRequest.header("Content-Type", "application/json");
			httpRequest.body(RequestParams.toJSONString());
						
			// SET REQUEST METHOD AND API ENDPOINT
			response = httpRequest.request(Method.GET, endpointViewProfile);
						
			// MANIPULATING RESPONSE DATA
			parser = new JSONParser();
			json = (JSONObject) parser.parse(response.getBody().asString());
						
			tempData = new JSONObject();
			tempData = (JSONObject) json.get("userData");
			balanceAfterConfirm = Integer.parseInt(tempData.get("balance").toString());
			logger.info("Balance after confirm payment : " + balanceAfterConfirm);
			
			Assert.assertEquals(balanceAfterConfirm, balanceBefore);
		}else if(responseStatus.toString().equals("104")){
			// ASSERTION
			Assert.assertEquals(responseDescription, "Payment failed");
			Assert.assertEquals(responseisCompleted, "true");
			Assert.assertEquals(responseStatus, "104");
						
			RestAssured.baseURI = baseUrl;
			httpRequest = RestAssured.given();
						
			// PROVIDE JSON DATA AT REQUEST BODY
			RequestParams = new JSONObject();
			RequestParams.put("userId", userId);
						
			// SET COOKIE, REQUEST HEADER AND REQUEST BODY
			httpRequest.cookie("JSESSIONID", sessionId);
			httpRequest.header("Content-Type", "application/json");
			httpRequest.body(RequestParams.toJSONString());
						
			// SET REQUEST METHOD AND API ENDPOINT
			response = httpRequest.request(Method.GET, endpointViewProfile);
						
			// MANIPULATING RESPONSE DATA
			parser = new JSONParser();
			json = (JSONObject) parser.parse(response.getBody().asString());
						
			tempData = new JSONObject();
			tempData = (JSONObject) json.get("userData");
			balanceAfterConfirm = Integer.parseInt(tempData.get("balance").toString());
			logger.info("Balance after confirm payment : " + balanceAfterConfirm);
			
			Assert.assertEquals(balanceAfterConfirm, balanceBefore);
			
		}
		
		Thread.sleep(5);
		logger.info("[ --- Finished ConfirmPayment_TSC_"+testId+" --- ]");

	}
	
	@SuppressWarnings("unchecked")
	@Parameters({"testCaseId"})
	@Test
	void ConfirmPayment_TSC_043_044_045(String testId)throws InterruptedException, ParseException{
//		User already registered
//		User Logged in
//		Enough Balance
//		requestId is null
//		Confirm payment 5 min
		logger.info("[ --- Started ConfirmPayment_TSC_"+testId+"--- ]");
		
		Thread.sleep(300000);
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("transactionHistoryId", transactionHistoryId);
		RequestParams.put("paymentMethod", "Dana");
		RequestParams.put("requestId", null);
		
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointConfirmPayment);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Response Body : " + responseBody);
		logger.info("Response Status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION
		Assert.assertEquals(responseDescription, "RequestBody attributes shouldn't be null");
		Assert.assertEquals(responseStatus, "106");
		
		if(responseStatus.toString().equals("106")) {
			
			RestAssured.baseURI = "https://dana-bussiness-member.herokuapp.com/v1";
			httpRequest = RestAssured.given();
			
			// PROVIDE JSON DATA AT REQUEST BODY
			RequestParams = new JSONObject();
			
			// SET COOKIE, REQUEST HEADER AND REQUEST BODY
			httpRequest.params("id", String.valueOf(userId));
			httpRequest.header("Content-Type", "application/json");
			httpRequest.body(RequestParams.toJSONString());
			
			// SET REQUEST METHOD AND API ENDPOINT
			response = httpRequest.request(Method.GET, endpointGetUser);
			
			// MANIPULATING RESPONSE DATA
			parser = new JSONParser();
			json = (JSONObject) parser.parse(response.getBody().asString());
			
			JSONObject tempData3 = new JSONObject();
			tempData3 = (JSONObject) json.get("userData");
			balanceAfterConfirm = Integer.parseInt(tempData3.get("balance").toString());
			logger.info("Balance after confirm payment : " + balanceAfterConfirm);
			Assert.assertEquals(balanceAfterConfirm, balanceBefore);
		}	
		
		Thread.sleep(5);
		logger.info("[ --- Finished ConfirmPayment_TSC_"+testId+"--- ]");

	}
	
	@SuppressWarnings("unchecked")
	@Parameters({"testCaseId"})
	@Test
	void ConfirmPayment_TSC_046_047_048(String testId)throws InterruptedException, ParseException{
//		User already registered
//		User Logged in
//		Not Enough Balance
//		requestId is filled and unique
//		Confirm payment 5 min
		logger.info("[ --- Started ConfirmPayment_TSC_"+testId+" --- ]");
		
		Thread.sleep(300000);
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("transactionHistoryId", transactionHistoryId);
		RequestParams.put("paymentMethod", "Dana");
		RequestParams.put("requestId", generateRequestId());
		
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointConfirmPayment);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		JSONObject tempData = new JSONObject();
		tempData = (JSONObject) json.get("transactionHistory");
		JSONObject tempData2 = new JSONObject();
		tempData2 = (JSONObject) json.get("transactionDetail");
		
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		String responseisCompleted = tempData.get("isCompleted").toString();
		String responseid = tempData.get("id").toString();
		String responsetransactionResult = tempData2.get("transactionResult").toString();
		
		logger.info("Response Body : " + responseBody);
		logger.info("Response Status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		logger.info("Response isCompleted : " + responseisCompleted);
		
		// ASSERTION
		Assert.assertEquals(responseDescription, "Transaction already completed");
		Assert.assertEquals(responseStatus, "108");
		Assert.assertEquals(responseisCompleted, "true");
		Assert.assertEquals(responseid, String.valueOf(transactionHistoryId));
		Assert.assertEquals(responsetransactionResult, "2");
		
		if(responseStatus.toString().equals("108")) {
			
			RestAssured.baseURI = "https://dana-bussiness-member.herokuapp.com/v1";
			httpRequest = RestAssured.given();
			
			// PROVIDE JSON DATA AT REQUEST BODY
			RequestParams = new JSONObject();
			
			// SET COOKIE, REQUEST HEADER AND REQUEST BODY
			httpRequest.params("id", String.valueOf(userId));
			httpRequest.header("Content-Type", "application/json");
			httpRequest.body(RequestParams.toJSONString());
			
			// SET REQUEST METHOD AND API ENDPOINT
			response = httpRequest.request(Method.GET, endpointGetUser);
			
			// MANIPULATING RESPONSE DATA
			parser = new JSONParser();
			json = (JSONObject) parser.parse(response.getBody().asString());
			
			JSONObject tempData3 = new JSONObject();
			tempData3 = (JSONObject) json.get("userData");
			balanceAfterConfirm = Integer.parseInt(tempData3.get("balance").toString());
			logger.info("Balance after confirm payment : " + balanceAfterConfirm);
			Assert.assertEquals(balanceAfterConfirm, balanceBefore);
		}	
		Thread.sleep(5);
		logger.info("[ --- Finished ConfirmPayment_TSC_"+testId+"--- ]");

	}
	
	@SuppressWarnings("unchecked")
	@Parameters({"testCaseId"})
	@Test
	void ConfirmPayment_TSC_049_050_051(String testId)throws InterruptedException, ParseException{
//		User already registered
//		User Logged in
//		Not Enough Balance
//		requestId is filled but not unique
//		Confirm payment 5 min
		logger.info("[ --- Started ConfirmPayment_TSC_"+testId+"--- ]");
		
		Thread.sleep(300000);
		
		try {
			getRequestId2 = new File("./assets/generated-data/requestId2.txt");
			requestIdReader2 = new Scanner(getRequestId2);
			while (requestIdReader2.hasNextLine()) {
				String data = requestIdReader2.nextLine();
				requestId2 = data;
			}
		}catch (FileNotFoundException e) {
			System.out.println("An error occurred.");
		      e.printStackTrace();
		}
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("transactionHistoryId", transactionHistoryId);
		RequestParams.put("paymentMethod", "Dana");
		RequestParams.put("requestId", requestId2);
		
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointConfirmPayment);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		JSONObject tempData = new JSONObject();
		tempData = (JSONObject) json.get("transactionHistory");
		
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		String responseisCompleted = tempData.get("isCompleted").toString();
		
		logger.info("Response Body : " + responseBody);
		logger.info("Response Status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		logger.info("Response isCompleted : " + responseisCompleted);
		
		
		if(responseStatus.toString().equals("300")) {
			// ASSERTION
			Assert.assertEquals(responseDescription, responseStatusSuccess);
			Assert.assertEquals(responseisCompleted, "true");
			Assert.assertEquals(responseStatus, "300");
			
			RestAssured.baseURI = baseUrl;
			httpRequest = RestAssured.given();
			
			// PROVIDE JSON DATA AT REQUEST BODY
			RequestParams = new JSONObject();
			RequestParams.put("userId", userId);
			
			// SET COOKIE, REQUEST HEADER AND REQUEST BODY
			httpRequest.cookie("JSESSIONID", sessionId);
			httpRequest.header("Content-Type", "application/json");
			httpRequest.body(RequestParams.toJSONString());
			
			// SET REQUEST METHOD AND API ENDPOINT
			response = httpRequest.request(Method.GET, endpointViewProfile);
			
			// MANIPULATING RESPONSE DATA
			parser = new JSONParser();
			json = (JSONObject) parser.parse(response.getBody().asString());
			
			tempData = new JSONObject();
			tempData = (JSONObject) json.get("userData");
			balanceAfterConfirm = Integer.parseInt(tempData.get("balance").toString());
			logger.info("Balance after confirm payment : " + balanceAfterConfirm);
			Assert.assertEquals(true, balanceAfterConfirm<balanceBefore);
			
		}else if(responseStatus.toString().equals("105")) {
			// ASSERTION
			Assert.assertEquals(responseDescription, "Payment refund");
			Assert.assertEquals(responseisCompleted, "true");
			Assert.assertEquals(responseStatus, "105");
						
			RestAssured.baseURI = baseUrl;
			httpRequest = RestAssured.given();
						
			// PROVIDE JSON DATA AT REQUEST BODY
			RequestParams = new JSONObject();
			RequestParams.put("userId", userId);
						
			// SET COOKIE, REQUEST HEADER AND REQUEST BODY
			httpRequest.cookie("JSESSIONID", sessionId);
			httpRequest.header("Content-Type", "application/json");
			httpRequest.body(RequestParams.toJSONString());
						
			// SET REQUEST METHOD AND API ENDPOINT
			response = httpRequest.request(Method.GET, endpointViewProfile);
						
			// MANIPULATING RESPONSE DATA
			parser = new JSONParser();
			json = (JSONObject) parser.parse(response.getBody().asString());
						
			tempData = new JSONObject();
			tempData = (JSONObject) json.get("userData");
			balanceAfterConfirm = Integer.parseInt(tempData.get("balance").toString());
			logger.info("Balance after confirm payment : " + balanceAfterConfirm);
			
			Assert.assertEquals(balanceAfterConfirm, balanceBefore);
		}else if(responseStatus.toString().equals("104")){
			// ASSERTION
			Assert.assertEquals(responseDescription, "Payment failed");
			Assert.assertEquals(responseisCompleted, "true");
			Assert.assertEquals(responseStatus, "104");
						
			RestAssured.baseURI = baseUrl;
			httpRequest = RestAssured.given();
						
			// PROVIDE JSON DATA AT REQUEST BODY
			RequestParams = new JSONObject();
			RequestParams.put("userId", userId);
						
			// SET COOKIE, REQUEST HEADER AND REQUEST BODY
			httpRequest.cookie("JSESSIONID", sessionId);
			httpRequest.header("Content-Type", "application/json");
			httpRequest.body(RequestParams.toJSONString());
						
			// SET REQUEST METHOD AND API ENDPOINT
			response = httpRequest.request(Method.GET, endpointViewProfile);
						
			// MANIPULATING RESPONSE DATA
			parser = new JSONParser();
			json = (JSONObject) parser.parse(response.getBody().asString());
						
			tempData = new JSONObject();
			tempData = (JSONObject) json.get("userData");
			balanceAfterConfirm = Integer.parseInt(tempData.get("balance").toString());
			logger.info("Balance after confirm payment : " + balanceAfterConfirm);
			
			Assert.assertEquals(balanceAfterConfirm, balanceBefore);
			
		}
		
		
		Thread.sleep(5);
		logger.info("[ --- Finished ConfirmPayment_TSC_"+testId+" --- ]");

	}
	
	@SuppressWarnings("unchecked")
	@Parameters({"testCaseId"})
	@Test
	void ConfirmPayment_TSC_052_053_054(String testId)throws InterruptedException, ParseException{
//		User already registered
//		User Logged in
//		Not Enough Balance
//		requestId is null
//		Confirm payment 5 min
		logger.info("[ --- Started ConfirmPayment_TSC_"+testId+"--- ]");
		
		Thread.sleep(300000);
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("transactionHistoryId", transactionHistoryId);
		RequestParams.put("paymentMethod", "Dana");
		RequestParams.put("requestId", null);
		
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointConfirmPayment);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Response Body : " + responseBody);
		logger.info("Response Status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION
		Assert.assertEquals(responseDescription, "RequestBody attributes shouldn't be null");
		Assert.assertEquals(responseStatus, "106");
		
		if(responseStatus.toString().equals("106")) {
			
			RestAssured.baseURI = "https://dana-bussiness-member.herokuapp.com/v1";
			httpRequest = RestAssured.given();
			
			// PROVIDE JSON DATA AT REQUEST BODY
			RequestParams = new JSONObject();
			
			// SET COOKIE, REQUEST HEADER AND REQUEST BODY
			httpRequest.params("id", String.valueOf(userId));
			httpRequest.header("Content-Type", "application/json");
			httpRequest.body(RequestParams.toJSONString());
			
			// SET REQUEST METHOD AND API ENDPOINT
			response = httpRequest.request(Method.GET, endpointGetUser);
			
			// MANIPULATING RESPONSE DATA
			parser = new JSONParser();
			json = (JSONObject) parser.parse(response.getBody().asString());
			
			JSONObject tempData = new JSONObject();
			tempData = (JSONObject) json.get("userData");
			balanceAfterConfirm = Integer.parseInt(tempData.get("balance").toString());
			logger.info("Balance after confirm payment : " + balanceAfterConfirm);
			Assert.assertEquals(balanceAfterConfirm, balanceBefore);
		}	
		
		Thread.sleep(5);
		logger.info("[ --- Finished ConfirmPayment_TSC_"+testId+"--- ]");

	}
	
	@SuppressWarnings("unchecked")
	@Parameters({"testCaseId"})
	@Test
	void ConfirmPayment_TSC_055_056_057(String testId)throws InterruptedException, ParseException{
//		User already registered
//		User not Logged in
//		Enough Balance
//		requestId is filled and unique
//		Confirm payment 5 min
		logger.info("[ --- Started ConfirmPayment_TSC_"+testId+"--- ]");
		
		Thread.sleep(300000);
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("transactionHistoryId", transactionHistoryId);
		RequestParams.put("paymentMethod", "Dana");
		RequestParams.put("requestId", generateRequestId());
		
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointConfirmPayment);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Response Body : " + responseBody);
		logger.info("Response Status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION
		Assert.assertEquals(responseDescription, "USER_NOT_LOGGED_IN");
		Assert.assertEquals(responseStatus, "301");
		
		if(responseStatus.toString().equals("301")) {
			
			RestAssured.baseURI = "https://dana-bussiness-member.herokuapp.com/v1";
			httpRequest = RestAssured.given();
			
			// PROVIDE JSON DATA AT REQUEST BODY
			RequestParams = new JSONObject();
			
			// SET COOKIE, REQUEST HEADER AND REQUEST BODY
			httpRequest.params("id", String.valueOf(userId));
			httpRequest.header("Content-Type", "application/json");
			httpRequest.body(RequestParams.toJSONString());
			
			// SET REQUEST METHOD AND API ENDPOINT
			response = httpRequest.request(Method.GET, endpointGetUser);
			
			// MANIPULATING RESPONSE DATA
			parser = new JSONParser();
			json = (JSONObject) parser.parse(response.getBody().asString());
			
			JSONObject tempData = new JSONObject();
			tempData = (JSONObject) json.get("userData");
			balanceAfterConfirm = Integer.parseInt(tempData.get("balance").toString());
			logger.info("Balance after confirm payment : " + balanceAfterConfirm);
			Assert.assertEquals(balanceAfterConfirm, balanceBefore);
		}
		
		
		Thread.sleep(5);
		logger.info("[ --- Finished ConfirmPayment_TSC_"+testId+"--- ]");

	}
	
	@SuppressWarnings("unchecked")
	@Parameters({"testCaseId"})
	@Test
	void ConfirmPayment_TSC_058_059_060(String testId)throws InterruptedException, ParseException{
//		User already registered
//		User not Logged in
//		Enough Balance
//		requestId is filled but not unique
//		Confirm payment 5 min
		logger.info("[ --- Started ConfirmPayment_TSC_"+testId+"--- ]");
		
		logger.info("Logged out user with ID : " + userId + " and JSESSION ID " + sessionId);
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParamsLogout = new JSONObject();
		
		RequestParamsLogout.put("userId", userId);
		
		httpRequest.cookie("JSESSIONID", sessionId);
		
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParamsLogout.toJSONString());
		response = httpRequest.request(Method.POST, endpointLogout);
		
		JSONParser parserLogout = new JSONParser();
		JSONObject jsonLogout = (JSONObject) parserLogout.parse(response.getBody().asString());
		String responseHeaderLogout = response.getHeader("Set-Cookie");
		String responseBodyLogout = response.getBody().asString();
		logger.info("Response Body : " + responseBodyLogout);
		
		String responseCodeLogout = jsonLogout.get("code").toString();
		String responseDescriptionLogout = jsonLogout.get("description").toString();
		
		logger.info("Response Header : " + responseHeaderLogout);
		logger.info("Response code : " + responseCodeLogout);
		logger.info("Response Description : " + responseDescriptionLogout);
		
		
		// ASSERTION
		Assert.assertEquals(responseDescriptionLogout, "SUCCESS");
		Assert.assertEquals(responseCodeLogout.toString(), "300");
		
		
		logger.info("[ --- Started ConfirmPayment_TSC_"+testId+"--- ]");

		Thread.sleep(300000);
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("transactionHistoryId", transactionHistoryId);
		RequestParams.put("paymentMethod", "Dana");
		RequestParams.put("requestId", requestId);
		
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointConfirmPayment);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Response Body : " + responseBody);
		logger.info("Response Status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION
		Assert.assertEquals(responseDescription, "USER_NOT_LOGGED_IN");
		Assert.assertEquals(responseStatus, "301");
		
		Thread.sleep(5);
		logger.info("[ --- Finished ConfirmPayment_TSC_"+testId+"--- ]");

	}
	
	@SuppressWarnings("unchecked")
	@Parameters({"testCaseId"})
	@Test
	void ConfirmPayment_TSC_061_062_063(String testId)throws InterruptedException, ParseException{
//		User already registered
//		User not Logged in
//		Enough Balance
//		requestId is null
//		Confirm payment 5 min
		logger.info("[ --- Started ConfirmPayment_TSC_"+testId+"--- ]");
		
		Thread.sleep(300000);
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("transactionHistoryId", transactionHistoryId);
		RequestParams.put("paymentMethod", "Dana");
		RequestParams.put("requestId", null);
		
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointConfirmPayment);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Response Body : " + responseBody);
		logger.info("Response Status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION
		Assert.assertEquals(responseDescription, "USER_NOT_LOGGED_IN");
		Assert.assertEquals(responseStatus, "301");
		
		if(responseStatus.toString().equals("301")) {
			
			RestAssured.baseURI = "https://dana-bussiness-member.herokuapp.com/v1";
			httpRequest = RestAssured.given();
			
			// PROVIDE JSON DATA AT REQUEST BODY
			RequestParams = new JSONObject();
			
			// SET COOKIE, REQUEST HEADER AND REQUEST BODY
			httpRequest.params("id", String.valueOf(userId));
			httpRequest.header("Content-Type", "application/json");
			httpRequest.body(RequestParams.toJSONString());
			
			// SET REQUEST METHOD AND API ENDPOINT
			response = httpRequest.request(Method.GET, endpointGetUser);
			
			// MANIPULATING RESPONSE DATA
			parser = new JSONParser();
			json = (JSONObject) parser.parse(response.getBody().asString());
			
			JSONObject tempData = new JSONObject();
			tempData = (JSONObject) json.get("userData");
			balanceAfterConfirm = Integer.parseInt(tempData.get("balance").toString());
			logger.info("Balance after confirm payment : " + balanceAfterConfirm);
			Assert.assertEquals(balanceAfterConfirm, balanceBefore);
		}
		
		Thread.sleep(5);
		logger.info("[ --- Finished ConfirmPayment_TSC_"+testId+"--- ]");

	}
	
	@SuppressWarnings("unchecked")
	@Parameters({"testCaseId"})
	@Test
	void ConfirmPayment_TSC_064_065_066(String testId)throws InterruptedException, ParseException{
//		User already registered
//		User not Logged in
//		Not Enough Balance
//		requestId is filled and unique
//		Confirm payment 5 min
		logger.info("[ --- Started ConfirmPayment_TSC_"+testId+" --- ]");
		
		Thread.sleep(300000);
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("transactionHistoryId", transactionHistoryId);
		RequestParams.put("paymentMethod", "Dana");
		RequestParams.put("requestId", generateRequestId());
		
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointConfirmPayment);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Response Body : " + responseBody);
		logger.info("Response Status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION
		Assert.assertEquals(responseDescription, "USER_NOT_LOGGED_IN");
		Assert.assertEquals(responseStatus, "301");
		
		if(responseStatus.toString().equals("301")) {
			
			RestAssured.baseURI = "https://dana-bussiness-member.herokuapp.com/v1";
			httpRequest = RestAssured.given();
			
			// PROVIDE JSON DATA AT REQUEST BODY
			RequestParams = new JSONObject();
			
			// SET COOKIE, REQUEST HEADER AND REQUEST BODY
			httpRequest.params("id", String.valueOf(userId));
			httpRequest.header("Content-Type", "application/json");
			httpRequest.body(RequestParams.toJSONString());
			
			// SET REQUEST METHOD AND API ENDPOINT
			response = httpRequest.request(Method.GET, endpointGetUser);
			
			// MANIPULATING RESPONSE DATA
			parser = new JSONParser();
			json = (JSONObject) parser.parse(response.getBody().asString());
			
			JSONObject tempData = new JSONObject();
			tempData = (JSONObject) json.get("userData");
			balanceAfterConfirm = Integer.parseInt(tempData.get("balance").toString());
			logger.info("Balance after confirm payment : " + balanceAfterConfirm);
			Assert.assertEquals(balanceAfterConfirm, balanceBefore);
		}
		
		Thread.sleep(5);
		logger.info("[ --- Finished ConfirmPayment_TSC_"+testId+"--- ]");

	}
	
	@SuppressWarnings("unchecked")
	@Parameters({"testCaseId"})
	@Test
	void ConfirmPayment_TSC_067_068_069(String testId)throws InterruptedException, ParseException{
//		User already registered
//		User not Logged in
//		Not Enough Balance
//		requestId is filled but not unique
//		Confirm payment 5 min
		logger.info("[ --- Started ConfirmPayment_TSC_"+testId+"--- ]");
		
		
		logger.info("Logged out user with ID : " + userId + " and JSESSION ID " + sessionId);
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParamsLogout = new JSONObject();
		
		RequestParamsLogout.put("userId", userId);
		
		httpRequest.cookie("JSESSIONID", sessionId);
		
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParamsLogout.toJSONString());
		response = httpRequest.request(Method.POST, endpointLogout);
		
		JSONParser parserLogout = new JSONParser();
		JSONObject jsonLogout = (JSONObject) parserLogout.parse(response.getBody().asString());
		String responseHeaderLogout = response.getHeader("Set-Cookie");
		String responseBodyLogout = response.getBody().asString();
		logger.info("Response Body : " + responseBodyLogout);
		
		String responseCodeLogout = jsonLogout.get("code").toString();
		String responseDescriptionLogout = jsonLogout.get("description").toString();
		
		logger.info("Response Header : " + responseHeaderLogout);
		logger.info("Response code : " + responseCodeLogout);
		logger.info("Response Description : " + responseDescriptionLogout);
		
		
		// ASSERTION
		Assert.assertEquals(responseDescriptionLogout, "SUCCESS");
		Assert.assertEquals(responseCodeLogout.toString(), "300");
		
		
		logger.info("[ --- Started ConfirmPayment_TSC_"+testId+"--- ]");
		
		Thread.sleep(300000);
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("transactionHistoryId", transactionHistoryId);
		RequestParams.put("paymentMethod", "Dana");
		RequestParams.put("requestId", requestId);
		
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointConfirmPayment);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Response Body : " + responseBody);
		logger.info("Response Status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION
		Assert.assertEquals(responseDescription, "USER_NOT_LOGGED_IN");
		Assert.assertEquals(responseStatus, "301");
		
		if(responseStatus.toString().equals("301")) {
			
			RestAssured.baseURI = "https://dana-bussiness-member.herokuapp.com/v1";
			httpRequest = RestAssured.given();
			
			// PROVIDE JSON DATA AT REQUEST BODY
			RequestParams = new JSONObject();
			
			// SET COOKIE, REQUEST HEADER AND REQUEST BODY
			httpRequest.params("id", String.valueOf(userId));
			httpRequest.header("Content-Type", "application/json");
			httpRequest.body(RequestParams.toJSONString());
			
			// SET REQUEST METHOD AND API ENDPOINT
			response = httpRequest.request(Method.GET, endpointGetUser);
			
			// MANIPULATING RESPONSE DATA
			parser = new JSONParser();
			json = (JSONObject) parser.parse(response.getBody().asString());
			
			JSONObject tempData = new JSONObject();
			tempData = (JSONObject) json.get("userData");
			balanceAfterConfirm = Integer.parseInt(tempData.get("balance").toString());
			logger.info("Balance after confirm payment : " + balanceAfterConfirm);
			Assert.assertEquals(balanceAfterConfirm, balanceBefore);
		}
		
		Thread.sleep(5);
		logger.info("[ --- Finished ConfirmPayment_TSC_"+testId+"--- ]");


	}
	
	@SuppressWarnings("unchecked")
	@Parameters({"testCaseId"})
	@Test
	void ConfirmPayment_TSC_070_071_072(String testId)throws InterruptedException, ParseException{
//		User already registered
//		User not Logged in
//		Not Enough Balance
//		requestId is null
//		Confirm payment 5 min
		logger.info("[ --- Started ConfirmPayment_TSC_"+testId+" --- ]");
		
		Thread.sleep(300000);
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("transactionHistoryId", transactionHistoryId);
		RequestParams.put("paymentMethod", "Dana");
		RequestParams.put("requestId", null);
		
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointConfirmPayment);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Response Body : " + responseBody);
		logger.info("Response Status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION
		Assert.assertEquals(responseDescription, "USER_NOT_LOGGED_IN");
		Assert.assertEquals(responseStatus, "301");
		
		if(responseStatus.toString().equals("301")) {
			
			RestAssured.baseURI = "https://dana-bussiness-member.herokuapp.com/v1";
			httpRequest = RestAssured.given();
			
			// PROVIDE JSON DATA AT REQUEST BODY
			RequestParams = new JSONObject();
			
			// SET COOKIE, REQUEST HEADER AND REQUEST BODY
			httpRequest.params("id", String.valueOf(userId));
			httpRequest.header("Content-Type", "application/json");
			httpRequest.body(RequestParams.toJSONString());
			
			// SET REQUEST METHOD AND API ENDPOINT
			response = httpRequest.request(Method.GET, endpointGetUser);
			
			// MANIPULATING RESPONSE DATA
			parser = new JSONParser();
			json = (JSONObject) parser.parse(response.getBody().asString());
			
			JSONObject tempData = new JSONObject();
			tempData = (JSONObject) json.get("userData");
			balanceAfterConfirm = Integer.parseInt(tempData.get("balance").toString());
			logger.info("Balance after confirm payment : " + balanceAfterConfirm);
			Assert.assertEquals(balanceAfterConfirm, balanceBefore);
		}
		
		Thread.sleep(5);
		logger.info("[ --- Finished ConfirmPayment_TSC_"+testId+"--- ]");

	}
	
	@AfterClass
	void tearDown() {
		logger.info("[ --- Finished TestSuite_ConfirmPayment --- ]");
		System.out.println();
	}
}
