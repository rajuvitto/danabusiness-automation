package testCases;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Properties;
import java.util.Scanner;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import io.restassured.RestAssured;
import io.restassured.http.Method;
import models.User;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import testBase.TestBase;

public class TestSuite_Register extends TestBase {
	final static Logger logger = Logger.getLogger(TestSuite_Register.class);
	String baseUrl = "";
	String responseStatusSuccess ="";
	String responseStatusFailed = "";
	String endpointRegister = "/member/user/register";
	String endPointGetAllUser = "/user/getAllUser";
	String randomStringNumber = generateRandomStringNumberForEmail();
	String nameToRegister = "John " + randomStringNumber;
	String phoneToRegister = getRandomPhone();
	String emailToRegister = "John" + randomStringNumber + "@gmail.com";
	String passwordToRegister = "Password@00";
	String confirmPasswordToRegister = "Password@00";
	String existingUserPhone = "";
	String existingUserEmail = "";
	Properties prop = new Properties();
	InputStream input = null;
	private User userData, userDataWithoutEmail, userDataWithoutName, userDataWithoutPhone, userDataNotValidPassword, userDataPasswordAndConfirmPasswordNotMatch;
	File getUserPhone, getUserEmail;
	Scanner userPhoneReader, userEmailReader;
	
	@BeforeClass
	@Parameters({"baseUrl", "responseStatusSuccess", "responseStatusFailed"})
	void startTest(String definedBaseUrl, String definedResponseStatusSuccess, String definedResponseStatusFailed) {
		userData = new User.UserBuilder().email(emailToRegister).name(nameToRegister).password(passwordToRegister).confirmPassword(confirmPasswordToRegister).phone(phoneToRegister).build();
		userDataWithoutName = new User.UserBuilder().email(emailToRegister).password(passwordToRegister).confirmPassword(confirmPasswordToRegister).phone(phoneToRegister).build();
		userDataWithoutEmail = new User.UserBuilder().name(nameToRegister).password(passwordToRegister).confirmPassword(confirmPasswordToRegister).phone(phoneToRegister).build();
		userDataWithoutPhone = new User.UserBuilder().email(emailToRegister).name(nameToRegister).password(passwordToRegister).confirmPassword(confirmPasswordToRegister).build();
		userDataNotValidPassword = new User.UserBuilder().email(emailToRegister).name(nameToRegister).phone(phoneToRegister).build();
		userDataPasswordAndConfirmPasswordNotMatch = new User.UserBuilder().email(emailToRegister).name(nameToRegister).password("Password@00").confirmPassword("Password@01").phone(phoneToRegister).build();
		baseUrl = definedBaseUrl;
		responseStatusSuccess = definedResponseStatusSuccess;
		responseStatusFailed = definedResponseStatusFailed;
		try {
			input = new FileInputStream("config.properties");
			
			// LOAD PROPERTIES FILE
			prop.load(input);
		
		}catch(IOException ex) {
			ex.printStackTrace();
		}finally {
			if(input!=null) {
				try {
					input.close();
				}catch(IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		logger.info("[ --- Started TestSuite_Register --- ]");
	}
	
	@SuppressWarnings("unchecked")
	@Test(priority=0)
	@Parameters({"registerType"})
	void Register_TSC_001(String type) throws InterruptedException, ParseException{
		if(type.equals("allParametersFilled")) {
			logger.info("[ --- Started Register_TSC_001 --- ]");
		}else {
			logger.info("[ --- Started "+type+" --- ]");
		}
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		if(type.equals("allParametersFilled")) {
			RequestParams.put("name", userData.getName());
			RequestParams.put("password", userData.getPassword());
			RequestParams.put("confirmPassword", userData.getConfirmPassword());
			RequestParams.put("phone", userData.getPhone());
			RequestParams.put("email", userData.getEmail());
			RequestParams.put("requestId", generateRequestId());
		}else if(type.equals("Register_TSC_002")) {
			RequestParams.put("name", userDataWithoutName.getName());
			RequestParams.put("password", userDataWithoutName.getPassword());
			RequestParams.put("confirmPassword", userDataWithoutName.getConfirmPassword());
			RequestParams.put("phone", userDataWithoutName.getPhone());
			RequestParams.put("email", userDataWithoutName.getEmail());
			RequestParams.put("requestId", generateRequestId());
		}else if(type.equals("Register_TSC_003")) {
			RequestParams.put("name", userDataWithoutEmail.getName());
			RequestParams.put("password", userDataWithoutEmail.getPassword());
			RequestParams.put("confirmPassword", userDataWithoutEmail.getConfirmPassword());
			RequestParams.put("phone", userDataWithoutEmail.getPhone());
			RequestParams.put("email", userDataWithoutEmail.getEmail());
			RequestParams.put("requestId", generateRequestId());
		}else if(type.equals("Register_TSC_004")) {
			RequestParams.put("name", userDataWithoutPhone.getName());
			RequestParams.put("password", userDataWithoutPhone.getPassword());
			RequestParams.put("confirmPassword", userDataWithoutPhone.getConfirmPassword());
			RequestParams.put("phone", userDataWithoutPhone.getPhone());
			RequestParams.put("email", userDataWithoutPhone.getEmail());
			RequestParams.put("requestId", generateRequestId());
		}else if(type.equals("Register_TSC_005")) {
			RequestParams.put("name", userDataNotValidPassword.getName());
			RequestParams.put("password", "pass");
			RequestParams.put("confirmPassword", "pass");
			RequestParams.put("phone", userDataNotValidPassword.getPhone());
			RequestParams.put("email", userDataNotValidPassword.getEmail());
			RequestParams.put("requestId", generateRequestId());
		}else if(type.equals("Register_TSC_006")) {
			RequestParams.put("name", userDataNotValidPassword.getName());
			RequestParams.put("password", "password");
			RequestParams.put("confirmPassword", "pass");
			RequestParams.put("phone", userDataNotValidPassword.getPhone());
			RequestParams.put("email", userDataNotValidPassword.getEmail());
			RequestParams.put("requestId", generateRequestId());
		}else if(type.equals("Register_TSC_007")) {
			RequestParams.put("name", userDataPasswordAndConfirmPasswordNotMatch.getName());
			RequestParams.put("password", userDataPasswordAndConfirmPasswordNotMatch.getPassword());
			RequestParams.put("confirmPassword", userDataPasswordAndConfirmPasswordNotMatch.getConfirmPassword());
			RequestParams.put("phone", userDataPasswordAndConfirmPasswordNotMatch.getPhone());
			RequestParams.put("email", userDataPasswordAndConfirmPasswordNotMatch.getEmail());
			RequestParams.put("requestId", generateRequestId());
		}
		
		
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointRegister);
		String responseBody = response.getBody().asString();
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		String responseCode = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Response Body : "+responseBody);
		logger.info("Response Code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION
		if(type.equals("allParametersFilled")) {
			Assert.assertEquals(responseDescription, "New user successfully added");
			Assert.assertEquals(responseCode, "300");
			
			JSONObject tempData = new JSONObject();
			tempData = (JSONObject) json.get("userData");
			
			Assert.assertEquals(tempData.get("name").toString(), nameToRegister);
			Assert.assertEquals(tempData.get("phone").toString(), phoneToRegister);
			Assert.assertEquals(tempData.get("email").toString(), emailToRegister);
			Assert.assertEquals(Integer.parseInt(tempData.get("balance").toString()), 0);

			if(responseCode.toString().equals("300")) {
				// DATABASE CONNECTION
				try {
					Class.forName("com.mysql.cj.jdbc.Driver");
					Connection con = DriverManager.getConnection("jdbc:mysql://"+prop.getProperty("dbHost")+":"+prop.getProperty("dbPort")+"/"+prop.getProperty("dbName")+"?useSSL=false", prop.getProperty("dbUsername"), prop.getProperty("dbPassword"));
					System.out.println(con.toString());
					Statement stmt = con.createStatement();
					ResultSet rs = stmt.executeQuery("SELECT * FROM USER");
					while (rs.next())
						System.out.println(rs.getInt(1));
					con.close();
				}catch(Exception e) {
					System.out.println(e);
				}
				
				// CHECK DATA IN DB
				RestAssured.baseURI = "https://dana-bussiness-member.herokuapp.com/v1";
				httpRequest = RestAssured.given();
				response = httpRequest.request(Method.GET, endPointGetAllUser);
				
				
				parser = new JSONParser();
				json = (JSONObject) parser.parse(response.getBody().asString());
				responseCode = json.get("code").toString();
				responseDescription = json.get("description").toString();
				
				Assert.assertEquals(responseCode.toString(), "200");
				Assert.assertEquals(responseDescription, responseStatusSuccess);
				
				JSONArray jsonarray = new JSONArray();
				jsonarray = (JSONArray) json.get("allUser");
				
				String dbCheckPoint = "Data user is not in database";
				for(int i = 0; i <=jsonarray.size()-1; i++) {
					JSONObject allUserData = new JSONObject();
					allUserData = (JSONObject) jsonarray.get(i);
					if(allUserData.get("email").toString().equals(userData.getEmail()) && allUserData.get("phone").toString().equals(userData.getPhone()) && tempData.get("name").toString().equals(userData.getName())) {
						dbCheckPoint = "Data user is in database";
					};
				}
				Assert.assertEquals(dbCheckPoint, "Data user is in database");
				logger.info("=== REGISTER SUCCESS ===");
				logger.info("ID : " + tempData.get("id").toString());
				logger.info("Name : " + tempData.get("name").toString());
				logger.info("Phone : " + tempData.get("phone").toString());
				logger.info("Email : " + tempData.get("email").toString());
				logger.info("Password : " + tempData.get("password").toString());
				logger.info("Balance : " + tempData.get("balance").toString());
				
				// Save data to file sessionId.txt
				try {
	                FileWriter generateUserId = new FileWriter("assets/generated-data/userId.txt");
	                FileWriter generateUserBalance = new FileWriter("assets/generated-data/userBalance.txt");
	                FileWriter generateUserName = new FileWriter("assets/generated-data/userName.txt");
	                FileWriter generateUserPhone = new FileWriter("assets/generated-data/userPhone.txt");
	                FileWriter generateUserEmail = new FileWriter("assets/generated-data/userEmail.txt");
	                FileWriter generateUserPassword = new FileWriter("assets/generated-data/userPassword.txt");
	                generateUserId.write(String.valueOf(tempData.get("id").toString()));
	                generateUserBalance.write(String.valueOf(tempData.get("balance").toString()));
	                generateUserName.write(String.valueOf(tempData.get("name").toString()));
	                generateUserPhone.write(String.valueOf(tempData.get("phone").toString()));
	                generateUserEmail.write(String.valueOf(tempData.get("email").toString()));
	                generateUserPassword.write(String.valueOf(tempData.get("password").toString()));
	                generateUserId.close();
	                generateUserBalance.close();
	                generateUserName.close();
	                generateUserPhone.close();
	                generateUserEmail.close();
	                generateUserPassword.close();
	            } catch (IOException e) {
	                System.err.format("IOException: %s%n", e);
	            }
					
			}else {
				logger.info("=== REGISTER FAILED ===");
			}	
		
		}else if(type.equals("Register_TSC_002") || type.equals("Register_TSC_003") || type.equals("Register_TSC_004")) {
			Assert.assertEquals(responseDescription, "Database Insertion Error");
			Assert.assertEquals(responseCode, "205");
		}else if(type.equals("Register_TSC_005") || type.equals("Register_TSC_006")) {
			Assert.assertEquals(responseDescription, "Invalid email or password format");
			Assert.assertEquals(responseCode, "206");
			// CHECK DATA IN DB
			RestAssured.baseURI = "https://dana-bussiness-member.herokuapp.com/v1";
			httpRequest = RestAssured.given();
			response = httpRequest.request(Method.GET, endPointGetAllUser);
			
			
			parser = new JSONParser();
			json = (JSONObject) parser.parse(response.getBody().asString());
			responseCode = json.get("code").toString();
			responseDescription = json.get("description").toString();
			
			Assert.assertEquals(responseCode.toString(), "200");
			Assert.assertEquals(responseDescription, responseStatusSuccess);
			
			JSONArray jsonarray = new JSONArray();
			jsonarray = (JSONArray) json.get("allUser");
			
			String dbCheckPoint = "Data user is not in database";
			for(int i = 0; i <=jsonarray.size()-1; i++) {
				JSONObject allUserData = new JSONObject();
				allUserData = (JSONObject) jsonarray.get(i);
				if(allUserData.get("email").toString().equals(userDataNotValidPassword.getEmail()) && allUserData.get("phone").toString().equals(userDataNotValidPassword.getPhone()) && allUserData.get("name").toString().equals(userDataNotValidPassword.getName())) {
					dbCheckPoint = "Data user is in database";
				};
			}
			Assert.assertEquals(dbCheckPoint, "Data user is not in database");
		}else if(type.equals("Register_TSC_007")) {
			Assert.assertEquals(responseDescription, "Password and Confirm Password not match");
		}else if(type.equals("Register_TSC_008")) {
			Assert.assertEquals(responseDescription, "Phone number already registered");
		}
		
		if(type.equals("allParametersFilled")) {
			logger.info("[ --- Finished Register_TSC_001 --- ]");
		}else {
			logger.info("[ --- Finished "+type+" --- ]");
		}
		
		Thread.sleep(5);
	}
	
	@SuppressWarnings("unchecked")
	@Test(priority=1)
	void Register_TSC_008() throws InterruptedException, ParseException{
		logger.info("[ --- Started Register_TSC_008 --- ]");
		try {
			getUserPhone = new File("./assets/generated-data/userPhone.txt");
			userPhoneReader = new Scanner(getUserPhone);
			while (userPhoneReader.hasNextLine()) {
				String data = userPhoneReader.nextLine();
				existingUserPhone = data;
			}
		}catch (FileNotFoundException e) {
			System.out.println("An error occurred.");
		      e.printStackTrace();
		}
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		RequestParams.put("name", userData.getName());
		RequestParams.put("password", userData.getPassword());
		RequestParams.put("confirmPassword", userData.getConfirmPassword());
		RequestParams.put("phone", existingUserPhone);
		RequestParams.put("email", userData.getEmail());
		RequestParams.put("requestId", generateRequestId());
		
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointRegister);
		String responseBody = response.getBody().asString();
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		String responseCode = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Response Body : "+responseBody);
		logger.info("Response Code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION
		Assert.assertEquals(responseDescription, "Phone number already registered");
		
		// CHECK DATA IN DB
		RestAssured.baseURI = "https://dana-bussiness-member.herokuapp.com/v1";
		httpRequest = RestAssured.given();
		response = httpRequest.request(Method.GET, endPointGetAllUser);
		
		
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		responseCode = json.get("code").toString();
		responseDescription = json.get("description").toString();
		
		Assert.assertEquals(responseCode.toString(), "200");
		Assert.assertEquals(responseDescription, responseStatusSuccess);
		
		JSONArray jsonarray = new JSONArray();
		jsonarray = (JSONArray) json.get("allUser");
		
		String dbCheckPoint = "Data user is not in database";
		for(int i = 0; i <=jsonarray.size()-1; i++) {
			JSONObject allUserData = new JSONObject();
			allUserData = (JSONObject) jsonarray.get(i);
			if(allUserData.get("email").toString().equals(userData.getEmail()) && allUserData.get("phone").toString().equals(existingUserPhone) && allUserData.get("name").toString().equals(userData.getName())) {
				dbCheckPoint = "Data user is in database";
			};
		}
		Assert.assertEquals(dbCheckPoint, "Data user is not in database");
		logger.info("[ --- Finished Register_TSC_008 --- ]");
		Thread.sleep(5);
	}
	
	@SuppressWarnings("unchecked")
	@Test(priority=1)
	void Register_TSC_009() throws InterruptedException, ParseException{
		logger.info("[ --- Started Register_TSC_009 --- ]");
		try {
			getUserEmail = new File("./assets/generated-data/userEmail.txt");
			userEmailReader = new Scanner(getUserEmail);
			while (userEmailReader.hasNextLine()) {
				String data = userEmailReader.nextLine();
				existingUserEmail = data;
			}
		}catch (FileNotFoundException e) {
			System.out.println("An error occurred.");
		      e.printStackTrace();
		}
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		RequestParams.put("name", userData.getName());
		RequestParams.put("password", userData.getPassword());
		RequestParams.put("confirmPassword", userData.getConfirmPassword());
		RequestParams.put("phone", userData.getPhone());
		RequestParams.put("email", existingUserEmail);
		RequestParams.put("requestId", generateRequestId());
		
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointRegister);
		String responseBody = response.getBody().asString();
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		String responseCode = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Response Body : "+responseBody);
		logger.info("Response Code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION
		Assert.assertEquals(responseDescription, "Email already registered");
		
		// CHECK DATA IN DB
		RestAssured.baseURI = "https://dana-bussiness-member.herokuapp.com/v1";
		httpRequest = RestAssured.given();
		response = httpRequest.request(Method.GET, endPointGetAllUser);
		
		
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		responseCode = json.get("code").toString();
		responseDescription = json.get("description").toString();
		
		Assert.assertEquals(responseCode.toString(), "200");
		Assert.assertEquals(responseDescription, responseStatusSuccess);
		
		JSONArray jsonarray = new JSONArray();
		jsonarray = (JSONArray) json.get("allUser");
		
		String dbCheckPoint = "Data user is not in database";
		for(int i = 0; i <=jsonarray.size()-1; i++) {
			JSONObject allUserData = new JSONObject();
			allUserData = (JSONObject) jsonarray.get(i);
			if(allUserData.get("email").toString().equals(existingUserEmail) && allUserData.get("phone").toString().equals(userData.getPhone()) && allUserData.get("name").toString().equals(userData.getName())) {
				dbCheckPoint = "Data user is in database";
			};
		}
		Assert.assertEquals(dbCheckPoint, "Data user is not in database");
		logger.info("[ --- Finished Register_TSC_009 --- ]");
		Thread.sleep(5);
	}
	
	@SuppressWarnings("unchecked")
	@Test(priority=1)
	void Register_TSC_010() throws InterruptedException, ParseException{
		logger.info("[ --- Started Register_TSC_010 --- ]");
		try {
			getUserEmail = new File("./assets/generated-data/userEmail.txt");
			getUserPhone = new File("./assets/generated-data/userPhone.txt");
			userEmailReader = new Scanner(getUserEmail);
			while (userEmailReader.hasNextLine()) {
				String data = userEmailReader.nextLine();
				existingUserEmail = data;
			}
			userPhoneReader = new Scanner(getUserPhone);
			while (userPhoneReader.hasNextLine()) {
				String data = userPhoneReader.nextLine();
				existingUserPhone = data;
			}
		}catch (FileNotFoundException e) {
			System.out.println("An error occurred.");
		      e.printStackTrace();
		}
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		RequestParams.put("name", userData.getName());
		RequestParams.put("password", userData.getPassword());
		RequestParams.put("confirmPassword", userData.getConfirmPassword());
		RequestParams.put("phone", existingUserPhone);
		RequestParams.put("email", existingUserEmail);
		RequestParams.put("requestId", generateRequestId());
		
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointRegister);
		String responseBody = response.getBody().asString();
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		String responseCode = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Response Body : "+responseBody);
		logger.info("Response Code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION
		Assert.assertEquals(responseDescription, "Phone number already registered");
		
		// CHECK DATA IN DB
		RestAssured.baseURI = "https://dana-bussiness-member.herokuapp.com/v1";
		httpRequest = RestAssured.given();
		response = httpRequest.request(Method.GET, endPointGetAllUser);
		
		
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		responseCode = json.get("code").toString();
		responseDescription = json.get("description").toString();
		
		Assert.assertEquals(responseCode.toString(), "200");
		Assert.assertEquals(responseDescription, responseStatusSuccess);
		
		JSONArray jsonarray = new JSONArray();
		jsonarray = (JSONArray) json.get("allUser");
		
		String dbCheckPoint = "Data user is not in database";
		for(int i = 0; i <=jsonarray.size()-1; i++) {
			JSONObject allUserData = new JSONObject();
			allUserData = (JSONObject) jsonarray.get(i);
			if(allUserData.get("email").toString().equals(existingUserEmail) && allUserData.get("phone").toString().equals(userData.getPhone()) && allUserData.get("name").toString().equals(userData.getName())) {
				dbCheckPoint = "Data user is in database";
			};
		}
		Assert.assertEquals(dbCheckPoint, "Data user is not in database");
		logger.info("[ --- Finished Register_TSC_010 --- ]");
		Thread.sleep(5);
	}
	
	@AfterClass
	void tearDown() {
		logger.info("[ --- Finished TestSuite_Register --- ]");
		System.out.println();
	}
}