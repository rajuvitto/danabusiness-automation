package testCases;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import io.restassured.RestAssured;
import io.restassured.http.Method;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import testBase.TestBase;

public class TestSuite_TopUp extends TestBase {
	final static Logger logger = Logger.getLogger(TestSuite_TopUp.class);
	String baseUrl = "";
	String endpointTopUp = "/payment/topup";
	String endpointViewProfile = "/member/user/viewProfile";
	String endpointLogin = "/member/user/login";
	String endpointLogout = "/member/user/logout";
	String responseStatusSuccess ="";
	String responseStatusFailed = "";
	String responseStatusTopUpUnderMin = "Top up amount under minimum";
	String responseStatusTopUpOverMax = "Top up amount over maximum";
	String responseStatusUserNotLoggedIn = "USER_NOT_LOGGED_IN";
	String responseBalanceExceedMax = "Balance exceed maximum";
	String responseStatusNullParams = "RequestBody attributes shouldn't be null";
	long userId;
	String requestId = "";
	String sessionId = "";
	String userPhone = "";
	String userPassword = "";
	File getUserId, getSessionId, getRequestId, getUserPhone, getUserPassword;
	Scanner userIdReader, sessionIdReader, requestIdReader, userPhoneReader, userPasswordReader;
	String staticRequestId = generateRequestId();
	Integer balanceBeforeTopUp = 0;
	Integer balanceAfterTopUp = 0;
	
//	String staticRequestIdForTopUpOverMax = generateRequestId();
//	String staticRequestIdForTopUpUnderMin = generateRequestId();
	
	
	@BeforeClass
	@Parameters({"baseUrl", "responseStatusSuccess", "responseStatusFailed"})
	void startTest(String definedBaseUrl, String definedResponseStatusSuccess, String definedResponseStatusFailed) {
		baseUrl = definedBaseUrl;
		responseStatusSuccess = definedResponseStatusSuccess;
		responseStatusFailed = definedResponseStatusFailed;
		logger.info("[ --- Started TestSuite_TopUp --- ]");
		
		try {
			getUserId = new File("./assets/generated-data/userId.txt");
			getSessionId = new File("./assets/generated-data/sessionId.txt");
			getUserPhone = new File("./assets/generated-data/userPhone.txt");
			getUserPassword = new File("./assets/generated-data/userPassword.txt");
			userIdReader = new Scanner(getUserId);
			sessionIdReader = new Scanner(getSessionId);
			userPhoneReader = new Scanner(getUserPhone);
			userPasswordReader = new Scanner(getUserPassword);
			while (userIdReader.hasNextLine()) {
				String data = userIdReader.nextLine();
				userId = Long.parseLong(data);
			}
			
			while (sessionIdReader.hasNextLine()) {
				String data = sessionIdReader.nextLine();
				sessionId = data;
			}
			
			while (userPhoneReader.hasNextLine()) {
				String data = userPhoneReader.nextLine();
				userPhone = data;
			}
			
			while (userPasswordReader.hasNextLine()) {
				String data = userPasswordReader.nextLine();
				userPassword = data;
			}
		}catch (FileNotFoundException e) {
			System.out.println("An error occurred.");
		      e.printStackTrace();
		}
	}
	
	@SuppressWarnings("unchecked")
	@Parameters({"amountToTopUp"})
	@Test(priority=0)
	void TopUp_PositiveCase(Integer amount) throws InterruptedException, ParseException{
//		- User already registered
//		- User logged in
//		- Current balance under Rp.2.000.000
//		- Correct amount to top up Rp.1000
//		- Balance after top up is or under Rp.2.000.000
//		- Idempotency key is filled and unique
		logger.info("[ --- Started TopUp_PositiveCase --- ]");	
		
		// === GET BALANCE BEFORE TOP UP ===
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		JSONObject RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		JSONObject tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceBeforeTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance before top up : " + balanceBeforeTopUp);
		
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("amount", amount);
		RequestParams.put("requestId", staticRequestId);
		
		try {
            FileWriter generateSessionId = new FileWriter("assets/generated-data/requestId.txt");
            generateSessionId.write(staticRequestId);
            generateSessionId.close();
        } catch (IOException e) {
            System.err.format("IOException: %s%n", e);
        }
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		String responseCode = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		logger.info("Top Up : Rp."+amount);
		
		// ASSERTION
		Assert.assertEquals(responseDescription, responseStatusSuccess);
		Assert.assertEquals(responseCode.toString(), "300");
		
		if(responseCode.toString().equals("300")) {
			
			logger.info("=== TOP UP SUCCESS ===");
			RestAssured.baseURI = baseUrl;
			httpRequest = RestAssured.given();
			
			// PROVIDE JSON DATA AT REQUEST BODY
			RequestParams = new JSONObject();
			RequestParams.put("userId", userId);
			
			// SET COOKIE, REQUEST HEADER AND REQUEST BODY
			httpRequest.cookie("JSESSIONID", sessionId);
			httpRequest.header("Content-Type", "application/json");
			httpRequest.body(RequestParams.toJSONString());
			
			// SET REQUEST METHOD AND API ENDPOINT
			response = httpRequest.request(Method.GET, endpointViewProfile);
			
			// MANIPULATING RESPONSE DATA
			parser = new JSONParser();
			json = (JSONObject) parser.parse(response.getBody().asString());
			
			tempData = new JSONObject();
			tempData = (JSONObject) json.get("userData");
			balanceAfterTopUp = Integer.parseInt(tempData.get("balance").toString());
			logger.info("Balance after top up : " + balanceAfterTopUp);
			Assert.assertEquals(balanceAfterTopUp.toString(), amount.toString());
			Assert.assertEquals(true, balanceAfterTopUp>balanceBeforeTopUp);
			
			// Save balance after top up data to file userBalanceAfterTopup.txt
			try {
                FileWriter generateSessionId = new FileWriter("assets/generated-data/userBalanceAfterTopup.txt");
                generateSessionId.write(balanceAfterTopUp.toString());
                generateSessionId.close();
            } catch (IOException e) {
                System.err.format("IOException: %s%n", e);
            }
			
			logger.info("Response code : " + responseCode);
		}else {
			logger.info("=== TOP UP FAILED ===");
		}
		
		Thread.sleep(5);
		logger.info("[ --- Finished TopUp_PositiveCase --- ]");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void TopUp_TSC_004() throws InterruptedException, ParseException{
//		'- User already registered
//		- User logged in
//		- Current balance under Rp.2.000.000
//		- Correct amount to top up Rp.1000
//		- Balance after top up is or under Rp.2.000.000
//		- Idempotency key is filled but not unique
		logger.info("[ --- Started TopUp_TSC_004 --- ]");	
		
		// GET EXISTING REQUEST ID
		try {
			getRequestId = new File("./assets/generated-data/requestId.txt");
			requestIdReader = new Scanner(getRequestId);
			while (requestIdReader.hasNextLine()) {
				String data = requestIdReader.nextLine();
				requestId = data;
			}
		}catch (FileNotFoundException e) {
			System.out.println("An error occurred.");
		      e.printStackTrace();
		}
		
		// === GET BALANCE BEFORE TOP UP ===
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		JSONObject RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		JSONObject tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceBeforeTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance before top up : " + balanceBeforeTopUp);
		
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("amount", 1000);
		RequestParams.put("requestId", requestId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		String responseCode = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		logger.info("Top Up : Rp.1000");
		
		// ASSERTION
		Assert.assertEquals(responseDescription, responseStatusSuccess);
		Assert.assertEquals(responseCode.toString(), "300");
		
		if(responseCode.toString().equals("300")) {
			logger.info("=== TOP UP SUCCESS ===");
			RestAssured.baseURI = baseUrl;
			httpRequest = RestAssured.given();
			
			// PROVIDE JSON DATA AT REQUEST BODY
			RequestParams = new JSONObject();
			RequestParams.put("userId", userId);
			
			// SET COOKIE, REQUEST HEADER AND REQUEST BODY
			httpRequest.cookie("JSESSIONID", sessionId);
			httpRequest.header("Content-Type", "application/json");
			httpRequest.body(RequestParams.toJSONString());
			
			// SET REQUEST METHOD AND API ENDPOINT
			response = httpRequest.request(Method.GET, endpointViewProfile);
			
			// MANIPULATING RESPONSE DATA
			parser = new JSONParser();
			json = (JSONObject) parser.parse(response.getBody().asString());
			
			tempData = new JSONObject();
			tempData = (JSONObject) json.get("userData");
			balanceAfterTopUp = Integer.parseInt(tempData.get("balance").toString());
			logger.info("Balance after top up : " + balanceAfterTopUp);
			Assert.assertEquals(balanceAfterTopUp.toString(), "0");
			Assert.assertEquals(true, balanceAfterTopUp==balanceBeforeTopUp);
			
			
			logger.info("Response code : " + responseCode);
		}else {
			logger.info("=== TOP UP FAILED ===");
		}
		
		Thread.sleep(5);
		logger.info("[ --- Finished TopUp_TSC_004 --- ]");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void TopUp_TSC_005() throws InterruptedException, ParseException{
//		'- User already registered
//		- User logged in
//		- Current balance under Rp.2.000.000
//		- Correct amount to top up Rp.100.000
//		- Balance after top up is or under Rp.2.000.000
//		- Idempotency key is filled but not unique
		logger.info("[ --- Started TopUp_TSC_005 --- ]");	
		
		// GET EXISTING REQUEST ID
		try {
			getRequestId = new File("./assets/generated-data/requestId.txt");
			requestIdReader = new Scanner(getRequestId);
			while (requestIdReader.hasNextLine()) {
				String data = requestIdReader.nextLine();
				requestId = data;
			}
		}catch (FileNotFoundException e) {
			System.out.println("An error occurred.");
		      e.printStackTrace();
		}
		
		// === GET BALANCE BEFORE TOP UP ===
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		JSONObject RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		JSONObject tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceBeforeTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance before top up : " + balanceBeforeTopUp);
		
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("amount", 100000);
		RequestParams.put("requestId", requestId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		String responseCode = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		logger.info("Top Up : Rp.100.000");
		
		// ASSERTION
		Assert.assertEquals(responseDescription, responseStatusSuccess);
		Assert.assertEquals(responseCode.toString(), "300");
		
		if(responseCode.toString().equals("300")) {
			logger.info("=== TOP UP SUCCESS ===");
			RestAssured.baseURI = baseUrl;
			httpRequest = RestAssured.given();
			
			// PROVIDE JSON DATA AT REQUEST BODY
			RequestParams = new JSONObject();
			RequestParams.put("userId", userId);
			
			// SET COOKIE, REQUEST HEADER AND REQUEST BODY
			httpRequest.cookie("JSESSIONID", sessionId);
			httpRequest.header("Content-Type", "application/json");
			httpRequest.body(RequestParams.toJSONString());
			
			// SET REQUEST METHOD AND API ENDPOINT
			response = httpRequest.request(Method.GET, endpointViewProfile);
			
			// MANIPULATING RESPONSE DATA
			parser = new JSONParser();
			json = (JSONObject) parser.parse(response.getBody().asString());
			
			tempData = new JSONObject();
			tempData = (JSONObject) json.get("userData");
			balanceAfterTopUp = Integer.parseInt(tempData.get("balance").toString());
			logger.info("Balance after top up : " + balanceAfterTopUp);
			Assert.assertEquals(balanceAfterTopUp.toString(), "0");
			Assert.assertEquals(true, balanceAfterTopUp==balanceBeforeTopUp);
			
			
			logger.info("Response code : " + responseCode);
		}else {
			logger.info("=== TOP UP FAILED ===");
		}
		
		Thread.sleep(5);
		logger.info("[ --- Finished TopUp_TSC_005 --- ]");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void TopUp_TSC_006() throws InterruptedException, ParseException{
//		'- User already registered
//		- User logged in
//		- Current balance under Rp.2.000.000
//		- Correct amount to top up Rp.1.000.000
//		- Balance after top up is or under Rp.2.000.000
//		- Idempotency key is filled but not unique
		logger.info("[ --- Started TopUp_TSC_006 --- ]");	
		
		// GET EXISTING REQUEST ID
		try {
			getRequestId = new File("./assets/generated-data/requestId.txt");
			requestIdReader = new Scanner(getRequestId);
			while (requestIdReader.hasNextLine()) {
				String data = requestIdReader.nextLine();
				requestId = data;
			}
		}catch (FileNotFoundException e) {
			System.out.println("An error occurred.");
		      e.printStackTrace();
		}
		
		// === GET BALANCE BEFORE TOP UP ===
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		JSONObject RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		JSONObject tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceBeforeTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance before top up : " + balanceBeforeTopUp);
		
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("amount", 1000000);
		RequestParams.put("requestId", requestId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		String responseCode = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		logger.info("Top Up : Rp.1.000.000");
		
		// ASSERTION
		Assert.assertEquals(responseDescription, responseStatusSuccess);
		Assert.assertEquals(responseCode.toString(), "300");
		
		if(responseCode.toString().equals("300")) {
			logger.info("=== TOP UP SUCCESS ===");
			RestAssured.baseURI = baseUrl;
			httpRequest = RestAssured.given();
			
			// PROVIDE JSON DATA AT REQUEST BODY
			RequestParams = new JSONObject();
			RequestParams.put("userId", userId);
			
			// SET COOKIE, REQUEST HEADER AND REQUEST BODY
			httpRequest.cookie("JSESSIONID", sessionId);
			httpRequest.header("Content-Type", "application/json");
			httpRequest.body(RequestParams.toJSONString());
			
			// SET REQUEST METHOD AND API ENDPOINT
			response = httpRequest.request(Method.GET, endpointViewProfile);
			
			// MANIPULATING RESPONSE DATA
			parser = new JSONParser();
			json = (JSONObject) parser.parse(response.getBody().asString());
			
			tempData = new JSONObject();
			tempData = (JSONObject) json.get("userData");
			balanceAfterTopUp = Integer.parseInt(tempData.get("balance").toString());
			logger.info("Balance after top up : " + balanceAfterTopUp);
			Assert.assertEquals(balanceAfterTopUp.toString(), "0");
			Assert.assertEquals(true, balanceAfterTopUp==balanceBeforeTopUp);
			
			
			logger.info("Response code : " + responseCode);
		}else {
			logger.info("=== TOP UP FAILED ===");
		}
		
		Thread.sleep(5);
		logger.info("[ --- Finished TopUp_TSC_006 --- ]");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	@Parameters({"amountToTopUp"})
	void TopUp_TSC_007(Integer amount) throws InterruptedException, ParseException{
//		'- User already registered
//		- User logged in
//		- Current balance under Rp.2.000.000
//		- Correct amount to top up Rp.1000
//		- Balance after top up is or under Rp.2.000.000
//		- Idempotency key is empty
		logger.info("[ --- Started TopUp_TSC_007 --- ]");	
		
		// GET EXISTING REQUEST ID
		try {
			getRequestId = new File("./assets/generated-data/requestId.txt");
			requestIdReader = new Scanner(getRequestId);
			while (requestIdReader.hasNextLine()) {
				String data = requestIdReader.nextLine();
				requestId = data;
			}
		}catch (FileNotFoundException e) {
			System.out.println("An error occurred.");
		      e.printStackTrace();
		}
		
		// === GET BALANCE BEFORE TOP UP ===
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		JSONObject RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		JSONObject tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceBeforeTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance before top up : " + balanceBeforeTopUp);
		
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("amount", amount);
		RequestParams.put("requestId", null);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		String responseCode = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		logger.info("Top Up : Rp."+amount);
		
		// ASSERTION
		Assert.assertEquals(responseDescription, responseStatusNullParams);
		Assert.assertEquals(responseCode.toString(), "106");
		
		if(responseCode.toString().equals("106")) {
			logger.info("=== TOP UP WITH NULL PARAMS SUCCESS ===");
			RestAssured.baseURI = baseUrl;
			httpRequest = RestAssured.given();
			
			// PROVIDE JSON DATA AT REQUEST BODY
			RequestParams = new JSONObject();
			RequestParams.put("userId", userId);
			
			// SET COOKIE, REQUEST HEADER AND REQUEST BODY
			httpRequest.cookie("JSESSIONID", sessionId);
			httpRequest.header("Content-Type", "application/json");
			httpRequest.body(RequestParams.toJSONString());
			
			// SET REQUEST METHOD AND API ENDPOINT
			response = httpRequest.request(Method.GET, endpointViewProfile);
			
			// MANIPULATING RESPONSE DATA
			parser = new JSONParser();
			json = (JSONObject) parser.parse(response.getBody().asString());
			
			tempData = new JSONObject();
			tempData = (JSONObject) json.get("userData");
			balanceAfterTopUp = Integer.parseInt(tempData.get("balance").toString());
			logger.info("Balance after top up : " + balanceAfterTopUp);
			Assert.assertEquals(balanceAfterTopUp.toString(), "0");
			Assert.assertEquals(true, balanceAfterTopUp==balanceBeforeTopUp);
			
			
			logger.info("Response code : " + responseCode);
		}else {
			logger.info("=== TOP UP WITH NULL PARAMS FAILED ===");
		}
		
		Thread.sleep(5);
		logger.info("[ --- Finished TopUp_TSC_007 --- ]");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void TopUp_TSC_010() throws InterruptedException, ParseException{
//		'- User already registered
//		- User logged in
//		- Current balance under Rp.2.000.000
//		- Correct amount to top up Rp. 1000
//		- Balance after top up over Rp.2.000.000
//		- Idempotency key is filled and unique
		logger.info("[ --- Started TopUp_TSC_010 --- ]");	
		
		// GET EXISTING REQUEST ID
		try {
			getRequestId = new File("./assets/generated-data/requestId.txt");
			requestIdReader = new Scanner(getRequestId);
			while (requestIdReader.hasNextLine()) {
				String data = requestIdReader.nextLine();
				requestId = data;
			}
		}catch (FileNotFoundException e) {
			System.out.println("An error occurred.");
		      e.printStackTrace();
		}
		
		// === GET BALANCE BEFORE TOP UP ===
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		JSONObject RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		JSONObject tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceBeforeTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance before top up : " + balanceBeforeTopUp);
		
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("amount", 1000000);
		RequestParams.put("requestId", generateRequestId());
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		String responseCode = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp.1.000.000");
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);	
		
		// === GET BALANCE AFTER TOP UP ===
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		
		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceAfterTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance after top up : " + balanceAfterTopUp);
		
		// === GET BALANCE BEFORE TOP UP ===
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		
		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceBeforeTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance before top up : " + balanceBeforeTopUp);
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("amount", 999999);
		RequestParams.put("requestId", generateRequestId());
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		responseCode = json.get("code").toString();
		responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp.999.999");
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		balanceAfterTopUp = balanceBeforeTopUp + 999999;
		logger.info("Balance after top up : " + balanceAfterTopUp);
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("amount", 1000);
		RequestParams.put("requestId", generateRequestId());
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		responseCode = json.get("code").toString();
		responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp.1000");
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION
		Assert.assertEquals(responseDescription, responseBalanceExceedMax);
		Assert.assertEquals(responseCode.toString(), "203");
		
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		
		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceAfterTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance after top up : " + balanceAfterTopUp);
		Assert.assertEquals(balanceAfterTopUp.toString(), "1999999");
		Assert.assertEquals(true, balanceAfterTopUp<=2000000);
		
		logger.info("Response code : " + responseCode);
		
		
		Thread.sleep(5);
		logger.info("[ --- Finished TopUp_TSC_010 --- ]");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void TopUp_TSC_011() throws InterruptedException, ParseException{
//		'- User already registered
//		- User logged in
//		- Current balance under Rp.2.000.000
//		- Correct amount to top up Rp. 100.000
//		- Balance after top up over Rp.2.000.000
//		- Idempotency key is filled and unique
		logger.info("[ --- Started TopUp_TSC_011 --- ]");	
		
		// GET EXISTING REQUEST ID
		try {
			getRequestId = new File("./assets/generated-data/requestId.txt");
			requestIdReader = new Scanner(getRequestId);
			while (requestIdReader.hasNextLine()) {
				String data = requestIdReader.nextLine();
				requestId = data;
			}
		}catch (FileNotFoundException e) {
			System.out.println("An error occurred.");
		      e.printStackTrace();
		}
		
		// === GET BALANCE BEFORE TOP UP ===
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		JSONObject RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		JSONObject tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceBeforeTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance before top up : " + balanceBeforeTopUp);
		
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("amount", 1000000);
		RequestParams.put("requestId", generateRequestId());
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		String responseCode = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp.1.000.000");
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);	
		
		// === GET BALANCE AFTER TOP UP ===
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		
		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceAfterTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance after top up : " + balanceAfterTopUp);
		
		// === GET BALANCE BEFORE TOP UP ===
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		
		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceBeforeTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance before top up : " + balanceBeforeTopUp);
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("amount", 999999);
		RequestParams.put("requestId", generateRequestId());
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		responseCode = json.get("code").toString();
		responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp.999.999");
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		balanceAfterTopUp = balanceBeforeTopUp + 999999;
		logger.info("Balance after top up : " + balanceAfterTopUp);
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("amount", 100000);
		RequestParams.put("requestId", generateRequestId());
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		responseCode = json.get("code").toString();
		responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp.100.000");
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION
		Assert.assertEquals(responseDescription, responseBalanceExceedMax);
		Assert.assertEquals(responseCode.toString(), "203");
		
		
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		
		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceAfterTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance after top up : " + balanceAfterTopUp);
		Assert.assertEquals(balanceAfterTopUp.toString(), "1999999");
		Assert.assertEquals(true, balanceAfterTopUp<=2000000);
		
		
		logger.info("Response code : " + responseCode);
		
		
		Thread.sleep(5);
		logger.info("[ --- Finished TopUp_TSC_011 --- ]");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void TopUp_TSC_012() throws InterruptedException, ParseException{
//		'- User already registered
//		- User logged in
//		- Current balance under Rp.2.000.000
//		- Correct amount to top up Rp. 100.000
//		- Balance after top up over Rp.2.000.000
//		- Idempotency key is filled and unique
		logger.info("[ --- Started TopUp_TSC_012 --- ]");	
		
		// GET EXISTING REQUEST ID
		try {
			getRequestId = new File("./assets/generated-data/requestId.txt");
			requestIdReader = new Scanner(getRequestId);
			while (requestIdReader.hasNextLine()) {
				String data = requestIdReader.nextLine();
				requestId = data;
			}
		}catch (FileNotFoundException e) {
			System.out.println("An error occurred.");
		      e.printStackTrace();
		}
		
		// === GET BALANCE BEFORE TOP UP ===
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		JSONObject RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		JSONObject tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceBeforeTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance before top up : " + balanceBeforeTopUp);
		
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("amount", 1000000);
		RequestParams.put("requestId", generateRequestId());
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		String responseCode = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp.1.000.000");
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);	
		
		// === GET BALANCE AFTER TOP UP ===
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		
		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceAfterTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance after top up : " + balanceAfterTopUp);
		
		// === GET BALANCE BEFORE TOP UP ===
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		
		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceBeforeTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance before top up : " + balanceBeforeTopUp);
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("amount", 999999);
		RequestParams.put("requestId", generateRequestId());
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		responseCode = json.get("code").toString();
		responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp.999.999");
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		balanceAfterTopUp = balanceBeforeTopUp + 999999;
		logger.info("Balance after top up : " + balanceAfterTopUp);
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("amount", 1000000);
		RequestParams.put("requestId", generateRequestId());
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		responseCode = json.get("code").toString();
		responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp.1.000.000");
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION
		Assert.assertEquals(responseDescription, responseBalanceExceedMax);
		Assert.assertEquals(responseCode.toString(), "203");
		
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		
		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceAfterTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance after top up : " + balanceAfterTopUp);
		Assert.assertEquals(balanceAfterTopUp.toString(), "1999999");
		Assert.assertEquals(true, balanceAfterTopUp<=2000000);

		logger.info("Response code : " + responseCode);
		Thread.sleep(5);
		logger.info("[ --- Finished TopUp_TSC_012 --- ]");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void TopUp_TSC_013() throws InterruptedException, ParseException{
//		'- User already registered
//		- User logged in
//		- Current balance under Rp.2.000.000
//		- Correct amount to top up Rp. 1000
//		- Balance after top up over Rp.2.000.000
//		- Idempotency key is filled but not unique
		logger.info("[ --- Started TopUp_TSC_013 --- ]");	
		
		// GET EXISTING REQUEST ID
		try {
			getRequestId = new File("./assets/generated-data/requestId.txt");
			requestIdReader = new Scanner(getRequestId);
			while (requestIdReader.hasNextLine()) {
				String data = requestIdReader.nextLine();
				requestId = data;
			}
		}catch (FileNotFoundException e) {
			System.out.println("An error occurred.");
		      e.printStackTrace();
		}
		
		// === GET BALANCE BEFORE TOP UP ===
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		JSONObject RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		JSONObject tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceBeforeTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance before top up : " + balanceBeforeTopUp);
		
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("amount", 1000000);
		RequestParams.put("requestId", generateRequestId());
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		String responseCode = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp.1.000.000");
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);	
		
		// === GET BALANCE AFTER TOP UP ===
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		
		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceAfterTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance after top up : " + balanceAfterTopUp);
		
		// === GET BALANCE BEFORE TOP UP ===
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		
		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceBeforeTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance before top up : " + balanceBeforeTopUp);
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("amount", 999999);
		RequestParams.put("requestId", generateRequestId());
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		responseCode = json.get("code").toString();
		responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp.999.999");
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		balanceAfterTopUp = balanceBeforeTopUp + 999999;
		logger.info("Balance after top up : " + balanceAfterTopUp);
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("amount", 1000);
		RequestParams.put("requestId", requestId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		responseCode = json.get("code").toString();
		responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp.1000");
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION
		Assert.assertEquals(responseDescription, responseStatusSuccess);
		Assert.assertEquals(responseCode.toString(), "300");
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		
		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceAfterTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance after top up : " + balanceAfterTopUp);
		Assert.assertEquals(balanceAfterTopUp.toString(), "1999999");
		Assert.assertEquals(true, balanceAfterTopUp<=2000000);
		
		logger.info("Response code : " + responseCode);
		Thread.sleep(5);
		logger.info("[ --- Finished TopUp_TSC_013 --- ]");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void TopUp_TSC_014() throws InterruptedException, ParseException{
//		'- User already registered
//		- User logged in
//		- Current balance under Rp.2.000.000
//		- Correct amount to top up Rp. 100.000
//		- Balance after top up over Rp.2.000.000
//		- Idempotency key is filled but not unique
		logger.info("[ --- Started TopUp_TSC_014 --- ]");	
		
		// GET EXISTING REQUEST ID
		try {
			getRequestId = new File("./assets/generated-data/requestId.txt");
			requestIdReader = new Scanner(getRequestId);
			while (requestIdReader.hasNextLine()) {
				String data = requestIdReader.nextLine();
				requestId = data;
			}
		}catch (FileNotFoundException e) {
			System.out.println("An error occurred.");
		      e.printStackTrace();
		}
		
		// === GET BALANCE BEFORE TOP UP ===
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		JSONObject RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		JSONObject tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceBeforeTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance before top up : " + balanceBeforeTopUp);
		
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("amount", 1000000);
		RequestParams.put("requestId", generateRequestId());
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		String responseCode = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp.1.000.000");
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);	
		
		// === GET BALANCE AFTER TOP UP ===
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		
		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceAfterTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance after top up : " + balanceAfterTopUp);
		
		// === GET BALANCE BEFORE TOP UP ===
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		
		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceBeforeTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance before top up : " + balanceBeforeTopUp);
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("amount", 999999);
		RequestParams.put("requestId", generateRequestId());
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		responseCode = json.get("code").toString();
		responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp.999.999");
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		balanceAfterTopUp = balanceBeforeTopUp + 999999;
		logger.info("Balance after top up : " + balanceAfterTopUp);
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("amount", 100000);
		RequestParams.put("requestId", requestId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		responseCode = json.get("code").toString();
		responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp.100.000");
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION
		Assert.assertEquals(responseDescription, responseStatusSuccess);
		Assert.assertEquals(responseCode.toString(), "300");
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		
		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceAfterTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance after top up : " + balanceAfterTopUp);
		Assert.assertEquals(balanceAfterTopUp.toString(), "1999999");
		Assert.assertEquals(true, balanceAfterTopUp<=2000000);
		
		logger.info("Response code : " + responseCode);
		Thread.sleep(5);
		logger.info("[ --- Finished TopUp_TSC_014 --- ]");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void TopUp_TSC_015() throws InterruptedException, ParseException{
//		'- User already registered
//		- User logged in
//		- Current balance under Rp.2.000.000
//		- Correct amount to top up Rp. 1.000.000
//		- Balance after top up over Rp.2.000.000
//		- Idempotency key is filled but not unique
		logger.info("[ --- Started TopUp_TSC_015 --- ]");	
		
		// GET EXISTING REQUEST ID
		try {
			getRequestId = new File("./assets/generated-data/requestId.txt");
			requestIdReader = new Scanner(getRequestId);
			while (requestIdReader.hasNextLine()) {
				String data = requestIdReader.nextLine();
				requestId = data;
			}
		}catch (FileNotFoundException e) {
			System.out.println("An error occurred.");
		      e.printStackTrace();
		}
		
		// === GET BALANCE BEFORE TOP UP ===
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		JSONObject RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		JSONObject tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceBeforeTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance before top up : " + balanceBeforeTopUp);
		
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("amount", 1000000);
		RequestParams.put("requestId", generateRequestId());
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		String responseCode = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp.1.000.000");
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);	
		
		// === GET BALANCE AFTER TOP UP ===
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		
		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceAfterTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance after top up : " + balanceAfterTopUp);
		
		// === GET BALANCE BEFORE TOP UP ===
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		
		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceBeforeTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance before top up : " + balanceBeforeTopUp);
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("amount", 999999);
		RequestParams.put("requestId", generateRequestId());
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		responseCode = json.get("code").toString();
		responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp.999.999");
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		balanceAfterTopUp = balanceBeforeTopUp + 999999;
		logger.info("Balance after top up : " + balanceAfterTopUp);
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("amount", 1000000);
		RequestParams.put("requestId", requestId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		responseCode = json.get("code").toString();
		responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp.1.000.000");
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION
		Assert.assertEquals(responseDescription, responseStatusSuccess);
		Assert.assertEquals(responseCode.toString(), "300");
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		
		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceAfterTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance after top up : " + balanceAfterTopUp);
		Assert.assertEquals(balanceAfterTopUp.toString(), "1999999");
		Assert.assertEquals(true, balanceAfterTopUp<=2000000);
		
		logger.info("Response code : " + responseCode);
		Thread.sleep(5);
		logger.info("[ --- Finished TopUp_TSC_015 --- ]");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	@Parameters({"amountToTopUp"})
	void TopUp_TSC_016(Integer amount) throws InterruptedException, ParseException{
//		'- User already registered
//		- User logged in
//		- Current balance under Rp.2.000.000
//		- Correct amount to top up Rp. 1000
//		- Balance after top up over Rp.2.000.000
//		- Idempotency key is empty 
		if(amount==1000) {
			logger.info("[ --- Started TopUp_TSC_016 --- ]");	
		}else if(amount==100000) {
			logger.info("[ --- Started TopUp_TSC_017 --- ]");	
		}else if(amount==1000000) {
			logger.info("[ --- Started TopUp_TSC_018 --- ]");	
		}
		
		
		// GET EXISTING REQUEST ID
		try {
			getRequestId = new File("./assets/generated-data/requestId.txt");
			requestIdReader = new Scanner(getRequestId);
			while (requestIdReader.hasNextLine()) {
				String data = requestIdReader.nextLine();
				requestId = data;
			}
		}catch (FileNotFoundException e) {
			System.out.println("An error occurred.");
		      e.printStackTrace();
		}
		
		// === GET BALANCE BEFORE TOP UP ===
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		JSONObject RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		JSONObject tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceBeforeTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance before top up : " + balanceBeforeTopUp);
		
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("amount", 1000000);
		RequestParams.put("requestId", generateRequestId());
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		String responseCode = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp.1.000.000");
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);	
		
		// === GET BALANCE AFTER TOP UP ===
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		
		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceAfterTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance after top up : " + balanceAfterTopUp);
		
		// === GET BALANCE BEFORE TOP UP ===
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		
		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceBeforeTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance before top up : " + balanceBeforeTopUp);
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("amount", 999999);
		RequestParams.put("requestId", generateRequestId());
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		responseCode = json.get("code").toString();
		responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp.999.999");
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		balanceAfterTopUp = balanceBeforeTopUp + 999999;
		logger.info("Balance after top up : " + balanceAfterTopUp);
		Assert.assertEquals(balanceAfterTopUp.toString(), "1999999");
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("amount", amount);
		RequestParams.put("requestId", null);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		responseCode = json.get("code").toString();
		responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp."+amount);
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION
		Assert.assertEquals(responseDescription, responseStatusNullParams);
		Assert.assertEquals(responseCode.toString(), "106");
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		
		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceAfterTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance after top up : " + balanceAfterTopUp);
		Assert.assertEquals(balanceAfterTopUp.toString(), "1999999");
		Assert.assertEquals(true, balanceAfterTopUp<=2000000);
		
		logger.info("Response code : " + responseCode);
		Thread.sleep(5);
		if(amount==1000) {
			logger.info("[ --- Finished TopUp_TSC_016 --- ]");	
		}else if(amount==100000) {
			logger.info("[ --- Finished TopUp_TSC_017 --- ]");	
		}else if(amount==1000000) {
			logger.info("[ --- Finished TopUp_TSC_018 --- ]");	
		}
	}
	
	@SuppressWarnings("unchecked")
	@Test(priority=0)
	@Parameters({"amountToTopUp"})
	void TopUp_TSC_019(Integer amount) throws InterruptedException, ParseException{
//		'- User already registered
//		- User logged in
//		- Current balance under Rp.2.000.000
//		- Incorrect amount to top up Rp.999
//		- Balance after top up is or under Rp.2.000.000
//		- Idempotency key is filled and unique
		if (amount <= 999) {
			logger.info("[ --- Started TopUp_TSC_019 --- ]");	
		}else if(amount >= 1000001) {
			logger.info("[ --- Started TopUp_TSC_020 --- ]");	
		}
		
		
		// === GET BALANCE BEFORE TOP UP ===
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		JSONObject RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		JSONObject tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceBeforeTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance before top up : " + balanceBeforeTopUp);
		
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("amount", amount);
		RequestParams.put("requestId", generateRequestId());
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		String responseCode = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		logger.info("Top Up : Rp."+amount);
		
		if(amount <= 999) {
			// ASSERTION
			Assert.assertEquals(responseDescription, responseStatusTopUpUnderMin);
			Assert.assertEquals(responseCode.toString(), "103");
			
			if(responseCode.toString().equals("103")) {
				logger.info("=== TOP UP AMOUNT UNDER MINIMUM SUCCESS ===");
				RestAssured.baseURI = baseUrl;
				httpRequest = RestAssured.given();
				
				// PROVIDE JSON DATA AT REQUEST BODY
				RequestParams = new JSONObject();
				RequestParams.put("userId", userId);
				
				// SET COOKIE, REQUEST HEADER AND REQUEST BODY
				httpRequest.cookie("JSESSIONID", sessionId);
				httpRequest.header("Content-Type", "application/json");
				httpRequest.body(RequestParams.toJSONString());
				
				// SET REQUEST METHOD AND API ENDPOINT
				response = httpRequest.request(Method.GET, endpointViewProfile);
				
				// MANIPULATING RESPONSE DATA
				parser = new JSONParser();
				json = (JSONObject) parser.parse(response.getBody().asString());
				
				tempData = new JSONObject();
				tempData = (JSONObject) json.get("userData");
				balanceAfterTopUp = Integer.parseInt(tempData.get("balance").toString());
				logger.info("Balance after top up : " + balanceAfterTopUp);
				Assert.assertEquals(balanceAfterTopUp.toString(), "0");
				Assert.assertEquals(true, balanceAfterTopUp==balanceBeforeTopUp);
				
				logger.info("Response code : " + responseCode);
			}else {
				logger.info("=== TOP UP AMOUNT UNDER MINIMUM SUCCESS FAILED ===");
			}
		}else if(amount >= 1000001) {
			if(responseCode.toString().equals("107")) {
				logger.info("=== TOP UP AMOUNT OVER MAXIMUM SUCCESS ===");
				RestAssured.baseURI = baseUrl;
				httpRequest = RestAssured.given();
				
				// PROVIDE JSON DATA AT REQUEST BODY
				RequestParams = new JSONObject();
				RequestParams.put("userId", userId);
				
				// SET COOKIE, REQUEST HEADER AND REQUEST BODY
				httpRequest.cookie("JSESSIONID", sessionId);
				httpRequest.header("Content-Type", "application/json");
				httpRequest.body(RequestParams.toJSONString());
				
				// SET REQUEST METHOD AND API ENDPOINT
				response = httpRequest.request(Method.GET, endpointViewProfile);
				
				// MANIPULATING RESPONSE DATA
				parser = new JSONParser();
				json = (JSONObject) parser.parse(response.getBody().asString());
				
				tempData = new JSONObject();
				tempData = (JSONObject) json.get("userData");
				balanceAfterTopUp = Integer.parseInt(tempData.get("balance").toString());
				logger.info("Balance after top up : " + balanceAfterTopUp);
				Assert.assertEquals(balanceAfterTopUp.toString(), "0");
				Assert.assertEquals(true, balanceAfterTopUp==balanceBeforeTopUp);
				
				logger.info("Response code : " + responseCode);
			}else {
				logger.info("=== TOP UP AMOUNT OVER MAXIMUM FAILED ===");
			}
		}
		Thread.sleep(5);
		if (amount <= 999) {
			logger.info("[ --- Finished TopUp_TSC_019 --- ]");	
		}else if(amount >= 1000001) {
			logger.info("[ --- Finished TopUp_TSC_020 --- ]");	
		}
	}
	
	@SuppressWarnings("unchecked")
	@Test(priority=0)
	void TopUp_TSC_021() throws InterruptedException, ParseException{
//		'- User already registered
//		- User logged in
//		- Current balance under Rp.2.000.000
//		- Incorrect amount to top up Rp.999
//		- Balance after top up is or under Rp.2.000.000
//		- Idempotency key is filled but not unique
		logger.info("[ --- Started TopUp_TSC_021 --- ]");	
		
		// === GET BALANCE BEFORE TOP UP ===
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		JSONObject RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		JSONObject tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceBeforeTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance before top up : " + balanceBeforeTopUp);
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("amount", 999);
		RequestParams.put("requestId", requestId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		String responseCode = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp.999");
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION
		Assert.assertEquals(responseDescription, responseStatusSuccess);
		Assert.assertEquals(responseCode.toString(), "300");
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		
		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceAfterTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance after top up : " + balanceAfterTopUp);
		Assert.assertEquals(balanceAfterTopUp.toString(), "0");
		Assert.assertEquals(true, balanceAfterTopUp<=2000000);
		
		Thread.sleep(5);
		logger.info("[ --- Finished TopUp_TSC_021 --- ]");
	}
	
	@SuppressWarnings("unchecked")
	@Test(priority=0)
	void TopUp_TSC_022() throws InterruptedException, ParseException{
//		'- User already registered
//		- User logged in
//		- Current balance under Rp.2.000.000
//		- Incorrect amount to top up Rp.1.001.000
//		- Balance after top up is or under Rp.2.000.000
//		- Idempotency key is filled but not unique
		logger.info("[ --- Started TopUp_TSC_022 --- ]");	
		
		// === GET BALANCE BEFORE TOP UP ===
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		JSONObject RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		JSONObject tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceBeforeTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance before top up : " + balanceBeforeTopUp);
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("amount", 1001000);
		RequestParams.put("requestId", requestId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		String responseCode = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp.999");
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION
		Assert.assertEquals(responseDescription, responseStatusSuccess);
		Assert.assertEquals(responseCode.toString(), "300");
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		
		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceAfterTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance after top up : " + balanceAfterTopUp);
		Assert.assertEquals(balanceAfterTopUp.toString(), "0");
		Assert.assertEquals(true, balanceAfterTopUp<=2000000);
		
		Thread.sleep(5);
		logger.info("[ --- Finished TopUp_TSC_022 --- ]");
	}
	
	@SuppressWarnings("unchecked")
	@Test(priority=0)
	@Parameters({"amountToTopUp"})
	void TopUp_TSC_023(Integer amount) throws InterruptedException, ParseException{
//		'- User already registered
//		- User logged in
//		- Current balance under Rp.2.000.000
//		- Incorrect amount to top up Rp.1.001.000
//		- Balance after top up is or under Rp.2.000.000
//		- Idempotency key is filled but not unique
		
		if(amount == 999) {
			logger.info("[ --- Started TopUp_TSC_023 --- ]");
		}else if(amount == 1001000) {
			logger.info("[ --- Started TopUp_TSC_024 --- ]");
		}
		
		// === GET BALANCE BEFORE TOP UP ===
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		JSONObject RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		JSONObject tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceBeforeTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance before top up : " + balanceBeforeTopUp);
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("amount", amount);
		RequestParams.put("requestId", null);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		String responseCode = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp."+amount);
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION
		Assert.assertEquals(responseDescription, responseStatusNullParams);
		Assert.assertEquals(responseCode.toString(), "106");
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		
		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceAfterTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance after top up : " + balanceAfterTopUp);
		Assert.assertEquals(balanceAfterTopUp.toString(), "0");
		Assert.assertEquals(true, balanceAfterTopUp<=2000000);
		
		Thread.sleep(5);
		if(amount == 999) {
			logger.info("[ --- Finished TopUp_TSC_023 --- ]");
		}else if(amount == 1001000) {
			logger.info("[ --- Finished TopUp_TSC_024 --- ]");
		}
		
	}
	
	
	@SuppressWarnings("unchecked")
	@Test
	void TopUp_TSC_025() throws InterruptedException, ParseException{
//		'- User already registered
//		- User logged in
//		- Current balance under Rp.2.000.000
//		- Incorrect amount to top up Rp.999
//		- Balance after top up over Rp.2.000.000
//		- Idempotency key is filled and unique
		logger.info("[ --- Started TopUp_TSC_025 --- ]");	
		
		// GET EXISTING REQUEST ID
		try {
			getRequestId = new File("./assets/generated-data/requestId.txt");
			requestIdReader = new Scanner(getRequestId);
			while (requestIdReader.hasNextLine()) {
				String data = requestIdReader.nextLine();
				requestId = data;
			}
		}catch (FileNotFoundException e) {
			System.out.println("An error occurred.");
		      e.printStackTrace();
		}
		
		// === GET BALANCE BEFORE TOP UP ===
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		JSONObject RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		JSONObject tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceBeforeTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance before top up : " + balanceBeforeTopUp);
		
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("amount", 1000000);
		RequestParams.put("requestId", generateRequestId());
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		String responseCode = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp.1.000.000");
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);	
		
		// === GET BALANCE AFTER TOP UP ===
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		
		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceAfterTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance after top up : " + balanceAfterTopUp);
		
		// === GET BALANCE BEFORE TOP UP ===
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		
		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceBeforeTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance before top up : " + balanceBeforeTopUp);
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("amount", 999999);
		RequestParams.put("requestId", generateRequestId());
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		responseCode = json.get("code").toString();
		responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp.999.999");
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		balanceAfterTopUp = balanceBeforeTopUp + 999999;
		logger.info("Balance after top up : " + balanceAfterTopUp);
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("amount", 999);
		RequestParams.put("requestId", generateRequestId());
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		responseCode = json.get("code").toString();
		responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp.999");
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION
		Assert.assertEquals(responseDescription, responseStatusTopUpUnderMin);
		Assert.assertEquals(responseCode.toString(), "103");
		
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		
		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceAfterTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance after top up : " + balanceAfterTopUp);
		Assert.assertEquals(balanceAfterTopUp.toString(), "1999999");
		Assert.assertEquals(true, balanceAfterTopUp<=2000000);
		
		logger.info("Response code : " + responseCode);
		Thread.sleep(5);
		logger.info("[ --- Finished TopUp_TSC_025 --- ]");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void TopUp_TSC_026() throws InterruptedException, ParseException{
//		'- User already registered
//		- User logged in
//		- Current balance under Rp.2.000.000
//		- Incorrect amount to top up Rp.1.001.000
//		- Balance after top up over Rp.2.000.000
//		- Idempotency key is filled and unique
		logger.info("[ --- Started TopUp_TSC_026 --- ]");	
		
		// GET EXISTING REQUEST ID
		try {
			getRequestId = new File("./assets/generated-data/requestId.txt");
			requestIdReader = new Scanner(getRequestId);
			while (requestIdReader.hasNextLine()) {
				String data = requestIdReader.nextLine();
				requestId = data;
			}
		}catch (FileNotFoundException e) {
			System.out.println("An error occurred.");
		      e.printStackTrace();
		}
		
		// === GET BALANCE BEFORE TOP UP ===
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		JSONObject RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		JSONObject tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceBeforeTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance before top up : " + balanceBeforeTopUp);
		
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("amount", 1000000);
		RequestParams.put("requestId", generateRequestId());
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		String responseCode = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp.1.000.000");
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);	
		
		// === GET BALANCE AFTER TOP UP ===
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		
		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceAfterTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance after top up : " + balanceAfterTopUp);
		
		// === GET BALANCE BEFORE TOP UP ===
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		
		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceBeforeTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance before top up : " + balanceBeforeTopUp);
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("amount", 999999);
		RequestParams.put("requestId", generateRequestId());
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		responseCode = json.get("code").toString();
		responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp.999.999");
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		balanceAfterTopUp = balanceBeforeTopUp + 999999;
		logger.info("Balance after top up : " + balanceAfterTopUp);
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("amount", 1001000);
		RequestParams.put("requestId", generateRequestId());
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		responseCode = json.get("code").toString();
		responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp.1.001.000");
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION
		Assert.assertEquals(responseDescription, responseStatusTopUpOverMax);
		Assert.assertEquals(responseCode.toString(), "107");
		
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		
		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceAfterTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance after top up : " + balanceAfterTopUp);
		Assert.assertEquals(balanceAfterTopUp.toString(), "1999999");
		Assert.assertEquals(true, balanceAfterTopUp<=2000000);
		
		logger.info("Response code : " + responseCode);
		Thread.sleep(5);
		logger.info("[ --- Finished TopUp_TSC_026 --- ]");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void TopUp_TSC_027() throws InterruptedException, ParseException{
//		'- User already registered
//		- User logged in
//		- Current balance under Rp.2.000.000
//		- Incorrect amount to top up Rp.999
//		- Balance after top up over Rp.2.000.000
//		- Idempotency key is filled but not unique
		logger.info("[ --- Started TopUp_TSC_027 --- ]");	
		
		// GET EXISTING REQUEST ID
		try {
			getRequestId = new File("./assets/generated-data/requestId.txt");
			requestIdReader = new Scanner(getRequestId);
			while (requestIdReader.hasNextLine()) {
				String data = requestIdReader.nextLine();
				requestId = data;
			}
		}catch (FileNotFoundException e) {
			System.out.println("An error occurred.");
		      e.printStackTrace();
		}
		
		// === GET BALANCE BEFORE TOP UP ===
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		JSONObject RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		JSONObject tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceBeforeTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance before top up : " + balanceBeforeTopUp);
		
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("amount", 1000000);
		RequestParams.put("requestId", generateRequestId());
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		String responseCode = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp.1.000.000");
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);	
		
		// === GET BALANCE AFTER TOP UP ===
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		
		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceAfterTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance after top up : " + balanceAfterTopUp);
		
		// === GET BALANCE BEFORE TOP UP ===
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		
		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceBeforeTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance before top up : " + balanceBeforeTopUp);
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("amount", 999999);
		RequestParams.put("requestId", generateRequestId());
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		responseCode = json.get("code").toString();
		responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp.999.999");
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		balanceAfterTopUp = balanceBeforeTopUp + 999999;
		logger.info("Balance after top up : " + balanceAfterTopUp);
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("amount", 999);
		RequestParams.put("requestId", requestId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		responseCode = json.get("code").toString();
		responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp.999");
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION
		Assert.assertEquals(responseDescription, responseStatusSuccess);
		Assert.assertEquals(responseCode.toString(), "300");
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		
		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceAfterTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance after top up : " + balanceAfterTopUp);
		Assert.assertEquals(balanceAfterTopUp.toString(), "1999999");
		Assert.assertEquals(true, balanceAfterTopUp<=2000000);
		
		logger.info("Response code : " + responseCode);
		Thread.sleep(5);
		logger.info("[ --- Finished TopUp_TSC_027 --- ]");
	}

	@SuppressWarnings("unchecked")
	@Test
	void TopUp_TSC_028() throws InterruptedException, ParseException{
//		'- User already registered
//		- User logged in
//		- Current balance under Rp.2.000.000
//		- Incorrect amount to top up Rp.1.001.000
//		- Balance after top up over Rp.2.000.000
//		- Idempotency key is filled but not unique
		logger.info("[ --- Started TopUp_TSC_028 --- ]");	
		
		// GET EXISTING REQUEST ID
		try {
			getRequestId = new File("./assets/generated-data/requestId.txt");
			requestIdReader = new Scanner(getRequestId);
			while (requestIdReader.hasNextLine()) {
				String data = requestIdReader.nextLine();
				requestId = data;
			}
		}catch (FileNotFoundException e) {
			System.out.println("An error occurred.");
		      e.printStackTrace();
		}
		
		// === GET BALANCE BEFORE TOP UP ===
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		JSONObject RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		JSONObject tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceBeforeTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance before top up : " + balanceBeforeTopUp);
		
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("amount", 1000000);
		RequestParams.put("requestId", generateRequestId());
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		String responseCode = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp.1.000.000");
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);	
		
		// === GET BALANCE AFTER TOP UP ===
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		
		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceAfterTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance after top up : " + balanceAfterTopUp);
		
		// === GET BALANCE BEFORE TOP UP ===
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		
		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceBeforeTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance before top up : " + balanceBeforeTopUp);
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("amount", 999999);
		RequestParams.put("requestId", generateRequestId());
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		responseCode = json.get("code").toString();
		responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp.999.999");
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		balanceAfterTopUp = balanceBeforeTopUp + 999999;
		logger.info("Balance after top up : " + balanceAfterTopUp);
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("amount", 1001000);
		RequestParams.put("requestId", requestId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		responseCode = json.get("code").toString();
		responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp.1.001.000");
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION
		Assert.assertEquals(responseDescription, responseStatusSuccess);
		Assert.assertEquals(responseCode.toString(), "300");
		
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		
		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceAfterTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance after top up : " + balanceAfterTopUp);
		Assert.assertEquals(balanceAfterTopUp.toString(), "1999999");
		Assert.assertEquals(true, balanceAfterTopUp<=2000000);
		
		logger.info("Response code : " + responseCode);
		Thread.sleep(5);
		logger.info("[ --- Finished TopUp_TSC_028 --- ]");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	@Parameters({"amountToTopUp"})
	void TopUp_TSC_029(Integer amount) throws InterruptedException, ParseException{
//		'- User already registered
//		- User logged in
//		- Current balance under Rp.2.000.000
//		- Incorrect amount to top up Rp.999
//		- Balance after top up over Rp.2.000.000
//		- Idempotency key is empty
		if(amount==999) {
			logger.info("[ --- Started TopUp_TSC_029 --- ]");
		}else if(amount==1001000) {
			logger.info("[ --- Started TopUp_TSC_030 --- ]");
		}
		
		// GET EXISTING REQUEST ID
		try {
			getRequestId = new File("./assets/generated-data/requestId.txt");
			requestIdReader = new Scanner(getRequestId);
			while (requestIdReader.hasNextLine()) {
				String data = requestIdReader.nextLine();
				requestId = data;
			}
		}catch (FileNotFoundException e) {
			System.out.println("An error occurred.");
		      e.printStackTrace();
		}
		
		// === GET BALANCE BEFORE TOP UP ===
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		JSONObject RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		JSONObject tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceBeforeTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance before top up : " + balanceBeforeTopUp);
		
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("amount", 1000000);
		RequestParams.put("requestId", generateRequestId());
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		String responseCode = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp.1.000.000");
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);	
		
		// === GET BALANCE AFTER TOP UP ===
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		
		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceAfterTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance after top up : " + balanceAfterTopUp);
		
		// === GET BALANCE BEFORE TOP UP ===
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		
		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceBeforeTopUp = Integer.parseInt(tempData.get("balance").toString());
		Assert.assertEquals(balanceBeforeTopUp.toString(), "1000000");
		logger.info("Balance before top up : " + balanceBeforeTopUp);
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("amount", 999999);
		RequestParams.put("requestId", generateRequestId());
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		responseCode = json.get("code").toString();
		responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp.999.999");
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		balanceAfterTopUp = balanceBeforeTopUp + 999999;
		Assert.assertEquals(balanceAfterTopUp.toString(), "1999999");
		logger.info("Balance after top up : " + balanceAfterTopUp);
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("amount", amount);
		RequestParams.put("requestId", null);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		responseCode = json.get("code").toString();
		responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp.1.001.000");
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION
		Assert.assertEquals(responseDescription, responseStatusNullParams);
		Assert.assertEquals(responseCode.toString(), "106");
		
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		
		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceAfterTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance after top up : " + balanceAfterTopUp);
		Assert.assertEquals(balanceAfterTopUp.toString(), "1999999");
		Assert.assertEquals(true, balanceAfterTopUp<=2000000);
		
		logger.info("Response code : " + responseCode);
		
		if(amount==999) {
			logger.info("[ --- Finished TopUp_TSC_029 --- ]");
		}else if(amount==1001000) {
			logger.info("[ --- Finished TopUp_TSC_030 --- ]");
		}
		Thread.sleep(5);
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void TopUp_TSC_031() throws InterruptedException, ParseException{
//		'- User already registered
//		- User logged in
//		- Current balance is Rp.2.000.000
//		- Correct amount to top up Rp.1000
//		- Balance after top up over Rp.2000000
//		- Idempotency key is filled and unique
		logger.info("[ --- Started TopUp_TSC_031 --- ]");	
		
		// GET EXISTING REQUEST ID
		try {
			getRequestId = new File("./assets/generated-data/requestId.txt");
			requestIdReader = new Scanner(getRequestId);
			while (requestIdReader.hasNextLine()) {
				String data = requestIdReader.nextLine();
				requestId = data;
			}
		}catch (FileNotFoundException e) {
			System.out.println("An error occurred.");
		      e.printStackTrace();
		}
		
		// === GET BALANCE BEFORE TOP UP ===
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		JSONObject RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		JSONObject tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceBeforeTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance before top up : " + balanceBeforeTopUp);
		
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("amount", 1000000);
		RequestParams.put("requestId", generateRequestId());
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		String responseCode = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp.1.000.000");
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);	
		
		// === GET BALANCE AFTER TOP UP ===
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		
		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceAfterTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance after top up : " + balanceAfterTopUp);
		
		// === GET BALANCE BEFORE TOP UP ===
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		
		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceBeforeTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance before top up : " + balanceBeforeTopUp);
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("amount", 1000000);
		RequestParams.put("requestId", generateRequestId());
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		responseCode = json.get("code").toString();
		responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp.1.000.000");
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		balanceAfterTopUp = balanceBeforeTopUp + 1000000;
		logger.info("Balance after top up : " + balanceAfterTopUp);
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("amount", 1000);
		RequestParams.put("requestId", generateRequestId());
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		responseCode = json.get("code").toString();
		responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp.1000");
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION
		Assert.assertEquals(responseDescription, responseBalanceExceedMax);
		Assert.assertEquals(responseCode.toString(), "203");
		
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		
		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceAfterTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance after top up : " + balanceAfterTopUp);
		Assert.assertEquals(balanceAfterTopUp.toString(), "2000000");
		Assert.assertEquals(true, balanceAfterTopUp<=2000000);
		
		logger.info("Response code : " + responseCode);
		Thread.sleep(5);
		logger.info("[ --- Finished TopUp_TSC_031 --- ]");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void TopUp_TSC_032() throws InterruptedException, ParseException{
//		'- User already registered
//		- User logged in
//		- Current balance is Rp.2.000.000
//		- Correct amount to top up Rp.100.000
//		- Balance after top up over Rp.2000000
//		- Idempotency key is filled and unique
		logger.info("[ --- Started TopUp_TSC_032 --- ]");	
		
		// GET EXISTING REQUEST ID
		try {
			getRequestId = new File("./assets/generated-data/requestId.txt");
			requestIdReader = new Scanner(getRequestId);
			while (requestIdReader.hasNextLine()) {
				String data = requestIdReader.nextLine();
				requestId = data;
			}
		}catch (FileNotFoundException e) {
			System.out.println("An error occurred.");
		      e.printStackTrace();
		}
		
		// === GET BALANCE BEFORE TOP UP ===
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		JSONObject RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		JSONObject tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceBeforeTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance before top up : " + balanceBeforeTopUp);
		
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("amount", 1000000);
		RequestParams.put("requestId", generateRequestId());
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		String responseCode = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp.1.000.000");
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);	
		
		// === GET BALANCE AFTER TOP UP ===
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		
		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceAfterTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance after top up : " + balanceAfterTopUp);
		
		// === GET BALANCE BEFORE TOP UP ===
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		
		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceBeforeTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance before top up : " + balanceBeforeTopUp);
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("amount", 1000000);
		RequestParams.put("requestId", generateRequestId());
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		responseCode = json.get("code").toString();
		responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp.1.000.000");
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		balanceAfterTopUp = balanceBeforeTopUp + 1000000;
		logger.info("Balance after top up : " + balanceAfterTopUp);
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("amount", 100000);
		RequestParams.put("requestId", generateRequestId());
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		responseCode = json.get("code").toString();
		responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp.100.000");
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION
		Assert.assertEquals(responseDescription, responseBalanceExceedMax);
		Assert.assertEquals(responseCode.toString(), "203");
		
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		
		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceAfterTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance after top up : " + balanceAfterTopUp);
		Assert.assertEquals(balanceAfterTopUp.toString(), "2000000");
		Assert.assertEquals(true, balanceAfterTopUp<=2000000);
		
		logger.info("Response code : " + responseCode);
		Thread.sleep(5);
		logger.info("[ --- Finished TopUp_TSC_032 --- ]");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void TopUp_TSC_033() throws InterruptedException, ParseException{
//		'- User already registered
//		- User logged in
//		- Current balance is Rp.2.000.000
//		- Correct amount to top up Rp.1.000.000
//		- Balance after top up over Rp.2000000
//		- Idempotency key is filled and unique
		logger.info("[ --- Started TopUp_TSC_033 --- ]");	
		
		// GET EXISTING REQUEST ID
		try {
			getRequestId = new File("./assets/generated-data/requestId.txt");
			requestIdReader = new Scanner(getRequestId);
			while (requestIdReader.hasNextLine()) {
				String data = requestIdReader.nextLine();
				requestId = data;
			}
		}catch (FileNotFoundException e) {
			System.out.println("An error occurred.");
		      e.printStackTrace();
		}
		
		// === GET BALANCE BEFORE TOP UP ===
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		JSONObject RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		JSONObject tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceBeforeTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance before top up : " + balanceBeforeTopUp);
		
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("amount", 1000000);
		RequestParams.put("requestId", generateRequestId());
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		String responseCode = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp.1.000.000");
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);	
		
		// === GET BALANCE AFTER TOP UP ===
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		
		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceAfterTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance after top up : " + balanceAfterTopUp);
		
		// === GET BALANCE BEFORE TOP UP ===
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		
		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceBeforeTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance before top up : " + balanceBeforeTopUp);
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("amount", 1000000);
		RequestParams.put("requestId", generateRequestId());
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		responseCode = json.get("code").toString();
		responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp.1.000.000");
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		balanceAfterTopUp = balanceBeforeTopUp + 1000000;
		logger.info("Balance after top up : " + balanceAfterTopUp);
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("amount", 1000000);
		RequestParams.put("requestId", generateRequestId());
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		responseCode = json.get("code").toString();
		responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp.1.000.000");
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION
		Assert.assertEquals(responseDescription, responseBalanceExceedMax);
		Assert.assertEquals(responseCode.toString(), "203");
		
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		
		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceAfterTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance after top up : " + balanceAfterTopUp);
		Assert.assertEquals(balanceAfterTopUp.toString(), "2000000");
		Assert.assertEquals(true, balanceAfterTopUp<=2000000);
		
		logger.info("Response code : " + responseCode);
		Thread.sleep(5);
		logger.info("[ --- Finished TopUp_TSC_033 --- ]");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void TopUp_TSC_034() throws InterruptedException, ParseException{
//		'- User already registered
//		- User logged in
//		- Current balance is Rp.2.000.000
//		- Correct amount to top up Rp.1000
//		- Balance after top up over Rp.2000000
//		- Idempotency key is filled but not unique
		logger.info("[ --- Started TopUp_TSC_034 --- ]");	
		
		// GET EXISTING REQUEST ID
		try {
			getRequestId = new File("./assets/generated-data/requestId.txt");
			requestIdReader = new Scanner(getRequestId);
			while (requestIdReader.hasNextLine()) {
				String data = requestIdReader.nextLine();
				requestId = data;
			}
		}catch (FileNotFoundException e) {
			System.out.println("An error occurred.");
		      e.printStackTrace();
		}
		
		// === GET BALANCE BEFORE TOP UP ===
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		JSONObject RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		JSONObject tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceBeforeTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance before top up : " + balanceBeforeTopUp);
		
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("amount", 1000000);
		RequestParams.put("requestId", generateRequestId());
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		String responseCode = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp.1.000.000");
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);	
		
		// === GET BALANCE AFTER TOP UP ===
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		
		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceAfterTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance after top up : " + balanceAfterTopUp);
		
		// === GET BALANCE BEFORE TOP UP ===
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		
		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceBeforeTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance before top up : " + balanceBeforeTopUp);
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("amount", 1000000);
		RequestParams.put("requestId", generateRequestId());
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		responseCode = json.get("code").toString();
		responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp.1.000.000");
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		balanceAfterTopUp = balanceBeforeTopUp + 1000000;
		logger.info("Balance after top up : " + balanceAfterTopUp);
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("amount", 1000);
		RequestParams.put("requestId", requestId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		responseCode = json.get("code").toString();
		responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp.1000");
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION
		Assert.assertEquals(responseDescription, responseStatusSuccess);
		Assert.assertEquals(responseCode.toString(), "300");
		
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		
		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceAfterTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance after top up : " + balanceAfterTopUp);
		Assert.assertEquals(balanceAfterTopUp.toString(), "2000000");
		Assert.assertEquals(true, balanceAfterTopUp<=2000000);
		
		logger.info("Response code : " + responseCode);
		Thread.sleep(5);
		logger.info("[ --- Finished TopUp_TSC_034 --- ]");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void TopUp_TSC_035() throws InterruptedException, ParseException{
//		'- User already registered
//		- User logged in
//		- Current balance is Rp.2.000.000
//		- Correct amount to top up Rp.100.000
//		- Balance after top up over Rp.2000000
//		- Idempotency key is filled but not unique
		logger.info("[ --- Started TopUp_TSC_035 --- ]");	
		
		// GET EXISTING REQUEST ID
		try {
			getRequestId = new File("./assets/generated-data/requestId.txt");
			requestIdReader = new Scanner(getRequestId);
			while (requestIdReader.hasNextLine()) {
				String data = requestIdReader.nextLine();
				requestId = data;
			}
		}catch (FileNotFoundException e) {
			System.out.println("An error occurred.");
		      e.printStackTrace();
		}
		
		// === GET BALANCE BEFORE TOP UP ===
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		JSONObject RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		JSONObject tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceBeforeTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance before top up : " + balanceBeforeTopUp);
		
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("amount", 1000000);
		RequestParams.put("requestId", generateRequestId());
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		String responseCode = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp.1.000.000");
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);	
		
		// === GET BALANCE AFTER TOP UP ===
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		
		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceAfterTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance after top up : " + balanceAfterTopUp);
		
		// === GET BALANCE BEFORE TOP UP ===
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		
		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceBeforeTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance before top up : " + balanceBeforeTopUp);
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("amount", 1000000);
		RequestParams.put("requestId", generateRequestId());
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		responseCode = json.get("code").toString();
		responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp.1.000.000");
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		balanceAfterTopUp = balanceBeforeTopUp + 1000000;
		logger.info("Balance after top up : " + balanceAfterTopUp);
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("amount", 100000);
		RequestParams.put("requestId", requestId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		responseCode = json.get("code").toString();
		responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp.100.000");
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION
		Assert.assertEquals(responseDescription, responseStatusSuccess);
		Assert.assertEquals(responseCode.toString(), "300");
		
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		
		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceAfterTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance after top up : " + balanceAfterTopUp);
		Assert.assertEquals(balanceAfterTopUp.toString(), "2000000");
		Assert.assertEquals(true, balanceAfterTopUp<=2000000);
		
		logger.info("Response code : " + responseCode);
		Thread.sleep(5);
		logger.info("[ --- Finished TopUp_TSC_035 --- ]");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void TopUp_TSC_036() throws InterruptedException, ParseException{
//		'- User already registered
//		- User logged in
//		- Current balance is Rp.2.000.000
//		- Correct amount to top up Rp.1.000.000
//		- Balance after top up over Rp.2000000
//		- Idempotency key is filled but not unique
		logger.info("[ --- Started TopUp_TSC_036 --- ]");	
		
		// GET EXISTING REQUEST ID
		try {
			getRequestId = new File("./assets/generated-data/requestId.txt");
			requestIdReader = new Scanner(getRequestId);
			while (requestIdReader.hasNextLine()) {
				String data = requestIdReader.nextLine();
				requestId = data;
			}
		}catch (FileNotFoundException e) {
			System.out.println("An error occurred.");
		      e.printStackTrace();
		}
		
		// === GET BALANCE BEFORE TOP UP ===
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		JSONObject RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		JSONObject tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceBeforeTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance before top up : " + balanceBeforeTopUp);
		
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("amount", 1000000);
		RequestParams.put("requestId", generateRequestId());
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		String responseCode = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp.1.000.000");
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);	
		
		// === GET BALANCE AFTER TOP UP ===
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		
		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceAfterTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance after top up : " + balanceAfterTopUp);
		
		// === GET BALANCE BEFORE TOP UP ===
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		
		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceBeforeTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance before top up : " + balanceBeforeTopUp);
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("amount", 1000000);
		RequestParams.put("requestId", generateRequestId());
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		responseCode = json.get("code").toString();
		responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp.1.000.000");
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		balanceAfterTopUp = balanceBeforeTopUp + 1000000;
		logger.info("Balance after top up : " + balanceAfterTopUp);
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("amount", 1000000);
		RequestParams.put("requestId", requestId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		responseCode = json.get("code").toString();
		responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp.1.000.000");
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION
		Assert.assertEquals(responseDescription, responseStatusSuccess);
		Assert.assertEquals(responseCode.toString(), "300");
		
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		
		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceAfterTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance after top up : " + balanceAfterTopUp);
		Assert.assertEquals(balanceAfterTopUp.toString(), "2000000");
		Assert.assertEquals(true, balanceAfterTopUp<=2000000);
		
		logger.info("Response code : " + responseCode);
		Thread.sleep(5);
		logger.info("[ --- Finished TopUp_TSC_036 --- ]");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	@Parameters({"amountToTopUp"})
	void TopUp_TSC_037(Integer amount) throws InterruptedException, ParseException{
//		'- User already registered
//		- User logged in
//		- Current balance is Rp.2.000.000
//		- Correct amount to top up Rp.1000
//		- Balance after top up over Rp.2000000
//		- Idempotency key is empty	
		if(amount==1000) {
			logger.info("[ --- Started TopUp_TSC_037 --- ]");
		}else if(amount==100000) {
			logger.info("[ --- Started TopUp_TSC_038 --- ]");
		}else if(amount==1000000) {
			logger.info("[ --- Started TopUp_TSC_039 --- ]");
		}else if(amount==999){
			logger.info("[ --- Started TopUp_TSC_044 --- ]");
		}else if(amount==1001000) {
			logger.info("[ --- Started TopUp_TSC_045 --- ]");
		}
		
		// GET EXISTING REQUEST ID
		try {
			getRequestId = new File("./assets/generated-data/requestId.txt");
			requestIdReader = new Scanner(getRequestId);
			while (requestIdReader.hasNextLine()) {
				String data = requestIdReader.nextLine();
				requestId = data;
			}
		}catch (FileNotFoundException e) {
			System.out.println("An error occurred.");
		      e.printStackTrace();
		}
		
		// === GET BALANCE BEFORE TOP UP ===
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		JSONObject RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		JSONObject tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceBeforeTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance before top up : " + balanceBeforeTopUp);
		
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("amount", 1000000);
		RequestParams.put("requestId", generateRequestId());
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		String responseCode = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp.1.000.000");
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);	
		
		// === GET BALANCE AFTER TOP UP ===
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		
		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceAfterTopUp = Integer.parseInt(tempData.get("balance").toString());
		Assert.assertEquals(balanceAfterTopUp.toString(), "1000000");
		logger.info("Balance after top up : " + balanceAfterTopUp);
		
		// === GET BALANCE BEFORE TOP UP ===
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		
		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceBeforeTopUp = Integer.parseInt(tempData.get("balance").toString());
		Assert.assertEquals(balanceBeforeTopUp.toString(), "1000000");
		logger.info("Balance before top up : " + balanceBeforeTopUp);
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("amount", 1000000);
		RequestParams.put("requestId", generateRequestId());
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		responseCode = json.get("code").toString();
		responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp.1.000.000");
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		balanceAfterTopUp = balanceBeforeTopUp + 1000000;
		logger.info("Balance after top up : " + balanceAfterTopUp);
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("amount", amount);
		RequestParams.put("requestId", null);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		responseCode = json.get("code").toString();
		responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp."+amount);
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION
		Assert.assertEquals(responseDescription, responseStatusNullParams);
		Assert.assertEquals(responseCode.toString(), "106");
		
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		
		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceAfterTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance after top up : " + balanceAfterTopUp);
		Assert.assertEquals(balanceAfterTopUp.toString(), "2000000");
		Assert.assertEquals(true, balanceAfterTopUp<=2000000);
		
		logger.info("Response code : " + responseCode);
		if(amount==1000) {
			logger.info("[ --- Finished TopUp_TSC_037 --- ]");
		}else if(amount==100000) {
			logger.info("[ --- Finished TopUp_TSC_038 --- ]");
		}else if(amount==1000000) {
			logger.info("[ --- Finished TopUp_TSC_039 --- ]");
		}else if(amount==999){
			logger.info("[ --- Finished TopUp_TSC_044 --- ]");
		}else if(amount==1001000) {
			logger.info("[ --- Finished TopUp_TSC_045 --- ]");
		}
		Thread.sleep(5);
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void TopUp_TSC_040() throws InterruptedException, ParseException{
//		'- User already registered
//		- User logged in
//		- Current balance is Rp.2.000.000
//		- Incorrect amount to top up Rp.999
//		- Balance after top up over Rp.2.000.000
//		- Idempotency key is filled and unique
		logger.info("[ --- Started TopUp_TSC_040 --- ]");	
		
		// GET EXISTING REQUEST ID
		try {
			getRequestId = new File("./assets/generated-data/requestId.txt");
			requestIdReader = new Scanner(getRequestId);
			while (requestIdReader.hasNextLine()) {
				String data = requestIdReader.nextLine();
				requestId = data;
			}
		}catch (FileNotFoundException e) {
			System.out.println("An error occurred.");
		      e.printStackTrace();
		}
		
		// === GET BALANCE BEFORE TOP UP ===
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		JSONObject RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		JSONObject tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceBeforeTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance before top up : " + balanceBeforeTopUp);
		
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("amount", 1000000);
		RequestParams.put("requestId", generateRequestId());
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		String responseCode = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp.1.000.000");
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);	
		
		// === GET BALANCE AFTER TOP UP ===
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		
		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceAfterTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance after top up : " + balanceAfterTopUp);
		
		// === GET BALANCE BEFORE TOP UP ===
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		
		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceBeforeTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance before top up : " + balanceBeforeTopUp);
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("amount", 1000000);
		RequestParams.put("requestId", generateRequestId());
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		responseCode = json.get("code").toString();
		responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp.1.000.000");
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		balanceAfterTopUp = balanceBeforeTopUp + 1000000;
		logger.info("Balance after top up : " + balanceAfterTopUp);
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("amount", 999);
		RequestParams.put("requestId", generateRequestId());
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		responseCode = json.get("code").toString();
		responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp.999");
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION
		Assert.assertEquals(responseDescription, responseStatusTopUpUnderMin);
		Assert.assertEquals(responseCode.toString(), "103");
		
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		
		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceAfterTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance after top up : " + balanceAfterTopUp);
		Assert.assertEquals(balanceAfterTopUp.toString(), "2000000");
		Assert.assertEquals(true, balanceAfterTopUp==2000000);
		
		logger.info("Response code : " + responseCode);
		Thread.sleep(5);
		logger.info("[ --- Finished TopUp_TSC_040 --- ]");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void TopUp_TSC_041() throws InterruptedException, ParseException{
//		'- User already registered
//		- User logged in
//		- Current balance is Rp.2.000.000
//		- Incorrect amount to top up Rp.1.001.000
//		- Balance after top up over Rp.2.000.000
//		- Idempotency key is filled and unique
		logger.info("[ --- Started TopUp_TSC_041 --- ]");	
		
		// GET EXISTING REQUEST ID
		try {
			getRequestId = new File("./assets/generated-data/requestId.txt");
			requestIdReader = new Scanner(getRequestId);
			while (requestIdReader.hasNextLine()) {
				String data = requestIdReader.nextLine();
				requestId = data;
			}
		}catch (FileNotFoundException e) {
			System.out.println("An error occurred.");
		      e.printStackTrace();
		}
		
		// === GET BALANCE BEFORE TOP UP ===
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		JSONObject RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		JSONObject tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceBeforeTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance before top up : " + balanceBeforeTopUp);
		
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("amount", 1000000);
		RequestParams.put("requestId", generateRequestId());
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		String responseCode = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp.1.000.000");
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);	
		
		// === GET BALANCE AFTER TOP UP ===
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		
		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceAfterTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance after top up : " + balanceAfterTopUp);
		
		// === GET BALANCE BEFORE TOP UP ===
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		
		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceBeforeTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance before top up : " + balanceBeforeTopUp);
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("amount", 1000000);
		RequestParams.put("requestId", generateRequestId());
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		responseCode = json.get("code").toString();
		responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp.1.000.000");
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		balanceAfterTopUp = balanceBeforeTopUp + 1000000;
		logger.info("Balance after top up : " + balanceAfterTopUp);
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("amount", 1001000);
		RequestParams.put("requestId", generateRequestId());
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		responseCode = json.get("code").toString();
		responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp.1.001.000");
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION
		Assert.assertEquals(responseDescription, responseStatusTopUpOverMax);
		Assert.assertEquals(responseCode.toString(), "107");
		
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		
		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceAfterTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance after top up : " + balanceAfterTopUp);
		Assert.assertEquals(balanceAfterTopUp.toString(), "2000000");
		Assert.assertEquals(true, balanceAfterTopUp==2000000);
		
		logger.info("Response code : " + responseCode);
		Thread.sleep(5);
		logger.info("[ --- Finished TopUp_TSC_041 --- ]");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void TopUp_TSC_042() throws InterruptedException, ParseException{
//		'- User already registered
//		- User logged in
//		- Current balance is Rp.2.000.000
//		- Incorrect amount to top up Rp.999
//		- Balance after top up over Rp.2.000.000
//		- Idempotency key is filled but not unique
		logger.info("[ --- Started TopUp_TSC_042 --- ]");	
		
		// GET EXISTING REQUEST ID
		try {
			getRequestId = new File("./assets/generated-data/requestId.txt");
			requestIdReader = new Scanner(getRequestId);
			while (requestIdReader.hasNextLine()) {
				String data = requestIdReader.nextLine();
				requestId = data;
			}
		}catch (FileNotFoundException e) {
			System.out.println("An error occurred.");
		      e.printStackTrace();
		}
		
		// === GET BALANCE BEFORE TOP UP ===
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		JSONObject RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		JSONObject tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceBeforeTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance before top up : " + balanceBeforeTopUp);
		
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("amount", 1000000);
		RequestParams.put("requestId", generateRequestId());
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		String responseCode = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp.1.000.000");
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);	
		
		// === GET BALANCE AFTER TOP UP ===
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		
		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceAfterTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance after top up : " + balanceAfterTopUp);
		
		// === GET BALANCE BEFORE TOP UP ===
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		
		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceBeforeTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance before top up : " + balanceBeforeTopUp);
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("amount", 1000000);
		RequestParams.put("requestId", generateRequestId());
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		responseCode = json.get("code").toString();
		responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp.1.000.000");
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		balanceAfterTopUp = balanceBeforeTopUp + 1000000;
		logger.info("Balance after top up : " + balanceAfterTopUp);
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("amount", 999);
		RequestParams.put("requestId", requestId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		responseCode = json.get("code").toString();
		responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp.999");
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION
		Assert.assertEquals(responseDescription, responseStatusSuccess);
		Assert.assertEquals(responseCode.toString(), "300");
		
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		
		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceAfterTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance after top up : " + balanceAfterTopUp);
		Assert.assertEquals(balanceAfterTopUp.toString(), "2000000");
		Assert.assertEquals(true, balanceAfterTopUp==2000000);
		
		logger.info("Response code : " + responseCode);
		Thread.sleep(5);
		logger.info("[ --- Finished TopUp_TSC_042 --- ]");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void TopUp_TSC_043() throws InterruptedException, ParseException{
//		'- User already registered
//		- User logged in
//		- Current balance is Rp.2.000.000
//		- Incorrect amount to top up Rp.1.001.000
//		- Balance after top up over Rp.2.000.000
//		- Idempotency key is filled but not unique
		logger.info("[ --- Started TopUp_TSC_043 --- ]");	
		
		// GET EXISTING REQUEST ID
		try {
			getRequestId = new File("./assets/generated-data/requestId.txt");
			requestIdReader = new Scanner(getRequestId);
			while (requestIdReader.hasNextLine()) {
				String data = requestIdReader.nextLine();
				requestId = data;
			}
		}catch (FileNotFoundException e) {
			System.out.println("An error occurred.");
		      e.printStackTrace();
		}
		
		// === GET BALANCE BEFORE TOP UP ===
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		JSONObject RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		JSONObject tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceBeforeTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance before top up : " + balanceBeforeTopUp);
		
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("amount", 1000000);
		RequestParams.put("requestId", generateRequestId());
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		String responseCode = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp.1.000.000");
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);	
		
		// === GET BALANCE AFTER TOP UP ===
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		
		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceAfterTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance after top up : " + balanceAfterTopUp);
		
		// === GET BALANCE BEFORE TOP UP ===
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		
		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceBeforeTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance before top up : " + balanceBeforeTopUp);
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("amount", 1000000);
		RequestParams.put("requestId", generateRequestId());
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		responseCode = json.get("code").toString();
		responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp.1.000.000");
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		balanceAfterTopUp = balanceBeforeTopUp + 1000000;
		logger.info("Balance after top up : " + balanceAfterTopUp);
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("amount", 1001000);
		RequestParams.put("requestId", requestId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		responseCode = json.get("code").toString();
		responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp.1.001.000");
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION
		Assert.assertEquals(responseDescription, responseStatusSuccess);
		Assert.assertEquals(responseCode.toString(), "300");
		
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		
		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceAfterTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance after top up : " + balanceAfterTopUp);
		Assert.assertEquals(balanceAfterTopUp.toString(), "2000000");
		Assert.assertEquals(true, balanceAfterTopUp==2000000);
		
		logger.info("Response code : " + responseCode);
		Thread.sleep(5);
		logger.info("[ --- Finished TopUp_TSC_043 --- ]");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void TopUp_TSC_046() throws InterruptedException, ParseException{
//		'- User already registered
//		- User not logged in
//		- Current balance under Rp.2.000.000
//		- Correct amount to top up Rp.1000
//		- Balance after top up over Rp.2.000.000
//		- Idempotency key is filled and unique
		logger.info("[ --- Started TopUp_TSC_046 --- ]");	
		
		// GET EXISTING REQUEST ID
		try {
			getRequestId = new File("./assets/generated-data/requestId.txt");
			requestIdReader = new Scanner(getRequestId);
			while (requestIdReader.hasNextLine()) {
				String data = requestIdReader.nextLine();
				requestId = data;
			}
		}catch (FileNotFoundException e) {
			System.out.println("An error occurred.");
		      e.printStackTrace();
		}
		
		// === GET BALANCE BEFORE TOP UP ===
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		JSONObject RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		JSONObject tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceBeforeTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance before top up : " + balanceBeforeTopUp);
		
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("amount", 1000000);
		RequestParams.put("requestId", generateRequestId());
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		String responseCode = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp.1.000.000");
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);	
		
		// === GET BALANCE AFTER TOP UP ===
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		
		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceAfterTopUp = Integer.parseInt(tempData.get("balance").toString());
		Assert.assertEquals(balanceAfterTopUp.toString(), "1000000");
		logger.info("Balance after top up : " + balanceAfterTopUp);
		
		// === GET BALANCE BEFORE TOP UP ===
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		
		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceBeforeTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance before top up : " + balanceBeforeTopUp);
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("amount", 999999);
		RequestParams.put("requestId", generateRequestId());
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		responseCode = json.get("code").toString();
		responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp.999.999");
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		balanceAfterTopUp = balanceBeforeTopUp + 999999;
		Assert.assertEquals(balanceAfterTopUp.toString(), "1999999");
		logger.info("Balance after top up : " + balanceAfterTopUp);
		
		// LOGOUT PROCESS
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		httpRequest.cookie("JSESSIONID", sessionId);
		
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointLogout);
		
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		String responseHeader = response.getHeader("Set-Cookie");
		String responseBody = response.getBody().asString();
		logger.info("Response Body : " + responseBody);
		
		responseCode = json.get("code").toString();
		responseDescription = json.get("description").toString();
		
		logger.info("Response Header : " + responseHeader);
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		
		
		// ASSERTION
		Assert.assertEquals(responseDescription, "SUCCESS");
		Assert.assertEquals(responseCode.toString(), "300");
		
		if(responseCode.toString().equals("300")) {	
			logger.info("LOGOUT SUCCESS");
		}else {
			logger.info("LOGOUT FAILED");
		}	
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("amount", 1000);
		RequestParams.put("requestId", requestId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		responseCode = json.get("code").toString();
		responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp.1000");
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION
		Assert.assertEquals(responseDescription, responseStatusUserNotLoggedIn);
		Assert.assertEquals(responseCode.toString(), "301");
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		RequestParams = new JSONObject();
		RequestParams.put("phone", userPhone);
		RequestParams.put("password", userPassword);
		
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointLogin);
		
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		responseHeader = response.getHeader("Set-Cookie");
		responseBody = response.getBody().asString();
		responseCode = json.get("code").toString();
		responseDescription = json.get("description").toString();
		
		String cookieId = between(responseHeader, "=", "; Path=/; Secure; HttpOnly");
		logger.info("Response Header : " + responseHeader);
		logger.info("Response Body : " + responseBody);
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		
		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		Assert.assertEquals(responseDescription, "Success");
		Assert.assertEquals(responseCode, "300");
		if(responseCode.toString().equals("300")) {
			// Save session data to file sessionId.txt
			try {
                FileWriter generateSessionId = new FileWriter("assets/generated-data/sessionId.txt");
                generateSessionId.write(cookieId);
                generateSessionId.close();
            } catch (IOException e) {
                System.err.format("IOException: %s%n", e);
            }
			logger.info("=== LOGIN SUCCESS ===");
			logger.info("User ID : "+tempData.get("id".toString()));
			logger.info("Name : "+tempData.get("name".toString()));
			logger.info("Phone : "+tempData.get("phone".toString()));
			logger.info("email : "+tempData.get("email".toString()));
			logger.info("Balance : "+tempData.get("balance".toString()));
		}else {
			logger.info("=== LOGIN FAILED ===");
		}	
		
		Assert.assertEquals(tempData.get("balance").toString(), "1999999");
		Assert.assertEquals(true, balanceAfterTopUp==1999999);
		
		logger.info("Response code : " + responseCode);
		Thread.sleep(5);
		logger.info("[ --- Finished TopUp_TSC_046 --- ]");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void TopUp_TSC_047() throws InterruptedException, ParseException{
//		'- User already registered
//		- User not logged in
//		- Current balance under Rp.2.000.000
//		- Correct amount to top up Rp.100.000
//		- Balance after top up over Rp.2.000.000
//		- Idempotency key is filled and unique
		logger.info("[ --- Started TopUp_TSC_047 --- ]");	
		
		// GET EXISTING REQUEST ID
		try {
			getRequestId = new File("./assets/generated-data/requestId.txt");
			requestIdReader = new Scanner(getRequestId);
			while (requestIdReader.hasNextLine()) {
				String data = requestIdReader.nextLine();
				requestId = data;
			}
		}catch (FileNotFoundException e) {
			System.out.println("An error occurred.");
		      e.printStackTrace();
		}
		
		// === GET BALANCE BEFORE TOP UP ===
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		JSONObject RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		JSONObject tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceBeforeTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance before top up : " + balanceBeforeTopUp);
		
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("amount", 1000000);
		RequestParams.put("requestId", generateRequestId());
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		String responseCode = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp.1.000.000");
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);	
		
		// === GET BALANCE AFTER TOP UP ===
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		
		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceAfterTopUp = Integer.parseInt(tempData.get("balance").toString());
		Assert.assertEquals(balanceAfterTopUp.toString(), "1000000");
		logger.info("Balance after top up : " + balanceAfterTopUp);
		
		// === GET BALANCE BEFORE TOP UP ===
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		
		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceBeforeTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance before top up : " + balanceBeforeTopUp);
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("amount", 999999);
		RequestParams.put("requestId", generateRequestId());
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		responseCode = json.get("code").toString();
		responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp.999.999");
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		balanceAfterTopUp = balanceBeforeTopUp + 999999;
		Assert.assertEquals(balanceAfterTopUp.toString(), "1999999");
		logger.info("Balance after top up : " + balanceAfterTopUp);
		
		// LOGOUT PROCESS
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		httpRequest.cookie("JSESSIONID", sessionId);
		
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointLogout);
		
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		String responseHeader = response.getHeader("Set-Cookie");
		String responseBody = response.getBody().asString();
		logger.info("Response Body : " + responseBody);
		
		responseCode = json.get("code").toString();
		responseDescription = json.get("description").toString();
		
		logger.info("Response Header : " + responseHeader);
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		
		
		// ASSERTION
		Assert.assertEquals(responseDescription, "SUCCESS");
		Assert.assertEquals(responseCode.toString(), "300");
		
		if(responseCode.toString().equals("300")) {	
			logger.info("LOGOUT SUCCESS");
		}else {
			logger.info("LOGOUT FAILED");
		}	
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("amount", 100000);
		RequestParams.put("requestId", requestId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		responseCode = json.get("code").toString();
		responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp.100.000");
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION
		Assert.assertEquals(responseDescription, responseStatusUserNotLoggedIn);
		Assert.assertEquals(responseCode.toString(), "301");
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		RequestParams = new JSONObject();
		RequestParams.put("phone", userPhone);
		RequestParams.put("password", userPassword);
		
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointLogin);
		
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		responseHeader = response.getHeader("Set-Cookie");
		responseBody = response.getBody().asString();
		responseCode = json.get("code").toString();
		responseDescription = json.get("description").toString();
		
		String cookieId = between(responseHeader, "=", "; Path=/; Secure; HttpOnly");
		logger.info("Response Header : " + responseHeader);
		logger.info("Response Body : " + responseBody);
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		
		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		Assert.assertEquals(responseDescription, "Success");
		Assert.assertEquals(responseCode, "300");
		if(responseCode.toString().equals("300")) {
			// Save session data to file sessionId.txt
			try {
                FileWriter generateSessionId = new FileWriter("assets/generated-data/sessionId.txt");
                generateSessionId.write(cookieId);
                generateSessionId.close();
            } catch (IOException e) {
                System.err.format("IOException: %s%n", e);
            }
			logger.info("=== LOGIN SUCCESS ===");
			logger.info("User ID : "+tempData.get("id".toString()));
			logger.info("Name : "+tempData.get("name".toString()));
			logger.info("Phone : "+tempData.get("phone".toString()));
			logger.info("email : "+tempData.get("email".toString()));
			logger.info("Balance : "+tempData.get("balance".toString()));
		}else {
			logger.info("=== LOGIN FAILED ===");
		}	
		
		Assert.assertEquals(tempData.get("balance").toString(), "1999999");
		Assert.assertEquals(true, balanceAfterTopUp==1999999);
		
		logger.info("Response code : " + responseCode);
		Thread.sleep(5);
		logger.info("[ --- Finished TopUp_TSC_047 --- ]");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void TopUp_TSC_048() throws InterruptedException, ParseException{
//		'- User already registered
//		- User not logged in
//		- Current balance under Rp.2.000.000
//		- Correct amount to top up Rp.1.000.000
//		- Balance after top up over Rp.2.000.000
//		- Idempotency key is filled and unique
		logger.info("[ --- Started TopUp_TSC_048 --- ]");	
		
		// GET EXISTING REQUEST ID
		try {
			getRequestId = new File("./assets/generated-data/requestId.txt");
			requestIdReader = new Scanner(getRequestId);
			while (requestIdReader.hasNextLine()) {
				String data = requestIdReader.nextLine();
				requestId = data;
			}
		}catch (FileNotFoundException e) {
			System.out.println("An error occurred.");
		      e.printStackTrace();
		}
		
		// === GET BALANCE BEFORE TOP UP ===
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		JSONObject RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		JSONObject tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceBeforeTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance before top up : " + balanceBeforeTopUp);
		
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("amount", 1000000);
		RequestParams.put("requestId", generateRequestId());
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		String responseCode = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp.1.000.000");
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);	
		
		// === GET BALANCE AFTER TOP UP ===
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		
		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceAfterTopUp = Integer.parseInt(tempData.get("balance").toString());
		Assert.assertEquals(balanceAfterTopUp.toString(), "1000000");
		logger.info("Balance after top up : " + balanceAfterTopUp);
		
		// === GET BALANCE BEFORE TOP UP ===
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		
		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceBeforeTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance before top up : " + balanceBeforeTopUp);
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("amount", 999999);
		RequestParams.put("requestId", generateRequestId());
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		responseCode = json.get("code").toString();
		responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp.999.999");
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		balanceAfterTopUp = balanceBeforeTopUp + 999999;
		Assert.assertEquals(balanceAfterTopUp.toString(), "1999999");
		logger.info("Balance after top up : " + balanceAfterTopUp);
		
		// LOGOUT PROCESS
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		httpRequest.cookie("JSESSIONID", sessionId);
		
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointLogout);
		
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		String responseHeader = response.getHeader("Set-Cookie");
		String responseBody = response.getBody().asString();
		logger.info("Response Body : " + responseBody);
		
		responseCode = json.get("code").toString();
		responseDescription = json.get("description").toString();
		
		logger.info("Response Header : " + responseHeader);
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		
		
		// ASSERTION
		Assert.assertEquals(responseDescription, "SUCCESS");
		Assert.assertEquals(responseCode.toString(), "300");
		
		if(responseCode.toString().equals("300")) {	
			logger.info("LOGOUT SUCCESS");
		}else {
			logger.info("LOGOUT FAILED");
		}	
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("amount", 1000000);
		RequestParams.put("requestId", requestId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		responseCode = json.get("code").toString();
		responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp.1.000.000");
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION
		Assert.assertEquals(responseDescription, responseStatusUserNotLoggedIn);
		Assert.assertEquals(responseCode.toString(), "301");
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		RequestParams = new JSONObject();
		RequestParams.put("phone", userPhone);
		RequestParams.put("password", userPassword);
		
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointLogin);
		
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		responseHeader = response.getHeader("Set-Cookie");
		responseBody = response.getBody().asString();
		responseCode = json.get("code").toString();
		responseDescription = json.get("description").toString();
		
		String cookieId = between(responseHeader, "=", "; Path=/; Secure; HttpOnly");
		logger.info("Response Header : " + responseHeader);
		logger.info("Response Body : " + responseBody);
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		
		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		Assert.assertEquals(responseDescription, "Success");
		Assert.assertEquals(responseCode, "300");
		if(responseCode.toString().equals("300")) {
			// Save session data to file sessionId.txt
			try {
                FileWriter generateSessionId = new FileWriter("assets/generated-data/sessionId.txt");
                generateSessionId.write(cookieId);
                generateSessionId.close();
            } catch (IOException e) {
                System.err.format("IOException: %s%n", e);
            }
			logger.info("=== LOGIN SUCCESS ===");
			logger.info("User ID : "+tempData.get("id".toString()));
			logger.info("Name : "+tempData.get("name".toString()));
			logger.info("Phone : "+tempData.get("phone".toString()));
			logger.info("email : "+tempData.get("email".toString()));
			logger.info("Balance : "+tempData.get("balance".toString()));
		}else {
			logger.info("=== LOGIN FAILED ===");
		}	
		
		Assert.assertEquals(tempData.get("balance").toString(), "1999999");
		Assert.assertEquals(true, balanceAfterTopUp==1999999);
		
		logger.info("Response code : " + responseCode);
		Thread.sleep(5);
		logger.info("[ --- Finished TopUp_TSC_048 --- ]");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	@Parameters({"amountToTopUp"})
	void TopUp_TSC_052(Integer amount) throws InterruptedException, ParseException{
//		'- User already registered
//		- User not logged in
//		- Current balance under Rp.2.000.000
//		- Correct amount to top up Rp.1000
//		- Balance after top up over Rp.2.000.000
//		- Idempotency key is empty
		if(amount==1000) {
			logger.info("[ --- Started TopUp_TSC_052 --- ]");
		}else if(amount==100000) {
			logger.info("[ --- Started TopUp_TSC_053 --- ]");
		}else if(amount==1000000) {
			logger.info("[ --- Started TopUp_TSC_054 --- ]");
		}
			
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		JSONObject RequestParams = new JSONObject();
		RequestParams.put("amount", 1000000);
		RequestParams.put("requestId", generateRequestId());
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		String responseCode = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp.1.000.000");
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		
		JSONObject tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		
		// ASSERTION
		Assert.assertEquals(responseDescription, responseStatusSuccess);
		Assert.assertEquals(responseCode, "300");	
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("amount", 999999);
		RequestParams.put("requestId", generateRequestId());
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		responseCode = json.get("code").toString();
		responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp.999.999");
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		
		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		
		// ASSERTION
		Assert.assertEquals(responseDescription, responseStatusSuccess);
		Assert.assertEquals(responseCode, "300");		

		
		// === GET BALANCE BEFORE TOP UP ===
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		
		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceBeforeTopUp = Integer.parseInt(tempData.get("balance").toString());
		Assert.assertEquals(balanceBeforeTopUp.toString(), "1999999");
		logger.info("Balance before top up : " + balanceBeforeTopUp);
		
		

		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointLogout);
		
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		String responseHeader = response.getHeader("Set-Cookie");
		String responseBody = response.getBody().asString();
		logger.info("Response Body : " + responseBody);
		
		responseCode = json.get("code").toString();
		responseDescription = json.get("description").toString();
		
		logger.info("Response Header : " + responseHeader);
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		
		
		// ASSERTION
		Assert.assertEquals(responseDescription, "SUCCESS");
		Assert.assertEquals(responseCode.toString(), "300");
		
		if(responseCode.toString().equals("300")) {	
			logger.info("LOGOUT SUCCESS");
		}else {
			logger.info("LOGOUT FAILED");
		}	
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("amount", amount);
		RequestParams.put("requestId", null);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		responseCode = json.get("code").toString();
		responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp."+amount);
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		
		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		
		// ASSERTION
		Assert.assertEquals(responseDescription, responseStatusUserNotLoggedIn);
		Assert.assertEquals(responseCode, "301");
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		RequestParams = new JSONObject();		
		RequestParams.put("phone", userPhone);
		RequestParams.put("password", userPassword);
		
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointLogin);
		
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		responseHeader = response.getHeader("Set-Cookie");
		responseBody = response.getBody().asString();
		responseCode = json.get("code").toString();
		responseDescription = json.get("description").toString();
		
		String cookieId = between(responseHeader, "=", "; Path=/; Secure; HttpOnly");
		logger.info("Response Header : " + responseHeader);
		logger.info("Response Body : " + responseBody);
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);

		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		Assert.assertEquals(responseDescription, "Success");
		Assert.assertEquals(responseCode, "300");		
		Assert.assertEquals(tempData.get("phone").toString(), userPhone);
		Assert.assertEquals(tempData.get("balance").toString(), "1999999");
		if(responseCode.toString().equals("300")) {
			// Save session data to file sessionId.txt
			try {
                FileWriter generateSessionId = new FileWriter("assets/generated-data/sessionId.txt");
                generateSessionId.write(cookieId);
                generateSessionId.close();
            } catch (IOException e) {
                System.err.format("IOException: %s%n", e);
            }
			logger.info("=== LOGIN SUCCESS ===");
			logger.info("User ID : "+tempData.get("id".toString()));
			logger.info("Name : "+tempData.get("name".toString()));
			logger.info("Phone : "+tempData.get("phone".toString()));
			logger.info("email : "+tempData.get("email".toString()));
			logger.info("Balance : "+tempData.get("balance".toString()));
		}else {
			logger.info("=== LOGIN FAILED ===");
		}
		if(amount==1000) {
			logger.info("[ --- Finished TopUp_TSC_052 --- ]");
		}else if(amount==100000) {
			logger.info("[ --- Finished TopUp_TSC_053 --- ]");
		}else if(amount==1000000) {
			logger.info("[ --- Finished TopUp_TSC_054 --- ]");
		}
		Thread.sleep(5);
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void TopUp_TSC_055() throws InterruptedException, ParseException{
//		'- User already registered
//		- User not logged in
//		- Current balance under Rp.2.000.000
//		- Correct amount to top up Rp.1000
//		- Balance after top up is or under Rp.2.000.000
//		- Idempotency key is filled and unique
		logger.info("[ --- Started TopUp_TSC_055 --- ]");	
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		JSONObject RequestParams = new JSONObject();
		RequestParams.put("amount", 1000);
		RequestParams.put("requestId", generateRequestId());
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		String responseCode = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp.1000");
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION
		Assert.assertEquals(responseDescription, responseStatusUserNotLoggedIn);
		Assert.assertEquals(responseCode.toString(), "301");
		
		RequestParams.put("phone", userPhone);
		RequestParams.put("password", userPassword);
		
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointLogin);
		
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		String responseHeader = response.getHeader("Set-Cookie");
		String responseBody = response.getBody().asString();
		responseCode = json.get("code").toString();
		responseDescription = json.get("description").toString();
	
		logger.info("Response Header : " + responseHeader);
		logger.info("Response Body : " + responseBody);
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		
		JSONObject tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		Assert.assertEquals(responseDescription, "Success");
		Assert.assertEquals(responseCode, "300");		
		Assert.assertEquals(tempData.get("balance").toString(), "0");
		logger.info("User Balance : " + tempData.get("balance"));
		Thread.sleep(5);
		logger.info("[ --- Finished TopUp_TSC_055 --- ]");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void TopUp_TSC_056() throws InterruptedException, ParseException{
//		'- User already registered
//		- User not logged in
//		- Current balance under Rp.2.000.000
//		- Correct amount to top up Rp.100.000
//		- Balance after top up is or under Rp.2.000.000
//		- Idempotency key is filled and unique
		logger.info("[ --- Started TopUp_TSC_056 --- ]");	
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		JSONObject RequestParams = new JSONObject();
		RequestParams.put("amount", 100000);
		RequestParams.put("requestId", generateRequestId());
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		String responseCode = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp.100.000");
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION
		Assert.assertEquals(responseDescription, responseStatusUserNotLoggedIn);
		Assert.assertEquals(responseCode.toString(), "301");
		
		RequestParams.put("phone", userPhone);
		RequestParams.put("password", userPassword);
		
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointLogin);
		
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		String responseHeader = response.getHeader("Set-Cookie");
		String responseBody = response.getBody().asString();
		responseCode = json.get("code").toString();
		responseDescription = json.get("description").toString();
	
		logger.info("Response Header : " + responseHeader);
		logger.info("Response Body : " + responseBody);
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		
		JSONObject tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		Assert.assertEquals(responseDescription, "Success");
		Assert.assertEquals(responseCode, "300");		
		Assert.assertEquals(tempData.get("balance").toString(), "0");
		logger.info("User Balance : " + tempData.get("balance"));
		Thread.sleep(5);
		logger.info("[ --- Finished TopUp_TSC_056 --- ]");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void TopUp_TSC_057() throws InterruptedException, ParseException{
//		'- User already registered
//		- User not logged in
//		- Current balance under Rp.2.000.000
//		- Correct amount to top up Rp.1.000.000
//		- Balance after top up is or under Rp.2.000.000
//		- Idempotency key is filled and unique
		logger.info("[ --- Started TopUp_TSC_057 --- ]");	
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		JSONObject RequestParams = new JSONObject();
		RequestParams.put("amount", 1000000);
		RequestParams.put("requestId", generateRequestId());
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		String responseCode = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp.1.000.000");
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION
		Assert.assertEquals(responseDescription, responseStatusUserNotLoggedIn);
		Assert.assertEquals(responseCode.toString(), "301");
		
		RequestParams.put("phone", userPhone);
		RequestParams.put("password", userPassword);
		
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointLogin);
		
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		String responseHeader = response.getHeader("Set-Cookie");
		String responseBody = response.getBody().asString();
		responseCode = json.get("code").toString();
		responseDescription = json.get("description").toString();
	
		logger.info("Response Header : " + responseHeader);
		logger.info("Response Body : " + responseBody);
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		
		JSONObject tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		Assert.assertEquals(responseDescription, "Success");
		Assert.assertEquals(responseCode, "300");		
		Assert.assertEquals(tempData.get("balance").toString(), "0");
		logger.info("User Balance : " + tempData.get("balance"));
		Thread.sleep(5);
		logger.info("[ --- Finished TopUp_TSC_057 --- ]");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void TopUp_TSC_058() throws InterruptedException, ParseException{
//		'- User already registered
//		- User not logged in
//		- Current balance under Rp.2.000.000
//		- Correct amount to top up Rp.1000
//		- Balance after top up is or under Rp.2.000.000
//		- Idempotency key is filled but not unique
		logger.info("[ --- Started TopUp_TSC_058 --- ]");	
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		JSONObject RequestParams = new JSONObject();
		RequestParams.put("amount", 1000);
		RequestParams.put("requestId", requestId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		String responseCode = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp.1000");
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION
		Assert.assertEquals(responseDescription, responseStatusUserNotLoggedIn);
		Assert.assertEquals(responseCode.toString(), "301");
		
		RequestParams.put("phone", userPhone);
		RequestParams.put("password", userPassword);
		
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointLogin);
		
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		String responseHeader = response.getHeader("Set-Cookie");
		String responseBody = response.getBody().asString();
		responseCode = json.get("code").toString();
		responseDescription = json.get("description").toString();
	
		logger.info("Response Header : " + responseHeader);
		logger.info("Response Body : " + responseBody);
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		
		JSONObject tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		Assert.assertEquals(responseDescription, "Success");
		Assert.assertEquals(responseCode, "300");		
		Assert.assertEquals(tempData.get("balance").toString(), "0");
		logger.info("User Balance : " + tempData.get("balance"));
		Thread.sleep(5);
		logger.info("[ --- Finished TopUp_TSC_058 --- ]");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void TopUp_TSC_059() throws InterruptedException, ParseException{
//		'- User already registered
//		- User not logged in
//		- Current balance under Rp.2.000.000
//		- Correct amount to top up Rp.100.000
//		- Balance after top up is or under Rp.2.000.000
//		- Idempotency key is filled but not unique
		logger.info("[ --- Started TopUp_TSC_059 --- ]");	
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		JSONObject RequestParams = new JSONObject();
		RequestParams.put("amount", 100000);
		RequestParams.put("requestId", requestId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		String responseCode = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp.100.000");
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION
		Assert.assertEquals(responseDescription, responseStatusUserNotLoggedIn);
		Assert.assertEquals(responseCode.toString(), "301");
		
		RequestParams.put("phone", userPhone);
		RequestParams.put("password", userPassword);
		
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointLogin);
		
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		String responseHeader = response.getHeader("Set-Cookie");
		String responseBody = response.getBody().asString();
		responseCode = json.get("code").toString();
		responseDescription = json.get("description").toString();
	
		logger.info("Response Header : " + responseHeader);
		logger.info("Response Body : " + responseBody);
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		
		JSONObject tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		Assert.assertEquals(responseDescription, "Success");
		Assert.assertEquals(responseCode, "300");		
		Assert.assertEquals(tempData.get("balance").toString(), "0");
		logger.info("User Balance : " + tempData.get("balance"));
		Thread.sleep(5);
		logger.info("[ --- Finished TopUp_TSC_059 --- ]");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void TopUp_TSC_060() throws InterruptedException, ParseException{
//		'- User already registered
//		- User not logged in
//		- Current balance under Rp.2.000.000
//		- Correct amount to top up Rp.1.000.000
//		- Balance after top up is or under Rp.2.000.000
//		- Idempotency key is filled but not unique
		logger.info("[ --- Started TopUp_TSC_060 --- ]");	
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		JSONObject RequestParams = new JSONObject();
		RequestParams.put("amount", 1000000);
		RequestParams.put("requestId", requestId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		String responseCode = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp.1.000.000");
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION
		Assert.assertEquals(responseDescription, responseStatusUserNotLoggedIn);
		Assert.assertEquals(responseCode.toString(), "301");
		
		RequestParams.put("phone", userPhone);
		RequestParams.put("password", userPassword);
		
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointLogin);
		
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		String responseHeader = response.getHeader("Set-Cookie");
		String responseBody = response.getBody().asString();
		responseCode = json.get("code").toString();
		responseDescription = json.get("description").toString();
	
		logger.info("Response Header : " + responseHeader);
		logger.info("Response Body : " + responseBody);
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		
		JSONObject tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		Assert.assertEquals(responseDescription, "Success");
		Assert.assertEquals(responseCode, "300");		
		Assert.assertEquals(tempData.get("balance").toString(), "0");
		logger.info("User Balance : " + tempData.get("balance"));
		Thread.sleep(5);
		logger.info("[ --- Finished TopUp_TSC_060 --- ]");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	@Parameters({"amountToTopUp"})
	void TopUp_TSC_061(Integer amount) throws InterruptedException, ParseException{
//		'- User already registered
//		- User not logged in
//		- Current balance under Rp.2.000.000
//		- Correct amount to top up Rp.1000
//		- Balance after top up is or under Rp.2.000.000
//		- Idempotency key is empty
		if(amount == 1000) {
			logger.info("[ --- Started TopUp_TSC_061 --- ]");	
		}else if(amount == 100000) {
			logger.info("[ --- Started TopUp_TSC_062 --- ]");
		}else if(amount == 1000000) {
			logger.info("[ --- Started TopUp_TSC_063 --- ]");
		}
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		JSONObject RequestParams = new JSONObject();
		RequestParams.put("amount", amount);
		RequestParams.put("requestId", null);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		String responseCode = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp.1000");
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION
		Assert.assertEquals(responseDescription, responseStatusUserNotLoggedIn);
		Assert.assertEquals(responseCode.toString(), "301");
		
		RequestParams.put("phone", userPhone);
		RequestParams.put("password", userPassword);
		
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointLogin);
		
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		String responseHeader = response.getHeader("Set-Cookie");
		String responseBody = response.getBody().asString();
		responseCode = json.get("code").toString();
		responseDescription = json.get("description").toString();
	
		logger.info("Response Header : " + responseHeader);
		logger.info("Response Body : " + responseBody);
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		
		JSONObject tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		Assert.assertEquals(responseDescription, "Success");
		Assert.assertEquals(responseCode, "300");		
		Assert.assertEquals(tempData.get("balance").toString(), "0");
		logger.info("User Balance : " + tempData.get("balance"));
		if(amount == 1000) {
			logger.info("[ --- Finished TopUp_TSC_061 --- ]");	
		}else if(amount == 100000) {
			logger.info("[ --- Finished TopUp_TSC_062 --- ]");
		}else if(amount == 1000000) {
			logger.info("[ --- Finished TopUp_TSC_063 --- ]");
		}
		Thread.sleep(5);
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void TopUp_TSC_064() throws InterruptedException, ParseException{
//		'- User already registered
//		- User not logged in
//		- Current balance under Rp.2.000.000
//		- Incorrect amount to top up Rp.999
//		- Balance after top up is or under Rp.2.000.000
//		- Idempotency key is filled and unique
		logger.info("[ --- Started TopUp_TSC_064 --- ]");	
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		JSONObject RequestParams = new JSONObject();
		RequestParams.put("amount", 999);
		RequestParams.put("requestId", generateRequestId());
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		String responseCode = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp.999");
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION
		Assert.assertEquals(responseDescription, responseStatusUserNotLoggedIn);
		Assert.assertEquals(responseCode.toString(), "301");
		
		RequestParams.put("phone", userPhone);
		RequestParams.put("password", userPassword);
		
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointLogin);
		
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		String responseHeader = response.getHeader("Set-Cookie");
		String responseBody = response.getBody().asString();
		responseCode = json.get("code").toString();
		responseDescription = json.get("description").toString();
	
		logger.info("Response Header : " + responseHeader);
		logger.info("Response Body : " + responseBody);
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		
		JSONObject tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		Assert.assertEquals(responseDescription, "Success");
		Assert.assertEquals(responseCode, "300");		
		Assert.assertEquals(tempData.get("balance").toString(), "0");
		logger.info("User Balance : " + tempData.get("balance"));
		Thread.sleep(5);
		logger.info("[ --- Finished TopUp_TSC_064 --- ]");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void TopUp_TSC_065() throws InterruptedException, ParseException{
//		'- User already registered
//		- User not logged in
//		- Current balance under Rp.2.000.000
//		- Incorrect amount to top up Rp.1.001.000
//		- Balance after top up is or under Rp.2.000.000
//		- Idempotency key is filled and unique
		logger.info("[ --- Started TopUp_TSC_065 --- ]");	
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		JSONObject RequestParams = new JSONObject();
		RequestParams.put("amount", 1001000);
		RequestParams.put("requestId", generateRequestId());
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		String responseCode = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp.1.001.000");
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION
		Assert.assertEquals(responseDescription, responseStatusUserNotLoggedIn);
		Assert.assertEquals(responseCode.toString(), "301");
		
		RequestParams.put("phone", userPhone);
		RequestParams.put("password", userPassword);
		
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointLogin);
		
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		String responseHeader = response.getHeader("Set-Cookie");
		String responseBody = response.getBody().asString();
		responseCode = json.get("code").toString();
		responseDescription = json.get("description").toString();
	
		logger.info("Response Header : " + responseHeader);
		logger.info("Response Body : " + responseBody);
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		
		JSONObject tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		Assert.assertEquals(responseDescription, "Success");
		Assert.assertEquals(responseCode, "300");		
		Assert.assertEquals(tempData.get("balance").toString(), "0");
		logger.info("User Balance : " + tempData.get("balance"));
		Thread.sleep(5);
		logger.info("[ --- Finished TopUp_TSC_065 --- ]");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void TopUp_TSC_066() throws InterruptedException, ParseException{
//		'- User already registered
//		- User not logged in
//		- Current balance under Rp.2.000.000
//		- Incorrect amount to top up Rp.999
//		- Balance after top up is or under Rp.2.000.000
//		- Idempotency key is filled but not unique
		logger.info("[ --- Started TopUp_TSC_066 --- ]");	
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		JSONObject RequestParams = new JSONObject();
		RequestParams.put("amount", 999);
		RequestParams.put("requestId", requestId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		String responseCode = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp.999");
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION
		Assert.assertEquals(responseDescription, responseStatusUserNotLoggedIn);
		Assert.assertEquals(responseCode.toString(), "301");
		
		RequestParams.put("phone", userPhone);
		RequestParams.put("password", userPassword);
		
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointLogin);
		
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		String responseHeader = response.getHeader("Set-Cookie");
		String responseBody = response.getBody().asString();
		responseCode = json.get("code").toString();
		responseDescription = json.get("description").toString();
	
		logger.info("Response Header : " + responseHeader);
		logger.info("Response Body : " + responseBody);
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		
		JSONObject tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		Assert.assertEquals(responseDescription, "Success");
		Assert.assertEquals(responseCode, "300");		
		Assert.assertEquals(tempData.get("balance").toString(), "0");
		logger.info("User Balance : " + tempData.get("balance"));
		Thread.sleep(5);
		logger.info("[ --- Finished TopUp_TSC_066 --- ]");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void TopUp_TSC_067() throws InterruptedException, ParseException{
//		'- User already registered
//		- User not logged in
//		- Current balance under Rp.2.000.000
//		- Incorrect amount to top up Rp.1.001.000
//		- Balance after top up is or under Rp.2.000.000
//		- Idempotency key is filled but not unique
		logger.info("[ --- Started TopUp_TSC_067 --- ]");	
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		JSONObject RequestParams = new JSONObject();
		RequestParams.put("amount", 1001000);
		RequestParams.put("requestId", requestId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		String responseCode = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp.1.001.000");
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION
		Assert.assertEquals(responseDescription, responseStatusUserNotLoggedIn);
		Assert.assertEquals(responseCode.toString(), "301");
		
		RequestParams.put("phone", userPhone);
		RequestParams.put("password", userPassword);
		
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointLogin);
		
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		String responseHeader = response.getHeader("Set-Cookie");
		String responseBody = response.getBody().asString();
		responseCode = json.get("code").toString();
		responseDescription = json.get("description").toString();
	
		logger.info("Response Header : " + responseHeader);
		logger.info("Response Body : " + responseBody);
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		
		JSONObject tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		Assert.assertEquals(responseDescription, "Success");
		Assert.assertEquals(responseCode, "300");		
		Assert.assertEquals(tempData.get("balance").toString(), "0");
		logger.info("User Balance : " + tempData.get("balance"));
		Thread.sleep(5);
		logger.info("[ --- Finished TopUp_TSC_067 --- ]");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	@Parameters({"amountToTopUp"})
	void TopUp_TSC_068(Integer amount) throws InterruptedException, ParseException{
//		'- User already registered
//		- User not logged in
//		- Current balance under Rp.2.000.000
//		- Incorrect amount to top up Rp.999
//		- Balance after top up is or under Rp.2.000.000
//		- Idempotency key is empty
		if(amount == 999) {
			logger.info("[ --- Started TopUp_TSC_068 --- ]");	
		}else if(amount == 1001000) {
			logger.info("[ --- Started TopUp_TSC_069 --- ]");	
		}
	
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		JSONObject RequestParams = new JSONObject();
		RequestParams.put("amount", amount);
		RequestParams.put("requestId", null);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		String responseCode = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp."+amount);
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION
		Assert.assertEquals(responseDescription, responseStatusUserNotLoggedIn);
		Assert.assertEquals(responseCode.toString(), "301");
		
		RequestParams.put("phone", userPhone);
		RequestParams.put("password", userPassword);
		
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointLogin);
		
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		String responseHeader = response.getHeader("Set-Cookie");
		String responseBody = response.getBody().asString();
		responseCode = json.get("code").toString();
		responseDescription = json.get("description").toString();
	
		logger.info("Response Header : " + responseHeader);
		logger.info("Response Body : " + responseBody);
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		
		JSONObject tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		Assert.assertEquals(responseDescription, "Success");
		Assert.assertEquals(responseCode, "300");		
		Assert.assertEquals(tempData.get("balance").toString(), "0");
		logger.info("User Balance : " + tempData.get("balance"));
		
		if(amount == 999) {
			logger.info("[ --- Finished TopUp_TSC_068 --- ]");	
		}else if(amount == 1001000) {
			logger.info("[ --- Finished TopUp_TSC_069 --- ]");	
		}
		Thread.sleep(5);
	}
	
	@SuppressWarnings("unchecked")
	@Test
	@Parameters({"amountToTopUp"})
	void TopUp_TSC_070(Integer amount) throws InterruptedException, ParseException{
//		'- User already registered
//		- User not logged in
//		- Current balance under Rp.2.000.000
//		- Incorrect amount to top up Rp.999
//		- Balance after top up over Rp.2.000.000
//		- Idempotency key is filled and unique
		if(amount == 999) {
			logger.info("[ --- Started TopUp_TSC_070 --- ]");
		}else if(amount == 1001000) {
			logger.info("[ --- Started TopUp_TSC_071 --- ]");
		}
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		JSONObject RequestParams = new JSONObject();
		RequestParams.put("amount", 1000000);
		RequestParams.put("requestId", generateRequestId());
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		String responseCode = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp.1.000.000");
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		
		JSONObject tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		
		// ASSERTION
		Assert.assertEquals(responseDescription, responseStatusSuccess);
		Assert.assertEquals(responseCode, "300");		
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("amount", 999999);
		RequestParams.put("requestId", generateRequestId());
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		responseCode = json.get("code").toString();
		responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp.999.999");
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		
		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		
		// ASSERTION
		Assert.assertEquals(responseDescription, responseStatusSuccess);
		Assert.assertEquals(responseCode, "300");		

		
		// === GET BALANCE BEFORE TOP UP ===
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		
		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceBeforeTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance before top up : " + balanceBeforeTopUp);
		
		

		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointLogout);
		
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		String responseHeader = response.getHeader("Set-Cookie");
		String responseBody = response.getBody().asString();
		logger.info("Response Body : " + responseBody);
		
		responseCode = json.get("code").toString();
		responseDescription = json.get("description").toString();
		
		logger.info("Response Header : " + responseHeader);
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		
		
		// ASSERTION
		Assert.assertEquals(responseDescription, "SUCCESS");
		Assert.assertEquals(responseCode.toString(), "300");
		
		if(responseCode.toString().equals("300")) {	
			logger.info("LOGOUT SUCCESS");
		}else {
			logger.info("LOGOUT FAILED");
		}	
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("amount", amount);
		RequestParams.put("requestId", generateRequestId());
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		responseCode = json.get("code").toString();
		responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp."+amount);
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		
		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		
		// ASSERTION
		Assert.assertEquals(responseDescription, responseStatusUserNotLoggedIn);
		Assert.assertEquals(responseCode, "301");
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		RequestParams = new JSONObject();		
		RequestParams.put("phone", userPhone);
		RequestParams.put("password", userPassword);
		
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointLogin);
		
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		responseHeader = response.getHeader("Set-Cookie");
		responseBody = response.getBody().asString();
		responseCode = json.get("code").toString();
		responseDescription = json.get("description").toString();
		
		String cookieId = between(responseHeader, "=", "; Path=/; Secure; HttpOnly");
		logger.info("Response Header : " + responseHeader);
		logger.info("Response Body : " + responseBody);
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);

		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		Assert.assertEquals(responseDescription, "Success");
		Assert.assertEquals(responseCode, "300");		
		Assert.assertEquals(tempData.get("phone").toString(), userPhone);
		Assert.assertEquals(tempData.get("balance").toString(), "1999999");
		if(responseCode.toString().equals("300")) {
			// Save session data to file sessionId.txt
			try {
                FileWriter generateSessionId = new FileWriter("assets/generated-data/sessionId.txt");
                generateSessionId.write(cookieId);
                generateSessionId.close();
            } catch (IOException e) {
                System.err.format("IOException: %s%n", e);
            }
			logger.info("=== LOGIN SUCCESS ===");
			logger.info("User ID : "+tempData.get("id".toString()));
			logger.info("Name : "+tempData.get("name".toString()));
			logger.info("Phone : "+tempData.get("phone".toString()));
			logger.info("email : "+tempData.get("email".toString()));
			logger.info("Balance : "+tempData.get("balance".toString()));
		}else {
			logger.info("=== LOGIN FAILED ===");
		}
		if(amount == 999) {
			logger.info("[ --- Finished TopUp_TSC_070 --- ]");
		}else if(amount == 1001000) {
			logger.info("[ --- Finished TopUp_TSC_071 --- ]");
		}
		Thread.sleep(5);
	}
	
	@SuppressWarnings("unchecked")
	@Test
	@Parameters({"amountToTopUp"})
	void TopUp_TSC_072(Integer amount) throws InterruptedException, ParseException{
//		'- User already registered
//		- User not logged in
//		- Current balance under Rp.2.000.000
//		- Incorrect amount to top up Rp.999
//		- Balance after top up over Rp.2.000.000
//		- Idempotency key is filled but not unique
		if(amount == 999) {
			logger.info("[ --- Started TopUp_TSC_072 --- ]");	
		}else if(amount == 1001000) {
			logger.info("[ --- Started TopUp_TSC_073 --- ]");
		}
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		JSONObject RequestParams = new JSONObject();
		RequestParams.put("amount", 1000000);
		RequestParams.put("requestId", generateRequestId());
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		String responseCode = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp.1.000.000");
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		
		JSONObject tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		
		// ASSERTION
		Assert.assertEquals(responseDescription, responseStatusSuccess);
		Assert.assertEquals(responseCode, "300");		
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("amount", 999999);
		RequestParams.put("requestId", generateRequestId());
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		responseCode = json.get("code").toString();
		responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp.999.999");
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		
		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		
		// ASSERTION
		Assert.assertEquals(responseDescription, responseStatusSuccess);
		Assert.assertEquals(responseCode, "300");		

		
		// === GET BALANCE BEFORE TOP UP ===
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		
		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceBeforeTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance before top up : " + balanceBeforeTopUp);
		
		

		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointLogout);
		
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		String responseHeader = response.getHeader("Set-Cookie");
		String responseBody = response.getBody().asString();
		logger.info("Response Body : " + responseBody);
		
		responseCode = json.get("code").toString();
		responseDescription = json.get("description").toString();
		
		logger.info("Response Header : " + responseHeader);
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		
		
		// ASSERTION
		Assert.assertEquals(responseDescription, "SUCCESS");
		Assert.assertEquals(responseCode.toString(), "300");
		
		if(responseCode.toString().equals("300")) {	
			logger.info("LOGOUT SUCCESS");
		}else {
			logger.info("LOGOUT FAILED");
		}	
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("amount", amount);
		RequestParams.put("requestId", requestId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		responseCode = json.get("code").toString();
		responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp."+amount);
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		
		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		
		// ASSERTION
		Assert.assertEquals(responseDescription, responseStatusUserNotLoggedIn);
		Assert.assertEquals(responseCode, "301");
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		RequestParams = new JSONObject();		
		RequestParams.put("phone", userPhone);
		RequestParams.put("password", userPassword);
		
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointLogin);
		
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		responseHeader = response.getHeader("Set-Cookie");
		responseBody = response.getBody().asString();
		responseCode = json.get("code").toString();
		responseDescription = json.get("description").toString();
		
		String cookieId = between(responseHeader, "=", "; Path=/; Secure; HttpOnly");
		logger.info("Response Header : " + responseHeader);
		logger.info("Response Body : " + responseBody);
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);

		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		Assert.assertEquals(responseDescription, "Success");
		Assert.assertEquals(responseCode, "300");		
		Assert.assertEquals(tempData.get("phone").toString(), userPhone);
		Assert.assertEquals(tempData.get("balance").toString(), "1999999");
		if(responseCode.toString().equals("300")) {
			// Save session data to file sessionId.txt
			try {
                FileWriter generateSessionId = new FileWriter("assets/generated-data/sessionId.txt");
                generateSessionId.write(cookieId);
                generateSessionId.close();
            } catch (IOException e) {
                System.err.format("IOException: %s%n", e);
            }
			logger.info("=== LOGIN SUCCESS ===");
			logger.info("User ID : "+tempData.get("id".toString()));
			logger.info("Name : "+tempData.get("name".toString()));
			logger.info("Phone : "+tempData.get("phone".toString()));
			logger.info("email : "+tempData.get("email".toString()));
			logger.info("Balance : "+tempData.get("balance".toString()));
		}else {
			logger.info("=== LOGIN FAILED ===");
		}
		
		if(amount == 999) {
			logger.info("[ --- Finished TopUp_TSC_072 --- ]");	
		}else if(amount == 1001000) {
			logger.info("[ --- Finished TopUp_TSC_073 --- ]");
		}
		
		Thread.sleep(5);
	}
	
	@SuppressWarnings("unchecked")
	@Test
	@Parameters({"amountToTopUp"})
	void TopUp_TSC_074(Integer amount) throws InterruptedException, ParseException{
//		'- User already registered
//		- User not logged in
//		- Current balance under Rp.2.000.000
//		- Incorrect amount to top up Rp.999
//		- Balance after top up over Rp.2.000.000
//		- Idempotency key is empty
		if(amount == 999) {
			logger.info("[ --- Started TopUp_TSC_074 --- ]");	
		}else if(amount == 1001000) {
			logger.info("[ --- Started TopUp_TSC_075 --- ]");
		}
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		JSONObject RequestParams = new JSONObject();
		RequestParams.put("amount", 1000000);
		RequestParams.put("requestId", generateRequestId());
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		String responseCode = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp.1.000.000");
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		
		JSONObject tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		
		// ASSERTION
		Assert.assertEquals(responseDescription, responseStatusSuccess);
		Assert.assertEquals(responseCode, "300");		
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("amount", 999999);
		RequestParams.put("requestId", generateRequestId());
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		responseCode = json.get("code").toString();
		responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp.999.999");
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		
		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		
		// ASSERTION
		Assert.assertEquals(responseDescription, responseStatusSuccess);
		Assert.assertEquals(responseCode, "300");		

		
		// === GET BALANCE BEFORE TOP UP ===
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		
		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceBeforeTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance before top up : " + balanceBeforeTopUp);
		
		

		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointLogout);
		
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		String responseHeader = response.getHeader("Set-Cookie");
		String responseBody = response.getBody().asString();
		logger.info("Response Body : " + responseBody);
		
		responseCode = json.get("code").toString();
		responseDescription = json.get("description").toString();
		
		logger.info("Response Header : " + responseHeader);
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		
		
		// ASSERTION
		Assert.assertEquals(responseDescription, "SUCCESS");
		Assert.assertEquals(responseCode.toString(), "300");
		
		if(responseCode.toString().equals("300")) {	
			logger.info("LOGOUT SUCCESS");
		}else {
			logger.info("LOGOUT FAILED");
		}	
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("amount", amount);
		RequestParams.put("requestId", null);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		responseCode = json.get("code").toString();
		responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp."+amount);
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		
		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		
		// ASSERTION
		Assert.assertEquals(responseDescription, responseStatusUserNotLoggedIn);
		Assert.assertEquals(responseCode, "301");
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		RequestParams = new JSONObject();		
		RequestParams.put("phone", userPhone);
		RequestParams.put("password", userPassword);
		
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointLogin);
		
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		responseHeader = response.getHeader("Set-Cookie");
		responseBody = response.getBody().asString();
		responseCode = json.get("code").toString();
		responseDescription = json.get("description").toString();
		
		String cookieId = between(responseHeader, "=", "; Path=/; Secure; HttpOnly");
		logger.info("Response Header : " + responseHeader);
		logger.info("Response Body : " + responseBody);
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);

		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		Assert.assertEquals(responseDescription, "Success");
		Assert.assertEquals(responseCode, "300");		
		Assert.assertEquals(tempData.get("phone").toString(), userPhone);
		Assert.assertEquals(tempData.get("balance").toString(), "1999999");
		if(responseCode.toString().equals("300")) {
			// Save session data to file sessionId.txt
			try {
                FileWriter generateSessionId = new FileWriter("assets/generated-data/sessionId.txt");
                generateSessionId.write(cookieId);
                generateSessionId.close();
            } catch (IOException e) {
                System.err.format("IOException: %s%n", e);
            }
			logger.info("=== LOGIN SUCCESS ===");
			logger.info("User ID : "+tempData.get("id".toString()));
			logger.info("Name : "+tempData.get("name".toString()));
			logger.info("Phone : "+tempData.get("phone".toString()));
			logger.info("email : "+tempData.get("email".toString()));
			logger.info("Balance : "+tempData.get("balance".toString()));
		}else {
			logger.info("=== LOGIN FAILED ===");
		}
		
		if(amount == 999) {
			logger.info("[ --- Finished TopUp_TSC_074 --- ]");	
		}else if(amount == 1001000) {
			logger.info("[ --- Finished TopUp_TSC_075 --- ]");
		}
		
		Thread.sleep(5);
	}
	
	@SuppressWarnings("unchecked")
	@Test
	@Parameters({"amountToTopUp"})
	void TopUp_TSC_076(Integer amount) throws InterruptedException, ParseException{
//		'- User already registered
//		- User not logged in
//		- Current balance is Rp.2.000.000
//		- Correct amount to top up Rp.1000
//		- Balance after top up over Rp.2.000.000
//		- Idempotency key is filled and unique
		logger.info("[ --- Started TopUp_TSC_076 --- ]");	
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		JSONObject RequestParams = new JSONObject();
		RequestParams.put("amount", 1000000);
		RequestParams.put("requestId", generateRequestId());
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		String responseCode = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp.1.000.000");
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		
		JSONObject tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		
		// ASSERTION
		Assert.assertEquals(responseDescription, responseStatusSuccess);
		Assert.assertEquals(responseCode, "300");		
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("amount", 1000000);
		RequestParams.put("requestId", generateRequestId());
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		responseCode = json.get("code").toString();
		responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp.1.000.000");
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		
		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		
		// ASSERTION
		Assert.assertEquals(responseDescription, responseStatusSuccess);
		Assert.assertEquals(responseCode, "300");		

		
		// === GET BALANCE BEFORE TOP UP ===
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		
		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceBeforeTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance before top up : " + balanceBeforeTopUp);
		
		

		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointLogout);
		
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		String responseHeader = response.getHeader("Set-Cookie");
		String responseBody = response.getBody().asString();
		logger.info("Response Body : " + responseBody);
		
		responseCode = json.get("code").toString();
		responseDescription = json.get("description").toString();
		
		logger.info("Response Header : " + responseHeader);
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		
		
		// ASSERTION
		Assert.assertEquals(responseDescription, "SUCCESS");
		Assert.assertEquals(responseCode.toString(), "300");
		
		if(responseCode.toString().equals("300")) {	
			logger.info("LOGOUT SUCCESS");
		}else {
			logger.info("LOGOUT FAILED");
		}	
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("amount", amount);
		RequestParams.put("requestId", generateRequestId());
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		responseCode = json.get("code").toString();
		responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp."+amount);
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		
		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		
		// ASSERTION
		Assert.assertEquals(responseDescription, responseStatusUserNotLoggedIn);
		Assert.assertEquals(responseCode, "301");
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		RequestParams = new JSONObject();		
		RequestParams.put("phone", userPhone);
		RequestParams.put("password", userPassword);
		
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointLogin);
		
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		responseHeader = response.getHeader("Set-Cookie");
		responseBody = response.getBody().asString();
		responseCode = json.get("code").toString();
		responseDescription = json.get("description").toString();
		
		String cookieId = between(responseHeader, "=", "; Path=/; Secure; HttpOnly");
		logger.info("Response Header : " + responseHeader);
		logger.info("Response Body : " + responseBody);
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);

		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		Assert.assertEquals(responseDescription, "Success");
		Assert.assertEquals(responseCode, "300");		
		Assert.assertEquals(tempData.get("phone").toString(), userPhone);
		Assert.assertEquals(tempData.get("balance").toString(), "2000000");
		if(responseCode.toString().equals("300")) {
			// Save session data to file sessionId.txt
			try {
                FileWriter generateSessionId = new FileWriter("assets/generated-data/sessionId.txt");
                generateSessionId.write(cookieId);
                generateSessionId.close();
            } catch (IOException e) {
                System.err.format("IOException: %s%n", e);
            }
			logger.info("=== LOGIN SUCCESS ===");
			logger.info("User ID : "+tempData.get("id".toString()));
			logger.info("Name : "+tempData.get("name".toString()));
			logger.info("Phone : "+tempData.get("phone".toString()));
			logger.info("email : "+tempData.get("email".toString()));
			logger.info("Balance : "+tempData.get("balance".toString()));
		}else {
			logger.info("=== LOGIN FAILED ===");
		}
		Thread.sleep(5);
		logger.info("[ --- Finished TopUp_TSC_076 --- ]");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	@Parameters({"amountToTopUp"})
	void TopUp_TSC_079(Integer amount) throws InterruptedException, ParseException{
//		'- User already registered
//		- User not logged in
//		- Current balance is Rp.2.000.000
//		- Correct amount to top up Rp.1000
//		- Balance after top up over Rp.2.000.000
//		- Idempotency key is filled but not unique
		logger.info("[ --- Started TopUp_TSC_079 --- ]");	
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		JSONObject RequestParams = new JSONObject();
		RequestParams.put("amount", 1000000);
		RequestParams.put("requestId", generateRequestId());
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		String responseCode = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp.1.000.000");
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		
		JSONObject tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		
		// ASSERTION
		Assert.assertEquals(responseDescription, responseStatusSuccess);
		Assert.assertEquals(responseCode, "300");		
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("amount", 1000000);
		RequestParams.put("requestId", generateRequestId());
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		responseCode = json.get("code").toString();
		responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp.1.000.000");
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		
		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		
		// ASSERTION
		Assert.assertEquals(responseDescription, responseStatusSuccess);
		Assert.assertEquals(responseCode, "300");		

		
		// === GET BALANCE BEFORE TOP UP ===
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		
		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceBeforeTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance before top up : " + balanceBeforeTopUp);
		
		

		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointLogout);
		
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		String responseHeader = response.getHeader("Set-Cookie");
		String responseBody = response.getBody().asString();
		logger.info("Response Body : " + responseBody);
		
		responseCode = json.get("code").toString();
		responseDescription = json.get("description").toString();
		
		logger.info("Response Header : " + responseHeader);
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		
		
		// ASSERTION
		Assert.assertEquals(responseDescription, "SUCCESS");
		Assert.assertEquals(responseCode.toString(), "300");
		
		if(responseCode.toString().equals("300")) {	
			logger.info("LOGOUT SUCCESS");
		}else {
			logger.info("LOGOUT FAILED");
		}	
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("amount", amount);
		RequestParams.put("requestId", requestId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		responseCode = json.get("code").toString();
		responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp."+amount);
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		
		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		
		// ASSERTION
		Assert.assertEquals(responseDescription, responseStatusUserNotLoggedIn);
		Assert.assertEquals(responseCode, "301");
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		RequestParams = new JSONObject();		
		RequestParams.put("phone", userPhone);
		RequestParams.put("password", userPassword);
		
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointLogin);
		
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		responseHeader = response.getHeader("Set-Cookie");
		responseBody = response.getBody().asString();
		responseCode = json.get("code").toString();
		responseDescription = json.get("description").toString();
		
		String cookieId = between(responseHeader, "=", "; Path=/; Secure; HttpOnly");
		logger.info("Response Header : " + responseHeader);
		logger.info("Response Body : " + responseBody);
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);

		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		Assert.assertEquals(responseDescription, "Success");
		Assert.assertEquals(responseCode, "300");		
		Assert.assertEquals(tempData.get("phone").toString(), userPhone);
		Assert.assertEquals(tempData.get("balance").toString(), "2000000");
		if(responseCode.toString().equals("300")) {
			// Save session data to file sessionId.txt
			try {
                FileWriter generateSessionId = new FileWriter("assets/generated-data/sessionId.txt");
                generateSessionId.write(cookieId);
                generateSessionId.close();
            } catch (IOException e) {
                System.err.format("IOException: %s%n", e);
            }
			logger.info("=== LOGIN SUCCESS ===");
			logger.info("User ID : "+tempData.get("id".toString()));
			logger.info("Name : "+tempData.get("name".toString()));
			logger.info("Phone : "+tempData.get("phone".toString()));
			logger.info("email : "+tempData.get("email".toString()));
			logger.info("Balance : "+tempData.get("balance".toString()));
		}else {
			logger.info("=== LOGIN FAILED ===");
		}
		Thread.sleep(5);
		logger.info("[ --- Finished TopUp_TSC_079 --- ]");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	@Parameters({"amountToTopUp"})
	void TopUp_TSC_082(Integer amount) throws InterruptedException, ParseException{
//		'- User already registered
//		- User not logged in
//		- Current balance is Rp.2.000.000
//		- Correct amount to top up Rp.1000
//		- Balance after top up over Rp.2.000.000
//		- Idempotency key is empty
		if(amount == 1000) {
			logger.info("[ --- Started TopUp_TSC_082 --- ]");	
		}else if(amount == 100000) {
			logger.info("[ --- Started TopUp_TSC_083 --- ]");	
		}else if(amount == 1000000) {
			logger.info("[ --- Started TopUp_TSC_084 --- ]");	
		}else if(amount == 999) {
			logger.info("[ --- Started TopUp_TSC_089 --- ]");
		}else if(amount == 1001000) {
			logger.info("[ --- Started TopUp_TSC_090 --- ]");
		}
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		JSONObject RequestParams = new JSONObject();
		RequestParams.put("amount", 1000000);
		RequestParams.put("requestId", generateRequestId());
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		String responseCode = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp.1.000.000");
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		
		JSONObject tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		
		// ASSERTION
		Assert.assertEquals(responseDescription, responseStatusSuccess);
		Assert.assertEquals(responseCode, "300");		
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("amount", 1000000);
		RequestParams.put("requestId", generateRequestId());
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		responseCode = json.get("code").toString();
		responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp.1.000.000");
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		
		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		
		// ASSERTION
		Assert.assertEquals(responseDescription, responseStatusSuccess);
		Assert.assertEquals(responseCode, "300");		

		
		// === GET BALANCE BEFORE TOP UP ===
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.GET, endpointViewProfile);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		
		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		balanceBeforeTopUp = Integer.parseInt(tempData.get("balance").toString());
		logger.info("Balance before top up : " + balanceBeforeTopUp);
		
		

		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		RequestParams = new JSONObject();
		RequestParams.put("userId", userId);
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointLogout);
		
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		String responseHeader = response.getHeader("Set-Cookie");
		String responseBody = response.getBody().asString();
		logger.info("Response Body : " + responseBody);
		
		responseCode = json.get("code").toString();
		responseDescription = json.get("description").toString();
		
		logger.info("Response Header : " + responseHeader);
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		
		
		// ASSERTION
		Assert.assertEquals(responseDescription, "SUCCESS");
		Assert.assertEquals(responseCode.toString(), "300");
		
		if(responseCode.toString().equals("300")) {	
			logger.info("LOGOUT SUCCESS");
		}else {
			logger.info("LOGOUT FAILED");
		}	
		
		// TOP UP PROCESS
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		// PROVIDE JSON DATA AT REQUEST BODY
		RequestParams = new JSONObject();
		RequestParams.put("amount", amount);
		RequestParams.put("requestId", null);
		
		// SET COOKIE, REQUEST HEADER AND REQUEST BODY
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		
		// SET REQUEST METHOD AND API ENDPOINT
		response = httpRequest.request(Method.POST, endpointTopUp);
		
		// MANIPULATING RESPONSE DATA
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		responseCode = json.get("code").toString();
		responseDescription = json.get("description").toString();
		
		logger.info("Top Up : Rp."+amount);
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		
		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		
		// ASSERTION
		Assert.assertEquals(responseDescription, responseStatusUserNotLoggedIn);
		Assert.assertEquals(responseCode, "301");
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		RequestParams = new JSONObject();		
		RequestParams.put("phone", userPhone);
		RequestParams.put("password", userPassword);
		
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointLogin);
		
		parser = new JSONParser();
		json = (JSONObject) parser.parse(response.getBody().asString());
		responseHeader = response.getHeader("Set-Cookie");
		responseBody = response.getBody().asString();
		responseCode = json.get("code").toString();
		responseDescription = json.get("description").toString();
		
		String cookieId = between(responseHeader, "=", "; Path=/; Secure; HttpOnly");
		logger.info("Response Header : " + responseHeader);
		logger.info("Response Body : " + responseBody);
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);

		tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		Assert.assertEquals(responseDescription, "Success");
		Assert.assertEquals(responseCode, "300");		
		Assert.assertEquals(tempData.get("phone").toString(), userPhone);
		Assert.assertEquals(tempData.get("balance").toString(), "2000000");
		if(responseCode.toString().equals("300")) {
			// Save session data to file sessionId.txt
			try {
                FileWriter generateSessionId = new FileWriter("assets/generated-data/sessionId.txt");
                generateSessionId.write(cookieId);
                generateSessionId.close();
            } catch (IOException e) {
                System.err.format("IOException: %s%n", e);
            }
			logger.info("=== LOGIN SUCCESS ===");
			logger.info("User ID : "+tempData.get("id".toString()));
			logger.info("Name : "+tempData.get("name".toString()));
			logger.info("Phone : "+tempData.get("phone".toString()));
			logger.info("email : "+tempData.get("email".toString()));
			logger.info("Balance : "+tempData.get("balance".toString()));
		}else {
			logger.info("=== LOGIN FAILED ===");
		}
		if(amount == 1000) {
			logger.info("[ --- Finished TopUp_TSC_082 --- ]");	
		}else if(amount == 100000) {
			logger.info("[ --- Finished TopUp_TSC_083 --- ]");	
		}else if(amount == 1000000) {
			logger.info("[ --- Finished TopUp_TSC_084 --- ]");	
		}else if(amount == 999) {
			logger.info("[ --- Finished TopUp_TSC_089 --- ]");
		}else if(amount == 1001000) {
			logger.info("[ --- Finished TopUp_TSC_090 --- ]");
		}
		
		Thread.sleep(5);
	}
	
	@AfterClass
	void tearDown() {
		logger.info("[ --- Finished TestSuite_TopUp --- ]");
		System.out.println();
	}
}