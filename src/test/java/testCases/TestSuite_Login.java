package testCases;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import io.restassured.RestAssured;
import io.restassured.http.Method;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import testBase.TestBase;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class TestSuite_Login extends TestBase {
	final static Logger logger = Logger.getLogger(TestSuite_Login.class);
	String baseUrl = "";
	String responseStatusSuccess ="";
	String responseStatusFailed = "";
	String endpointLogin = "/member/user/login";
	File getUserName, getUserPhone, getUserPassword, getUserEmail, getUserBalance, getSessionId;
	Scanner userNameReader, userPhoneReader, userPasswordReader, userEmailReader, userBalanceReader, sessionIdReader;
	String userName = "";
	String userPhone = "";
	String userPassword = "";
	String userEmail = "";
	Integer userBalance = 0;
	String sessionId = "";

	@BeforeClass
	@Parameters({"baseUrl", "responseStatusSuccess", "responseStatusFailed"})
	void startTest(String definedBaseUrl, @Optional("Optional Parameter")  String definedResponseStatusSuccess, String definedResponseStatusFailed) {
		baseUrl = definedBaseUrl;
		responseStatusSuccess = definedResponseStatusSuccess;
		responseStatusFailed = definedResponseStatusFailed;
		logger.info("[ --- Started TestSuite_Login --- ]");

		try {
			getUserName = new File("./assets/generated-data/userName.txt");
			getUserPhone = new File("./assets/generated-data/userPhone.txt");
			getUserPassword = new File("./assets/generated-data/userPassword.txt");
			getUserEmail = new File("./assets/generated-data/userEmail.txt");
			getUserBalance = new File("./assets/generated-data/userBalance.txt");

			userNameReader = new Scanner(getUserName);
			userPhoneReader = new Scanner(getUserPhone);
			userPasswordReader = new Scanner(getUserPassword);
			userEmailReader = new Scanner(getUserEmail);
			userBalanceReader = new Scanner(getUserBalance);

			while (userNameReader.hasNextLine()) {
				String data = userNameReader.nextLine();
				userName = data;
			}

			while (userPhoneReader.hasNextLine()) {
				String data = userPhoneReader.nextLine();
				userPhone = data;
			}

			while (userPasswordReader.hasNextLine()) {
				String data = userPasswordReader.nextLine();
				userPassword = data;
			}

			while (userEmailReader.hasNextLine()) {
				String data = userEmailReader.nextLine();
				userEmail = data;
			}

			while (userBalanceReader.hasNextLine()) {
				String data = userBalanceReader.nextLine();
				userBalance = Integer.parseInt(data);
			}


		}catch (FileNotFoundException e) {
			System.out.println("An error occurred.");
		      e.printStackTrace();
		}
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void Login_TSC_001() throws InterruptedException, ParseException{
//		Phone filled
//		Password Filled
//		Phone and Password match
		
		logger.info("[ --- Started Login_TSC_001 --- ]");	
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		RequestParams.put("phone", userPhone);
		RequestParams.put("password", userPassword);
		
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointLogin);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		String responseHeader = response.getHeader("Set-Cookie");
		String responseBody = response.getBody().asString();
		String responseCode = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		String cookieId = between(responseHeader, "=", "; Path=/; Secure; HttpOnly");
		logger.info("Response Header : " + responseHeader);
		logger.info("Response Body : " + responseBody);
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);

		JSONObject tempData = new JSONObject();
		tempData = (JSONObject) json.get("userData");
		Assert.assertEquals(responseDescription, "Success");
		Assert.assertEquals(responseCode, "300");
		Assert.assertEquals(tempData.get("name").toString(), userName);
		Assert.assertEquals(tempData.get("phone").toString(), userPhone);
		Assert.assertEquals(tempData.get("email").toString(), userEmail);
		if(responseCode.toString().equals("300")) {
			// Save session data to file sessionId.txt
			try {
                FileWriter generateSessionId = new FileWriter("assets/generated-data/sessionId.txt");
                generateSessionId.write(cookieId);
                generateSessionId.close();
            } catch (IOException e) {
                System.err.format("IOException: %s%n", e);
            }
			logger.info("=== LOGIN SUCCESS ===");
			logger.info("User ID : "+tempData.get("id".toString()));
			logger.info("Name : "+tempData.get("name".toString()));
			logger.info("Phone : "+tempData.get("phone".toString()));
			logger.info("email : "+tempData.get("email".toString()));
			logger.info("Balance : "+tempData.get("balance".toString()));
		}else {
			logger.info("=== LOGIN FAILED ===");
		}
		
		Thread.sleep(5);
		logger.info("[ --- Finished Login_TSC_001 --- ]");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void Login_TSC_002() throws InterruptedException, ParseException{
//		Phone filled
//		Password Filled
//		Phone and Password not match
		
		logger.info("[ --- Started Login_TSC_002 --- ]");
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		RequestParams.put("phone", userPhone);
		RequestParams.put("password", "Password");
		
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointLogin);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		String responseHeader = response.getHeader("Set-Cookie");
		String responseBody = response.getBody().asString();
		String responseCode = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Response Header : " + responseHeader);
		logger.info("Response Body : " + responseBody);
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);

		Assert.assertEquals(responseDescription, "User not found");
		Assert.assertEquals(responseCode, "201");
		
		Thread.sleep(5);
		logger.info("[ --- Finished Login_TSC_002 --- ]");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void Login_TSC_003() throws InterruptedException, ParseException{
//		Phone filled
//		Password Filled
//		Phone and Password match
		
		logger.info("[ --- Started Login_TSC_003 --- ]");

		try {
			getSessionId = new File("./assets/generated-data/sessionId.txt");
			sessionIdReader = new Scanner(getSessionId);
			while (sessionIdReader.hasNextLine()) {
				String data = sessionIdReader.nextLine();
				sessionId = data;
			}
		}catch (FileNotFoundException e) {
			System.out.println("An error occurred.");
		      e.printStackTrace();
		}
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		RequestParams.put("phone", userPhone);
		RequestParams.put("password", userPassword);
		
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointLogin);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		String responseBody = response.getBody().asString();
		String responseCode = json.get("status").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Response Body : " + responseBody);
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);

		Assert.assertEquals(responseDescription, "DEVICE_ALREADY_LOGGED_IN");
		Assert.assertEquals(responseCode, "302");
		
		Thread.sleep(5);
		logger.info("[ --- Finished Login_TSC_003 --- ]");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void Login_TSC_004() throws InterruptedException, ParseException{
//		Phone filled
//		Password Filled
//		Phone and Password match
		
		logger.info("[ --- Started Login_TSC_004 --- ]");

		try {
			getSessionId = new File("./assets/generated-data/sessionId.txt");
			sessionIdReader = new Scanner(getSessionId);
			while (sessionIdReader.hasNextLine()) {
				String data = sessionIdReader.nextLine();
				sessionId = data;
			}
		}catch (FileNotFoundException e) {
			System.out.println("An error occurred.");
		      e.printStackTrace();
		}
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		RequestParams.put("phone", userPhone);
		RequestParams.put("password", "Password");
		
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointLogin);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		String responseBody = response.getBody().asString();
		String responseCode = json.get("status").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Response Body : " + responseBody);
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);

		Assert.assertEquals(responseDescription, "DEVICE_ALREADY_LOGGED_IN");
		Assert.assertEquals(responseCode, "302");
		
		Thread.sleep(5);
		logger.info("[ --- Finished Login_TSC_004 --- ]");
	}
	
	@AfterClass
	void tearDown() {
		logger.info("[ --- Finished TestSuite_Login --- ]");
		System.out.println();
	}
}