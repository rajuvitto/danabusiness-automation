package testCases;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import testBase.TestBase;

public class TestSuite_RequestPayment extends TestBase{
	final static Logger logger = Logger.getLogger(TestSuite_RequestPayment.class);
	String baseUrl = "";
	String responseStatusSuccess ="";
	String responseStatusFailed = "";
	String endpointRequestPayment = "/payment/pay/request";
	String endpointViewProfile = "/member/user/viewProfile";
	long userId;
	String sessionId = "";
	File getUserId, getSessionId, getRequestId;
	Scanner userIdReader, sessionIdReader, requestIdReader;
	String requestId="";
	String requestIdtxt="";
	String transactionHistoryId="";
	Boolean status = true;
	
	@BeforeClass
	@Parameters({"baseUrl", "responseStatusSuccess", "responseStatusFailed"})
	void startTest(String definedBaseUrl, @Optional("Optional Parameter")  String definedResponseStatusSuccess, String definedResponseStatusFailed){
		baseUrl = definedBaseUrl;
		responseStatusSuccess = definedResponseStatusSuccess;
		responseStatusFailed = definedResponseStatusFailed;
		logger.info("[ --- Started TestSuite_RequestPayment --- ]");
		
		try {
			getUserId = new File("./assets/generated-data/userId.txt");
			getSessionId = new File("./assets/generated-data/sessionId.txt");
			getRequestId = new File("./assets/generated-data/requestId.txt");
			userIdReader = new Scanner(getUserId);
			sessionIdReader = new Scanner(getSessionId);
			requestIdReader = new Scanner(getRequestId);
			
			while (userIdReader.hasNextLine()) {
				String data = userIdReader.nextLine();
				userId = Long.parseLong(data);
			}
			
			while (sessionIdReader.hasNextLine()) {
				String data = sessionIdReader.nextLine();
				sessionId = data;
			}
			
			while (requestIdReader.hasNextLine()) {
				String data = requestIdReader.nextLine();
				requestIdtxt = data;
			}
		}catch (FileNotFoundException e) {
			System.out.println("An error occurred.");
		      e.printStackTrace();
		}
	}
	
	@SuppressWarnings("unchecked")
	@Parameters({"testCaseId"})
	@Test
	void RequestPayment(String testId)throws InterruptedException, ParseException{
//		Merchant exist
//		Total payment valid
//		request Id is filled and unique
		logger.info("[ --- Started RequestPayment --- ]");	
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		if((testId.equals("004"))||(testId.equals("013"))||(testId.equals("022"))||(testId.equals("031"))||(testId.equals("040"))||(testId.equals("049"))||(testId.equals("058"))||(testId.equals("067"))){
			RequestParams.put("merchantId", 1);
			RequestParams.put("merchantName", "Fremilt");
			RequestParams.put("totalPayment", 25000);
			RequestParams.put("requestId", generateRequestId());
		}else if((testId.equals("005"))||(testId.equals("014"))||(testId.equals("023"))||(testId.equals("032"))||(testId.equals("041"))||(testId.equals("050"))||(testId.equals("059"))||(testId.equals("068"))){
			RequestParams.put("merchantId", 1);
			RequestParams.put("merchantName", "Fremilt");
			RequestParams.put("totalPayment", 1000);
			RequestParams.put("requestId", generateRequestId());
		}else {
			RequestParams.put("merchantId", 1);
			RequestParams.put("merchantName", "Fremilt");
			RequestParams.put("totalPayment", 999);
			RequestParams.put("requestId", generateRequestId());
		}
		
		
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointRequestPayment);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		JSONObject tempData = new JSONObject();
		tempData = (JSONObject) json.get("transactionHistory");
		
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		String responseisCompleted = tempData.get("isCompleted").toString();
		transactionHistoryId = tempData.get("id").toString();
		
		logger.info("Response Body : " + responseBody);
		logger.info("Response Status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		logger.info("Response isCompleted : " + responseisCompleted);
		
		// ASSERTION
		Assert.assertEquals(responseDescription, responseStatusSuccess);
		Assert.assertEquals(responseStatus, "300");
		Assert.assertEquals(responseisCompleted, "false");
		
		if(responseStatus.toString().equals("300")) {
			// Save transaction history id to file transactionHistoryId.txt
			try {
				FileWriter generateTransactionHistoryId = new FileWriter("./assets/generated-data/transactionHistoryId2.txt");
				generateTransactionHistoryId.write(tempData.get("id").toString());
				generateTransactionHistoryId.close();
			} catch (IOException e) {
			    System.err.format("IOException: %s%n", e);
			}
		}
		
		Thread.sleep(5);
		logger.info("[ --- Finished RequestPayment --- ]");
		
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void RequestPayment_TSC_001()throws InterruptedException, ParseException{
//		User already registered
//		User Logged in
//		Merchant exist
//		Total payment valid for success transaction
//		request Id is filled and unique
		logger.info("[ --- Started RequestPayment_TSC_001 --- ]");	
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("merchantId", 1);
		RequestParams.put("merchantName", "Fremilt");
		RequestParams.put("totalPayment", 25000);
		RequestParams.put("requestId", requestId=generateRequestId());
		
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointRequestPayment);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		JSONObject tempData = new JSONObject();
		tempData = (JSONObject) json.get("transactionHistory");
		
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		String responseisCompleted = tempData.get("isCompleted").toString();
		transactionHistoryId = tempData.get("id").toString();
		
		logger.info("Response Body : " + responseBody);
		logger.info("Response Status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		logger.info("Response isCompleted : " + responseisCompleted);
		
		// ASSERTION
		Assert.assertEquals(responseDescription, responseStatusSuccess);
		Assert.assertEquals(responseStatus, "300");
		Assert.assertEquals(responseisCompleted, "false");
		
		if(responseStatus.toString().equals("300")) {
			// Save transaction history id to file transactionHistoryId.txt
			try {
				FileWriter generateTransactionHistoryId = new FileWriter("./assets/generated-data/transactionHistoryId.txt");
				FileWriter generateRequestId = new FileWriter("./assets/generated-data/requestId.txt");
				generateTransactionHistoryId.write(tempData.get("id").toString());
				generateRequestId.write(requestId);
				generateTransactionHistoryId.close();
				generateRequestId.close();
			} catch (IOException e) {
			    System.err.format("IOException: %s%n", e);
			}
		}
		
		Thread.sleep(5);
		logger.info("[ --- Finished RequestPayment_TSC_001 --- ]");
		
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void RequestPayment_TSC_002()throws InterruptedException, ParseException{
//		User already registered
//		User Logged in
//		Merchant exist
//		Total payment valid for refund transaction
//		request Id is filled and unique
		logger.info("[ --- Started RequestPayment_TSC_002 --- ]");	
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("merchantId", 1);
		RequestParams.put("merchantName", "Fremilt");
		RequestParams.put("totalPayment", 1000);
		RequestParams.put("requestId", generateRequestId());
		
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointRequestPayment);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		JSONObject tempData = new JSONObject();
		tempData = (JSONObject) json.get("transactionHistory");
		
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		String responseisCompleted = tempData.get("isCompleted").toString();
		
		logger.info("Response body : " + responseBody);
		logger.info("Response status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		logger.info("Response isCompleted : " + responseisCompleted);
		
		// ASSERTION
		Assert.assertEquals(responseDescription, responseStatusSuccess);
		Assert.assertEquals(responseStatus, "300");
		Assert.assertEquals(responseisCompleted, "false");
		
		if(responseStatus.toString().equals("300")) {
			// Save session id to file transactionHistoryId.txt
			try {
				FileWriter generateTransactionHistoryId = new FileWriter("./assets/generated-data/transactionHistoryId.txt");
				generateTransactionHistoryId.write(tempData.get("id").toString());
				generateTransactionHistoryId.close();
			} catch (IOException e) {
			    System.err.format("IOException: %s%n", e);
			}
		}
		
		Thread.sleep(5);
		logger.info("[ --- Finished RequestPayment_TSC_002 --- ]");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void RequestPayment_TSC_003()throws InterruptedException, ParseException{
//		User already registered
//		User Logged in
//		Merchant exist
//		Total payment valid for failed transaction
//		request Id is filled and unique
		logger.info("[ --- Started RequestPayment_TSC_003 --- ]");	
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("merchantId", 1);
		RequestParams.put("merchantName", "Fremilt");
		RequestParams.put("totalPayment", 999);
		RequestParams.put("requestId", generateRequestId());
		
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointRequestPayment);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		JSONObject tempData = new JSONObject();
		tempData = (JSONObject) json.get("transactionHistory");
		
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		String responseisCompleted = tempData.get("isCompleted").toString();
		
		logger.info("Response body : " + responseBody);
		logger.info("Response status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		logger.info("Response isCompleted : " + responseisCompleted);
		
		// ASSERTION
		Assert.assertEquals(responseDescription, responseStatusSuccess);
		Assert.assertEquals(responseStatus, "300");
		Assert.assertEquals(responseisCompleted, "false");
		
		if(responseStatus.toString().equals("300")) {
			// Save session id to file transactionHistoryId.txt
			try {
				FileWriter generateTransactionHistoryId = new FileWriter("./assets/generated-data/transactionHistoryId.txt");
				generateTransactionHistoryId.write(tempData.get("id").toString());
				generateTransactionHistoryId.close();
			} catch (IOException e) {
			    System.err.format("IOException: %s%n", e);
			}
		}
		
		Thread.sleep(5);
		logger.info("[ --- Finished RequestPayment_TSC_003 --- ]");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void RequestPayment_TSC_004()throws InterruptedException, ParseException{
//		User already registered
//		User Logged in
//		Merchant exist
//		Total payment valid for success transaction
//		request Id is filled but not unique
		logger.info("[ --- Started RequestPayment_TSC_004 --- ]");	
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("merchantId", 1);
		RequestParams.put("merchantName", "Fremilt");
		RequestParams.put("totalPayment", 25000);
		RequestParams.put("requestId", requestId);
		
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointRequestPayment);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		JSONObject tempData = new JSONObject();
		tempData = (JSONObject) json.get("transactionHistory");
		
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		String responseisCompleted = tempData.get("isCompleted").toString();
		String responseTransactionHistoryId = tempData.get("id").toString();
		
		logger.info("Response body : " + responseBody);
		logger.info("Response status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		logger.info("Response isCompleted : " + responseisCompleted);
		
		// ASSERTION
		Assert.assertEquals(responseDescription, responseStatusSuccess);
		Assert.assertEquals(responseStatus, "300");
		Assert.assertEquals(responseisCompleted, "false");
		Assert.assertEquals(responseTransactionHistoryId, transactionHistoryId);
		
		
		Thread.sleep(5);
		logger.info("[ --- Finished RequestPayment_TSC_004 --- ]");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void RequestPayment_TSC_005()throws InterruptedException, ParseException{
//		User already registered
//		User Logged in
//		Merchant exist
//		Total payment valid for success transaction
//		request Id is null
		logger.info("[ --- Started RequestPayment_TSC_005 --- ]");	
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("merchantId", 1);
		RequestParams.put("merchantName", "Fremilt");
		RequestParams.put("totalPayment", 25000);
		RequestParams.put("requestId", null);
		
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointRequestPayment);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Response Body : " + responseBody);
		logger.info("Response Status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION
		Assert.assertEquals(responseDescription, "RequestBody attributes shouldn't be null");
		Assert.assertEquals(responseStatus, "106");
		
		Thread.sleep(5);
		logger.info("[ --- Finished RequestPayment_TSC_005 --- ]");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void RequestPayment_TSC_006()throws InterruptedException, ParseException{
//		User already registered
//		User Logged in
//		Merchant exist
//		Total payment invalid
//		request Id is null
		logger.info("[ --- Started RequestPayment_TSC_006 --- ]");	
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("merchantId", 1);
		RequestParams.put("merchantName", "Fremilt");
		RequestParams.put("totalPayment", -15000);
		RequestParams.put("requestId", generateRequestId());
		
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointRequestPayment);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Response Body : " + responseBody);
		logger.info("Response Status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION
		Assert.assertEquals(responseDescription, "Total payment under minimum");
		Assert.assertEquals(responseStatus, "109");
		
		Thread.sleep(5);
		logger.info("[ --- Finished RequestPayment_TSC_006 --- ]");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void RequestPayment_TSC_007()throws InterruptedException, ParseException{
//		User already registered
//		User Logged in
//		Merchant exist
//		Total payment invalid
//		request Id is null
		logger.info("[ --- Started RequestPayment_TSC_007 --- ]");	
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("merchantId", 1);
		RequestParams.put("merchantName", "Fremilt");
		RequestParams.put("totalPayment", 0);
		RequestParams.put("requestId", generateRequestId());
		
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointRequestPayment);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Response Body : " + responseBody);
		logger.info("Response Status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION
		Assert.assertEquals(responseDescription, "Total payment under minimum");
		Assert.assertEquals(responseStatus, "109");
		
		Thread.sleep(5);
		logger.info("[ --- Finished RequestPayment_TSC_007 --- ]");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void RequestPayment_TSC_008()throws InterruptedException, ParseException{
//		User already registered
//		User Logged in
//		Merchant exist
//		Total payment invalid
//		request Id is filled but not unique
		logger.info("[ --- Started RequestPayment_TSC_008 --- ]");	
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("merchantId", 1);
		RequestParams.put("merchantName", "Fremilt");
		RequestParams.put("totalPayment", -15000);
		RequestParams.put("requestId",requestId);
		
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointRequestPayment);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		JSONObject tempData = new JSONObject();
		tempData = (JSONObject) json.get("transactionHistory");
		
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		String responseisCompleted = tempData.get("isCompleted").toString();
		String responseTransactionHistoryId = tempData.get("id").toString();
		
		logger.info("Response body : " + responseBody);
		logger.info("Response status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		logger.info("Response isCompleted : " + responseisCompleted);
		
		// ASSERTION
		Assert.assertEquals(responseDescription, responseStatusSuccess);
		Assert.assertEquals(responseStatus, "300");
		Assert.assertEquals(responseisCompleted, "false");
		Assert.assertEquals(responseTransactionHistoryId, transactionHistoryId);
		
		Thread.sleep(5);
		logger.info("[ --- Finished RequestPayment_TSC_008 --- ]");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void RequestPayment_TSC_009()throws InterruptedException, ParseException{
//		User already registered
//		User Logged in
//		Merchant exist
//		Total payment invalid
//		request Id is filled but not unique
		logger.info("[ --- Started RequestPayment_TSC_009 --- ]");	
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("merchantId", 1);
		RequestParams.put("merchantName", "Fremilt");
		RequestParams.put("totalPayment", 0);
		RequestParams.put("requestId",requestId);
		
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointRequestPayment);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		JSONObject tempData = new JSONObject();
		tempData = (JSONObject) json.get("transactionHistory");
		
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		String responseisCompleted = tempData.get("isCompleted").toString();
		String responseTransactionHistoryId = tempData.get("id").toString();
		
		logger.info("Response body : " + responseBody);
		logger.info("Response status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		logger.info("Response isCompleted : " + responseisCompleted);
		
		// ASSERTION
		Assert.assertEquals(responseDescription, responseStatusSuccess);
		Assert.assertEquals(responseStatus, "300");
		Assert.assertEquals(responseisCompleted, "false");
		Assert.assertEquals(responseTransactionHistoryId, transactionHistoryId);
		
		Thread.sleep(5);
		logger.info("[ --- Finished RequestPayment_TSC_009 --- ]");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void RequestPayment_TSC_010()throws InterruptedException, ParseException{
//		User already registered
//		User Logged in
//		Merchant exist
//		Total payment invalid
//		request Id is null
		logger.info("[ --- Started RequestPayment_TSC_010 --- ]");	
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("merchantId", 1);
		RequestParams.put("merchantName", "Fremilt");
		RequestParams.put("totalPayment", -15000);
		RequestParams.put("requestId", null);
		
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointRequestPayment);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Response Body : " + responseBody);
		logger.info("Response Status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION
		Assert.assertEquals(responseDescription, "RequestBody attributes shouldn't be null");
		Assert.assertEquals(responseStatus, "106");
		
		Thread.sleep(5);
		logger.info("[ --- Finished RequestPayment_TSC_010 --- ]");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void RequestPayment_TSC_011()throws InterruptedException, ParseException{
//		User already registered
//		User Logged in
//		Merchant exist
//		Total payment invalid
//		request Id is null
		logger.info("[ --- Started RequestPayment_TSC_011 --- ]");	
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("merchantId", 1);
		RequestParams.put("merchantName", "Fremilt");
		RequestParams.put("totalPayment", 0);
		RequestParams.put("requestId", null);
		
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointRequestPayment);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Response Body : " + responseBody);
		logger.info("Response Status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION
		Assert.assertEquals(responseDescription, "RequestBody attributes shouldn't be null");
		Assert.assertEquals(responseStatus, "106");
		
		Thread.sleep(5);
		logger.info("[ --- Finished RequestPayment_TSC_011 --- ]");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void RequestPayment_TSC_012()throws InterruptedException, ParseException{
//		User already registered
//		User Logged in
//		Merchant not exist
//		Total payment valid for success
//		request Id is filled and unique
		logger.info("[ --- Started RequestPayment_TSC_012 --- ]");	
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("merchantId", "a");
		RequestParams.put("merchantName", "Fremilt");
		RequestParams.put("totalPayment", 25000);
		RequestParams.put("requestId", generateRequestId());
		
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointRequestPayment);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("status").toString();
		String responseDescription = json.get("error").toString();
		
		logger.info("Response Body : " + responseBody);
		logger.info("Response Status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION >> bad request
		Assert.assertEquals(responseStatus, "400");
		Assert.assertEquals(responseDescription, "Bad Request");
		
		Thread.sleep(5);
		logger.info("[ --- Finished RequestPayment_TSC_012 --- ]");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void RequestPayment_TSC_013()throws InterruptedException, ParseException{
//		User already registered
//		User Logged in
//		Merchant not exist
//		Total payment valid for refund
//		request Id is filled and unique
		logger.info("[ --- Started RequestPayment_TSC_013 --- ]");	
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("merchantId", "a");
		RequestParams.put("merchantName", "Fremilt");
		RequestParams.put("totalPayment", 1000);
		RequestParams.put("requestId", generateRequestId());
		
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointRequestPayment);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("status").toString();
		String responseDescription = json.get("error").toString();
		
		logger.info("Response Body : " + responseBody);
		logger.info("Response Status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION >> bad request
		Assert.assertEquals(responseStatus, "400");
		Assert.assertEquals(responseDescription, "Bad Request");
		
		Thread.sleep(5);
		logger.info("[ --- Finished RequestPayment_TSC_013 --- ]");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void RequestPayment_TSC_014()throws InterruptedException, ParseException{
//		User already registered
//		User Logged in
//		Merchant not exist
//		Total payment valid for failed
//		request Id is filled and unique
		logger.info("[ --- Started RequestPayment_TSC_014 --- ]");	
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("merchantId", "a");
		RequestParams.put("merchantName", "Fremilt");
		RequestParams.put("totalPayment", 999);
		RequestParams.put("requestId", generateRequestId());
		
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointRequestPayment);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("status").toString();
		String responseDescription = json.get("error").toString();
		
		logger.info("Response Body : " + responseBody);
		logger.info("Response Status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION >> bad request
		Assert.assertEquals(responseStatus, "400");
		Assert.assertEquals(responseDescription, "Bad Request");
		
		Thread.sleep(5);
		logger.info("[ --- Finished RequestPayment_TSC_014 --- ]");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void RequestPayment_TSC_015()throws InterruptedException, ParseException{
//		User already registered
//		User Logged in
//		Merchant not exist
//		Total payment valid for success
//		request Id is filled but not unique
		logger.info("[ --- Started RequestPayment_TSC_015 --- ]");	
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("merchantId", "a");
		RequestParams.put("merchantName", "Fremilt");
		RequestParams.put("totalPayment", 25000);
		RequestParams.put("requestId", requestId);
		
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointRequestPayment);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("status").toString();
		String responseDescription = json.get("error").toString();
		
		logger.info("Response Body : " + responseBody);
		logger.info("Response Status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION >> bad request
		Assert.assertEquals(responseStatus, "400");
		Assert.assertEquals(responseDescription, "Bad Request");
		
		Thread.sleep(5);
		logger.info("[ --- Finished RequestPayment_TSC_015 --- ]");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void RequestPayment_TSC_016()throws InterruptedException, ParseException{
//		User already registered
//		User Logged in
//		Merchant not exist
//		Total payment valid for refund
//		request Id is filled but not unique
		logger.info("[ --- Started RequestPayment_TSC_016 --- ]");	
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("merchantId", "a");
		RequestParams.put("merchantName", "Fremilt");
		RequestParams.put("totalPayment", 1000);
		RequestParams.put("requestId", requestId);
		
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointRequestPayment);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("status").toString();
		String responseDescription = json.get("error").toString();
		
		logger.info("Response Body : " + responseBody);
		logger.info("Response Status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION >> bad request
		Assert.assertEquals(responseStatus, "400");
		Assert.assertEquals(responseDescription, "Bad Request");
		
		Thread.sleep(5);
		logger.info("[ --- Finished RequestPayment_TSC_016 --- ]");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void RequestPayment_TSC_017()throws InterruptedException, ParseException{
//		User already registered
//		User Logged in
//		Merchant not exist
//		Total payment valid for failed
//		request Id is filled but not unique
		logger.info("[ --- Started RequestPayment_TSC_017 --- ]");	
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("merchantId", "a");
		RequestParams.put("merchantName", "Fremilt");
		RequestParams.put("totalPayment", 999);
		RequestParams.put("requestId", requestId);
		
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointRequestPayment);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("status").toString();
		String responseDescription = json.get("error").toString();
		
		logger.info("Response Body : " + responseBody);
		logger.info("Response Status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION >> bad request
		Assert.assertEquals(responseStatus, "400");
		Assert.assertEquals(responseDescription, "Bad Request");
		
		Thread.sleep(5);
		logger.info("[ --- Finished RequestPayment_TSC_017 --- ]");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void RequestPayment_TSC_018()throws InterruptedException, ParseException{
//		User already registered
//		User Logged in
//		Merchant not exist
//		Total payment valid for success
//		request Id is null
		logger.info("[ --- Started RequestPayment_TSC_018 --- ]");	
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("merchantId", "a");
		RequestParams.put("merchantName", "Fremilt");
		RequestParams.put("totalPayment", 25000);
		RequestParams.put("requestId", null);
		
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointRequestPayment);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("status").toString();
		String responseDescription = json.get("error").toString();
		
		logger.info("Response Body : " + responseBody);
		logger.info("Response Status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION >> bad request
		Assert.assertEquals(responseStatus, "400");
		Assert.assertEquals(responseDescription, "Bad Request");
		
		Thread.sleep(5);
		logger.info("[ --- Finished RequestPayment_TSC_018 --- ]");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void RequestPayment_TSC_019()throws InterruptedException, ParseException{
//		User already registered
//		User Logged in
//		Merchant not exist
//		Total payment valid for refund
//		request Id is null
		logger.info("[ --- Started RequestPayment_TSC_019 --- ]");	
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("merchantId", "a");
		RequestParams.put("merchantName", "Fremilt");
		RequestParams.put("totalPayment", 1000);
		RequestParams.put("requestId", null);
		
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointRequestPayment);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("status").toString();
		String responseDescription = json.get("error").toString();
		
		logger.info("Response Body : " + responseBody);
		logger.info("Response Status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION >> bad request
		Assert.assertEquals(responseStatus, "400");
		Assert.assertEquals(responseDescription, "Bad Request");
		
		Thread.sleep(5);
		logger.info("[ --- Finished RequestPayment_TSC_019 --- ]");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void RequestPayment_TSC_020()throws InterruptedException, ParseException{
//		User already registered
//		User Logged in
//		Merchant not exist
//		Total payment valid for failed
//		request Id is null
		logger.info("[ --- Started RequestPayment_TSC_020 --- ]");	
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("merchantId", "a");
		RequestParams.put("merchantName", "Fremilt");
		RequestParams.put("totalPayment", 999);
		RequestParams.put("requestId", null);
		
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointRequestPayment);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("status").toString();
		String responseDescription = json.get("error").toString();
		
		logger.info("Response Body : " + responseBody);
		logger.info("Response Status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION >> bad request
		Assert.assertEquals(responseStatus, "400");
		Assert.assertEquals(responseDescription, "Bad Request");
		
		Thread.sleep(5);
		logger.info("[ --- Finished RequestPayment_TSC_020 --- ]");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void RequestPayment_TSC_021()throws InterruptedException, ParseException{
//		User already registered
//		User Logged in
//		Merchant not exist
//		Total payment invalid
//		request Id is filled and unique
		logger.info("[ --- Started RequestPayment_TSC_021 --- ]");	
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("merchantId", "a");
		RequestParams.put("merchantName", "Fremilt");
		RequestParams.put("totalPayment", -15000);
		RequestParams.put("requestId", generateRequestId());
		
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointRequestPayment);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("status").toString();
		String responseDescription = json.get("error").toString();
		
		logger.info("Response Body : " + responseBody);
		logger.info("Response Status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION >> bad request
		Assert.assertEquals(responseStatus, "400");
		Assert.assertEquals(responseDescription, "Bad Request");
		
		Thread.sleep(5);
		logger.info("[ --- Finished RequestPayment_TSC_021 --- ]");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void RequestPayment_TSC_022()throws InterruptedException, ParseException{
//		User already registered
//		User Logged in
//		Merchant not exist
//		Total payment invalid
//		request Id is filled and unique
		logger.info("[ --- Started RequestPayment_TSC_022 --- ]");	
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("merchantId", "a");
		RequestParams.put("merchantName", "Fremilt");
		RequestParams.put("totalPayment", 0);
		RequestParams.put("requestId", generateRequestId());
		
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointRequestPayment);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("status").toString();
		String responseDescription = json.get("error").toString();
		
		logger.info("Response Body : " + responseBody);
		logger.info("Response Status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION >> bad request
		Assert.assertEquals(responseStatus, "400");
		Assert.assertEquals(responseDescription, "Bad Request");
		
		Thread.sleep(5);
		logger.info("[ --- Finished RequestPayment_TSC_022 --- ]");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void RequestPayment_TSC_023()throws InterruptedException, ParseException{
//		User already registered
//		User Logged in
//		Merchant not exist
//		Total payment invalid
//		request Id is filled but not unique
		logger.info("[ --- Started RequestPayment_TSC_023 --- ]");	
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("merchantId", "a");
		RequestParams.put("merchantName", "Fremilt");
		RequestParams.put("totalPayment", -15000);
		RequestParams.put("requestId", requestId);
		
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointRequestPayment);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("status").toString();
		String responseDescription = json.get("error").toString();
		
		logger.info("Response Body : " + responseBody);
		logger.info("Response Status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION >> bad request
		Assert.assertEquals(responseStatus, "400");
		Assert.assertEquals(responseDescription, "Bad Request");
		
		Thread.sleep(5);
		logger.info("[ --- Finished RequestPayment_TSC_023 --- ]");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void RequestPayment_TSC_024()throws InterruptedException, ParseException{
//		User already registered
//		User Logged in
//		Merchant not exist
//		Total payment invalid
//		request Id is filled but not unique
		logger.info("[ --- Started RequestPayment_TSC_024 --- ]");	
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("merchantId", "a");
		RequestParams.put("merchantName", "Fremilt");
		RequestParams.put("totalPayment", 0);
		RequestParams.put("requestId", requestId);
		
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointRequestPayment);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("status").toString();
		String responseDescription = json.get("error").toString();
		
		logger.info("Response Body : " + responseBody);
		logger.info("Response Status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION >> bad request
		Assert.assertEquals(responseStatus, "400");
		Assert.assertEquals(responseDescription, "Bad Request");
		
		Thread.sleep(5);
		logger.info("[ --- Finished RequestPayment_TSC_024 --- ]");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void RequestPayment_TSC_025()throws InterruptedException, ParseException{
//		User already registered
//		User Logged in
//		Merchant not exist
//		Total payment invalid
//		request Id is null
		logger.info("[ --- Started RequestPayment_TSC_025 --- ]");	
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("merchantId", "a");
		RequestParams.put("merchantName", "Fremilt");
		RequestParams.put("totalPayment", -15000);
		RequestParams.put("requestId", null);
		
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointRequestPayment);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("status").toString();
		String responseDescription = json.get("error").toString();
		
		logger.info("Response Body : " + responseBody);
		logger.info("Response Status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION >> bad request
		Assert.assertEquals(responseStatus, "400");
		Assert.assertEquals(responseDescription, "Bad Request");
		
		Thread.sleep(5);
		logger.info("[ --- Finished RequestPayment_TSC_025 --- ]");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void RequestPayment_TSC_026()throws InterruptedException, ParseException{
//		User already registered
//		User Logged in
//		Merchant not exist
//		Total payment invalid
//		request Id is null
		logger.info("[ --- Started RequestPayment_TSC_026 --- ]");	
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("merchantId", "a");
		RequestParams.put("merchantName", "Fremilt");
		RequestParams.put("totalPayment", 0);
		RequestParams.put("requestId", null);
		
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointRequestPayment);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("status").toString();
		String responseDescription = json.get("error").toString();
		
		logger.info("Response Body : " + responseBody);
		logger.info("Response Status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION >> bad request
		Assert.assertEquals(responseStatus, "400");
		Assert.assertEquals(responseDescription, "Bad Request");
		
		Thread.sleep(5);
		logger.info("[ --- Finished RequestPayment_TSC_026 --- ]");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void RequestPayment_TSC_027()throws InterruptedException, ParseException{
//		User already registered
//		User not Logged in
//		Merchant exist
//		Total payment valid for success transaction
//		request Id is filled and unique
		logger.info("[ --- Started RequestPayment_TSC_027 --- ]");	
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("merchantId", 1);
		RequestParams.put("merchantName", "Fremilt");
		RequestParams.put("totalPayment", 25000);
		RequestParams.put("requestId", generateRequestId());
		
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointRequestPayment);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Response Body : " + responseBody);
		logger.info("Response Status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION
		Assert.assertEquals(responseDescription, "USER_NOT_LOGGED_IN");
		Assert.assertEquals(responseStatus, "301");
		
		Thread.sleep(5);
		logger.info("[ --- Finished RequestPayment_TSC_027 --- ]");
		
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void RequestPayment_TSC_028()throws InterruptedException, ParseException{
//		User already registered
//		User not Logged in
//		Merchant exist
//		Total payment valid for refund transaction
//		request Id is filled and unique
		logger.info("[ --- Started RequestPayment_TSC_028 --- ]");	
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("merchantId", 1);
		RequestParams.put("merchantName", "Fremilt");
		RequestParams.put("totalPayment", 1000);
		RequestParams.put("requestId", generateRequestId());
		
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointRequestPayment);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Response Body : " + responseBody);
		logger.info("Response Status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION
		Assert.assertEquals(responseDescription, "USER_NOT_LOGGED_IN");
		Assert.assertEquals(responseStatus, "301");
		
		Thread.sleep(5);
		logger.info("[ --- Finished RequestPayment_TSC_028 --- ]");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void RequestPayment_TSC_029()throws InterruptedException, ParseException{
//		User already registered
//		User not Logged in
//		Merchant exist
//		Total payment valid for failed transaction
//		request Id is filled and unique
		logger.info("[ --- Started RequestPayment_TSC_029 --- ]");	
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("merchantId", 1);
		RequestParams.put("merchantName", "Fremilt");
		RequestParams.put("totalPayment", 999);
		RequestParams.put("requestId", generateRequestId());
		
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointRequestPayment);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Response Body : " + responseBody);
		logger.info("Response Status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION
		Assert.assertEquals(responseDescription, "USER_NOT_LOGGED_IN");
		Assert.assertEquals(responseStatus, "301");
		
		Thread.sleep(5);
		logger.info("[ --- Finished RequestPayment_TSC_029 --- ]");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void RequestPayment_TSC_030()throws InterruptedException, ParseException{
//		User already registered
//		User not Logged in
//		Merchant exist
//		Total payment valid for success transaction
//		request Id is filled but not unique
		
		logger.info("[ --- Started RequestPayment_TSC_030 --- ]");	
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("merchantId", 1);
		RequestParams.put("merchantName", "Fremilt");
		RequestParams.put("totalPayment", 25000);
		RequestParams.put("requestId", requestIdtxt);
		
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointRequestPayment);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Response Body : " + responseBody);
		logger.info("Response Status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION
		Assert.assertEquals(responseDescription, "USER_NOT_LOGGED_IN");
		Assert.assertEquals(responseStatus, "301");
		
		Thread.sleep(5);
		logger.info("[ --- Finished RequestPayment_TSC_030 --- ]");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void RequestPayment_TSC_031()throws InterruptedException, ParseException{
//		User already registered
//		User not Logged in
//		Merchant exist
//		Total payment valid for refund transaction
//		request Id is filled but not unique
		logger.info("[ --- Started RequestPayment_TSC_031 --- ]");	
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("merchantId", 1);
		RequestParams.put("merchantName", "Fremilt");
		RequestParams.put("totalPayment", 1000);
		RequestParams.put("requestId", requestIdtxt);
		
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointRequestPayment);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Response Body : " + responseBody);
		logger.info("Response Status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION
		Assert.assertEquals(responseDescription, "USER_NOT_LOGGED_IN");
		Assert.assertEquals(responseStatus, "301");
		
		Thread.sleep(5);
		logger.info("[ --- Finished RequestPayment_TSC_031 --- ]");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void RequestPayment_TSC_032()throws InterruptedException, ParseException{
//		User already registered
//		User not Logged in
//		Merchant exist
//		Total payment valid for failed transaction
//		request Id is filled but not unique
		logger.info("[ --- Started RequestPayment_TSC_032 --- ]");	
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("merchantId", 1);
		RequestParams.put("merchantName", "Fremilt");
		RequestParams.put("totalPayment", 999);
		RequestParams.put("requestId", requestIdtxt);
		
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointRequestPayment);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Response Body : " + responseBody);
		logger.info("Response Status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION
		Assert.assertEquals(responseDescription, "USER_NOT_LOGGED_IN");
		Assert.assertEquals(responseStatus, "301");
		
		Thread.sleep(5);
		logger.info("[ --- Finished RequestPayment_TSC_032 --- ]");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void RequestPayment_TSC_033()throws InterruptedException, ParseException{
//		User already registered
//		User not Logged in
//		Merchant exist
//		Total payment valid for success transaction
//		request Id is null
		logger.info("[ --- Started RequestPayment_TSC_033 --- ]");	
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("merchantId", 1);
		RequestParams.put("merchantName", "Fremilt");
		RequestParams.put("totalPayment", 25000);
		RequestParams.put("requestId", null);
		
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointRequestPayment);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Response Body : " + responseBody);
		logger.info("Response Status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION
		Assert.assertEquals(responseDescription, "USER_NOT_LOGGED_IN");
		Assert.assertEquals(responseStatus, "301");
		
		Thread.sleep(5);
		logger.info("[ --- Finished RequestPayment_TSC_033 --- ]");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void RequestPayment_TSC_034()throws InterruptedException, ParseException{
//		User already registered
//		User not Logged in
//		Merchant exist
//		Total payment valid for refund transaction
//		request Id is null
		logger.info("[ --- Started RequestPayment_TSC_034 --- ]");	
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("merchantId", 1);
		RequestParams.put("merchantName", "Fremilt");
		RequestParams.put("totalPayment", 1000);
		RequestParams.put("requestId", null);
		
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointRequestPayment);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Response Body : " + responseBody);
		logger.info("Response Status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION
		Assert.assertEquals(responseDescription, "USER_NOT_LOGGED_IN");
		Assert.assertEquals(responseStatus, "301");
		
		Thread.sleep(5);
		logger.info("[ --- Finished RequestPayment_TSC_034 --- ]");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void RequestPayment_TSC_035()throws InterruptedException, ParseException{
//		User already registered
//		User not Logged in
//		Merchant exist
//		Total payment valid for failed transaction
//		request Id is null
		logger.info("[ --- Started RequestPayment_TSC_035 --- ]");	
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("merchantId", 1);
		RequestParams.put("merchantName", "Fremilt");
		RequestParams.put("totalPayment", 999);
		RequestParams.put("requestId", null);
		
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointRequestPayment);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Response Body : " + responseBody);
		logger.info("Response Status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION
		Assert.assertEquals(responseDescription, "USER_NOT_LOGGED_IN");
		Assert.assertEquals(responseStatus, "301");
		
		Thread.sleep(5);
		logger.info("[ --- Finished RequestPayment_TSC_035 --- ]");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void RequestPayment_TSC_036()throws InterruptedException, ParseException{
//		User already registered
//		User not Logged in
//		Merchant exist
//		Total payment invalid
//		request Id is filled and unique
		logger.info("[ --- Started RequestPayment_TSC_036 --- ]");	
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("merchantId", 1);
		RequestParams.put("merchantName", "Fremilt");
		RequestParams.put("totalPayment", -15000);
		RequestParams.put("requestId", generateRequestId());
		
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointRequestPayment);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Response Body : " + responseBody);
		logger.info("Response Status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION
		Assert.assertEquals(responseDescription, "USER_NOT_LOGGED_IN");
		Assert.assertEquals(responseStatus, "301");
		
		Thread.sleep(5);
		logger.info("[ --- Finished RequestPayment_TSC_036 --- ]");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void RequestPayment_TSC_037()throws InterruptedException, ParseException{
//		User already registered
//		User not Logged in
//		Merchant exist
//		Total payment invalid
//		request Id is filled and unique
		logger.info("[ --- Started RequestPayment_TSC_037 --- ]");	
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("merchantId", 1);
		RequestParams.put("merchantName", "Fremilt");
		RequestParams.put("totalPayment", 0);
		RequestParams.put("requestId", generateRequestId());
		
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointRequestPayment);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Response Body : " + responseBody);
		logger.info("Response Status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION
		Assert.assertEquals(responseDescription, "USER_NOT_LOGGED_IN");
		Assert.assertEquals(responseStatus, "301");
		
		Thread.sleep(5);
		logger.info("[ --- Finished RequestPayment_TSC_037 --- ]");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void RequestPayment_TSC_038()throws InterruptedException, ParseException{
//		User already registered
//		User not Logged in
//		Merchant exist
//		Total payment invalid
//		request Id is filled but not unique
		logger.info("[ --- Started RequestPayment_TSC_038 --- ]");	
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("merchantId", 1);
		RequestParams.put("merchantName", "Fremilt");
		RequestParams.put("totalPayment", -15000);
		RequestParams.put("requestId", requestIdtxt);
		
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointRequestPayment);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Response Body : " + responseBody);
		logger.info("Response Status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION
		Assert.assertEquals(responseDescription, "USER_NOT_LOGGED_IN");
		Assert.assertEquals(responseStatus, "301");
		
		Thread.sleep(5);
		logger.info("[ --- Finished RequestPayment_TSC_038 --- ]");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void RequestPayment_TSC_039()throws InterruptedException, ParseException{
//		User already registered
//		User not Logged in
//		Merchant exist
//		Total payment invalid
//		request Id is filled but not unique
		logger.info("[ --- Started RequestPayment_TSC_039 --- ]");	
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("merchantId", 1);
		RequestParams.put("merchantName", "Fremilt");
		RequestParams.put("totalPayment", 0);
		RequestParams.put("requestId", requestIdtxt);
		
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointRequestPayment);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Response Body : " + responseBody);
		logger.info("Response Status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION
		Assert.assertEquals(responseDescription, "USER_NOT_LOGGED_IN");
		Assert.assertEquals(responseStatus, "301");
		
		Thread.sleep(5);
		logger.info("[ --- Finished RequestPayment_TSC_039 --- ]");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void RequestPayment_TSC_040()throws InterruptedException, ParseException{
//		User already registered
//		User not Logged in
//		Merchant exist
//		Total payment invalid
//		request Id is null
		logger.info("[ --- Started RequestPayment_TSC_040 --- ]");	
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("merchantId", 1);
		RequestParams.put("merchantName", "Fremilt");
		RequestParams.put("totalPayment", -15000);
		RequestParams.put("requestId", null);
		
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointRequestPayment);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Response Body : " + responseBody);
		logger.info("Response Status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION
		Assert.assertEquals(responseDescription, "USER_NOT_LOGGED_IN");
		Assert.assertEquals(responseStatus, "301");
		
		Thread.sleep(5);
		logger.info("[ --- Finished RequestPayment_TSC_040 --- ]");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void RequestPayment_TSC_041()throws InterruptedException, ParseException{
//		User already registered
//		User not Logged in
//		Merchant exist
//		Total payment invalid
//		request Id is null
		logger.info("[ --- Started RequestPayment_TSC_041 --- ]");	
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("merchantId", 1);
		RequestParams.put("merchantName", "Fremilt");
		RequestParams.put("totalPayment", 0);
		RequestParams.put("requestId", null);
		
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointRequestPayment);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Response Body : " + responseBody);
		logger.info("Response Status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION
		Assert.assertEquals(responseDescription, "USER_NOT_LOGGED_IN");
		Assert.assertEquals(responseStatus, "301");
		
		Thread.sleep(5);
		logger.info("[ --- Finished RequestPayment_TSC_041 --- ]");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void RequestPayment_TSC_042()throws InterruptedException, ParseException{
//		User already registered
//		User not Logged in
//		Merchant not exist
//		Total payment valid for success transaction
//		request Id is filled and unique
		logger.info("[ --- Started RequestPayment_TSC_042 --- ]");	
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("merchantId", "a");
		RequestParams.put("merchantName", "Fremilt");
		RequestParams.put("totalPayment", 25000);
		RequestParams.put("requestId", generateRequestId());
		
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointRequestPayment);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("status").toString();
		String responseDescription = json.get("error").toString();
		
		logger.info("Response Body : " + responseBody);
		logger.info("Response Status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION >> bad request
		Assert.assertEquals(responseStatus, "400");
		Assert.assertEquals(responseDescription, "Bad Request");
		
		Thread.sleep(5);
		logger.info("[ --- Finished RequestPayment_TSC_042 --- ]");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void RequestPayment_TSC_043()throws InterruptedException, ParseException{
//		User already registered
//		User not Logged in
//		Merchant not exist
//		Total payment valid for refund transaction
//		request Id is filled and unique
		logger.info("[ --- Started RequestPayment_TSC_043 --- ]");	
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("merchantId", "a");
		RequestParams.put("merchantName", "Fremilt");
		RequestParams.put("totalPayment", 1000);
		RequestParams.put("requestId", generateRequestId());
		
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointRequestPayment);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("status").toString();
		String responseDescription = json.get("error").toString();
		
		logger.info("Response Body : " + responseBody);
		logger.info("Response Status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION >> bad request
		Assert.assertEquals(responseStatus, "400");
		Assert.assertEquals(responseDescription, "Bad Request");
		
		Thread.sleep(5);
		logger.info("[ --- Finished RequestPayment_TSC_043 --- ]");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void RequestPayment_TSC_044()throws InterruptedException, ParseException{
//		User already registered
//		User not Logged in
//		Merchant not exist
//		Total payment valid for failed transaction
//		request Id is filled and unique
		logger.info("[ --- Started RequestPayment_TSC_044 --- ]");	
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("merchantId", "a");
		RequestParams.put("merchantName", "Fremilt");
		RequestParams.put("totalPayment", 1000);
		RequestParams.put("requestId", generateRequestId());
		
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointRequestPayment);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("status").toString();
		String responseDescription = json.get("error").toString();
		
		logger.info("Response Body : " + responseBody);
		logger.info("Response Status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION >> bad request
		Assert.assertEquals(responseStatus, "400");
		Assert.assertEquals(responseDescription, "Bad Request");
		
		Thread.sleep(5);
		logger.info("[ --- Finished RequestPayment_TSC_044 --- ]");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void RequestPayment_TSC_045()throws InterruptedException, ParseException{
//		User already registered
//		User not Logged in
//		Merchant not exist
//		Total payment valid for success transaction
//		request Id is filled but not unique
		logger.info("[ --- Started RequestPayment_TSC_045 --- ]");	
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("merchantId", "a");
		RequestParams.put("merchantName", "Fremilt");
		RequestParams.put("totalPayment", 25000);
		RequestParams.put("requestId", requestIdtxt);
		
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointRequestPayment);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("status").toString();
		String responseDescription = json.get("error").toString();
		
		logger.info("Response Body : " + responseBody);
		logger.info("Response Status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION >> bad request
		Assert.assertEquals(responseStatus, "400");
		Assert.assertEquals(responseDescription, "Bad Request");
		
		Thread.sleep(5);
		logger.info("[ --- Finished RequestPayment_TSC_045 --- ]");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void RequestPayment_TSC_046()throws InterruptedException, ParseException{
//		User already registered
//		User not Logged in
//		Merchant not exist
//		Total payment valid for refund transaction
//		request Id is filled but not unique
		logger.info("[ --- Started RequestPayment_TSC_046 --- ]");	
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("merchantId", "a");
		RequestParams.put("merchantName", "Fremilt");
		RequestParams.put("totalPayment", 1000);
		RequestParams.put("requestId", requestIdtxt);
		
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointRequestPayment);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("status").toString();
		String responseDescription = json.get("error").toString();
		
		logger.info("Response Body : " + responseBody);
		logger.info("Response Status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION >> bad request
		Assert.assertEquals(responseStatus, "400");
		Assert.assertEquals(responseDescription, "Bad Request");
		
		Thread.sleep(5);
		logger.info("[ --- Finished RequestPayment_TSC_046 --- ]");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void RequestPayment_TSC_047()throws InterruptedException, ParseException{
//		User already registered
//		User not Logged in
//		Merchant not exist
//		Total payment valid for failed transaction
//		request Id is filled but not unique
		logger.info("[ --- Started RequestPayment_TSC_047 --- ]");	
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("merchantId", "a");
		RequestParams.put("merchantName", "Fremilt");
		RequestParams.put("totalPayment", 999);
		RequestParams.put("requestId", requestIdtxt);
		
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointRequestPayment);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("status").toString();
		String responseDescription = json.get("error").toString();
		
		logger.info("Response Body : " + responseBody);
		logger.info("Response Status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION >> bad request
		Assert.assertEquals(responseStatus, "400");
		Assert.assertEquals(responseDescription, "Bad Request");
		
		Thread.sleep(5);
		logger.info("[ --- Finished RequestPayment_TSC_047 --- ]");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void RequestPayment_TSC_048()throws InterruptedException, ParseException{
//		User already registered
//		User not Logged in
//		Merchant not exist
//		Total payment valid for success transaction
//		request Id is null
		logger.info("[ --- Started RequestPayment_TSC_048 --- ]");	
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("merchantId", "a");
		RequestParams.put("merchantName", "Fremilt");
		RequestParams.put("totalPayment", 25000);
		RequestParams.put("requestId", null);
		
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointRequestPayment);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("status").toString();
		String responseDescription = json.get("error").toString();
		
		logger.info("Response Body : " + responseBody);
		logger.info("Response Status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION >> bad request
		Assert.assertEquals(responseStatus, "400");
		Assert.assertEquals(responseDescription, "Bad Request");
		
		Thread.sleep(5);
		logger.info("[ --- Finished RequestPayment_TSC_048 --- ]");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void RequestPayment_TSC_049()throws InterruptedException, ParseException{
//		User already registered
//		User not Logged in
//		Merchant not exist
//		Total payment valid for refund transaction
//		request Id is null
		logger.info("[ --- Started RequestPayment_TSC_049 --- ]");	
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("merchantId", "a");
		RequestParams.put("merchantName", "Fremilt");
		RequestParams.put("totalPayment", 1000);
		RequestParams.put("requestId", null);
		
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointRequestPayment);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("status").toString();
		String responseDescription = json.get("error").toString();
		
		logger.info("Response Body : " + responseBody);
		logger.info("Response Status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION >> bad request
		Assert.assertEquals(responseStatus, "400");
		Assert.assertEquals(responseDescription, "Bad Request");
		
		Thread.sleep(5);
		logger.info("[ --- Finished RequestPayment_TSC_049 --- ]");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void RequestPayment_TSC_050()throws InterruptedException, ParseException{
//		User already registered
//		User not Logged in
//		Merchant not exist
//		Total payment valid for failed transaction
//		request Id is null
		logger.info("[ --- Started RequestPayment_TSC_050 --- ]");	
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("merchantId", "a");
		RequestParams.put("merchantName", "Fremilt");
		RequestParams.put("totalPayment", 999);
		RequestParams.put("requestId", null);
		
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointRequestPayment);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("status").toString();
		String responseDescription = json.get("error").toString();
		
		logger.info("Response Body : " + responseBody);
		logger.info("Response Status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION >> bad request
		Assert.assertEquals(responseStatus, "400");
		Assert.assertEquals(responseDescription, "Bad Request");
		
		Thread.sleep(5);
		logger.info("[ --- Finished RequestPayment_TSC_050 --- ]");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void RequestPayment_TSC_051()throws InterruptedException, ParseException{
//		User already registered
//		User not Logged in
//		Merchant not exist
//		Total payment invalid
//		request Id is filled and unique
		logger.info("[ --- Started RequestPayment_TSC_051 --- ]");	
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("merchantId", "a");
		RequestParams.put("merchantName", "Fremilt");
		RequestParams.put("totalPayment", -15000);
		RequestParams.put("requestId", generateRequestId());
		
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointRequestPayment);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("status").toString();
		String responseDescription = json.get("error").toString();
		
		logger.info("Response Body : " + responseBody);
		logger.info("Response Status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION >> bad request
		Assert.assertEquals(responseStatus, "400");
		Assert.assertEquals(responseDescription, "Bad Request");
		
		Thread.sleep(5);
		logger.info("[ --- Finished RequestPayment_TSC_051 --- ]");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void RequestPayment_TSC_052()throws InterruptedException, ParseException{
//		User already registered
//		User not Logged in
//		Merchant not exist
//		Total payment invalid
//		request Id is filled and unique
		logger.info("[ --- Started RequestPayment_TSC_052 --- ]");	
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("merchantId", "a");
		RequestParams.put("merchantName", "Fremilt");
		RequestParams.put("totalPayment", 0);
		RequestParams.put("requestId", generateRequestId());
		
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointRequestPayment);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("status").toString();
		String responseDescription = json.get("error").toString();
		
		logger.info("Response Body : " + responseBody);
		logger.info("Response Status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION >> bad request
		Assert.assertEquals(responseStatus, "400");
		Assert.assertEquals(responseDescription, "Bad Request");
		
		Thread.sleep(5);
		logger.info("[ --- Finished RequestPayment_TSC_052 --- ]");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void RequestPayment_TSC_053()throws InterruptedException, ParseException{
//		User already registered
//		User not Logged in
//		Merchant not exist
//		Total payment invalid
//		request Id is filled but not unique
		logger.info("[ --- Started RequestPayment_TSC_053 --- ]");	
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("merchantId", "a");
		RequestParams.put("merchantName", "Fremilt");
		RequestParams.put("totalPayment", -15000);
		RequestParams.put("requestId", requestIdtxt);
		
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointRequestPayment);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("status").toString();
		String responseDescription = json.get("error").toString();
		
		logger.info("Response Body : " + responseBody);
		logger.info("Response Status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION >> bad request
		Assert.assertEquals(responseStatus, "400");
		Assert.assertEquals(responseDescription, "Bad Request");
		
		Thread.sleep(5);
		logger.info("[ --- Finished RequestPayment_TSC_053 --- ]");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void RequestPayment_TSC_054()throws InterruptedException, ParseException{
//		User already registered
//		User not Logged in
//		Merchant not exist
//		Total payment invalid
//		request Id is filled but not unique
		logger.info("[ --- Started RequestPayment_TSC_054 --- ]");	
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("merchantId", "a");
		RequestParams.put("merchantName", "Fremilt");
		RequestParams.put("totalPayment", 0);
		RequestParams.put("requestId", requestIdtxt);
		
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointRequestPayment);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("status").toString();
		String responseDescription = json.get("error").toString();
		
		logger.info("Response Body : " + responseBody);
		logger.info("Response Status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION >> bad request
		Assert.assertEquals(responseStatus, "400");
		Assert.assertEquals(responseDescription, "Bad Request");
		
		Thread.sleep(5);
		logger.info("[ --- Finished RequestPayment_TSC_054 --- ]");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void RequestPayment_TSC_055()throws InterruptedException, ParseException{
//		User already registered
//		User not Logged in
//		Merchant not exist
//		Total payment invalid
//		request Id is null
		logger.info("[ --- Started RequestPayment_TSC_055 --- ]");	
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("merchantId", "a");
		RequestParams.put("merchantName", "Fremilt");
		RequestParams.put("totalPayment", -15000);
		RequestParams.put("requestId", null);
		
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointRequestPayment);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("status").toString();
		String responseDescription = json.get("error").toString();
		
		logger.info("Response Body : " + responseBody);
		logger.info("Response Status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION >> bad request
		Assert.assertEquals(responseStatus, "400");
		Assert.assertEquals(responseDescription, "Bad Request");
		
		Thread.sleep(5);
		logger.info("[ --- Finished RequestPayment_TSC_055 --- ]");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void RequestPayment_TSC_056()throws InterruptedException, ParseException{
//		User already registered
//		User not Logged in
//		Merchant not exist
//		Total payment invalid
//		request Id is null
		logger.info("[ --- Started RequestPayment_TSC_056 --- ]");	
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("merchantId", "a");
		RequestParams.put("merchantName", "Fremilt");
		RequestParams.put("totalPayment", 0);
		RequestParams.put("requestId", null);
		
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointRequestPayment);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		String responseBody = response.getBody().asString();
		String responseStatus = json.get("status").toString();
		String responseDescription = json.get("error").toString();
		
		logger.info("Response Body : " + responseBody);
		logger.info("Response Status : " + responseStatus);
		logger.info("Response Description : " + responseDescription);
		
		// ASSERTION >> bad request
		Assert.assertEquals(responseStatus, "400");
		Assert.assertEquals(responseDescription, "Bad Request");
		
		Thread.sleep(5);
		logger.info("[ --- Finished RequestPayment_TSC_056 --- ]");
	}
	
	@AfterClass
	void tearDown() {
		logger.info("[ --- Finished TestSuite_RequestPayment --- ]");
		System.out.println();
	}

}
