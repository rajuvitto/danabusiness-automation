package testCases;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import testBase.TestBase;

public class TestSuite_ForgotPassword extends TestBase{
	final static Logger logger = Logger.getLogger(TestSuite_HistoryTransaction.class);
	String baseUrl = "";
	String responseStatusSuccess ="";
	String responseStatusFailed = "";
	String endpointForgotPassword = "/member/user/forgotpassword";
	String endpointChangePassword = "/member/user/resetpassword";
	String endpointDBProfile = "/user/viewProfile";
	long userId;
	String sessionId = "";
	File getUserId, getSessionId, getUserEmail;
	Scanner userIdReader, sessionIdReader, userEmailReader;
	String userEmail = "";
	
	
	@BeforeClass
	@Parameters({"baseUrl", "responseStatusSuccess", "responseStatusFailed"})
	void startTest(String definedBaseUrl, String definedResponseStatusSuccess, String definedResponseStatusFailed) {
		baseUrl = definedBaseUrl;
		responseStatusSuccess = definedResponseStatusSuccess;
		responseStatusFailed = definedResponseStatusFailed;
		logger.info("[ --- Started TestSuite_ForgotPassword --- ]");
		
		try {
			getUserId = new File("./assets/generated-data/userId.txt");
			getSessionId = new File("./assets/generated-data/sessionId.txt");
			getUserEmail = new File("./assets/generated-data/userEmail.txt");
			userIdReader = new Scanner(getUserId);
			sessionIdReader = new Scanner(getSessionId);
			userEmailReader = new Scanner(getUserEmail);
			
			while (userIdReader.hasNextLine()) {
				String data = userIdReader.nextLine();
				userId = Long.parseLong(data);
			}
			
			while (sessionIdReader.hasNextLine()) {
				String data = sessionIdReader.nextLine();
				sessionId = data;
			}
			
			while (userEmailReader.hasNextLine()) {
				String data = userEmailReader.nextLine();
				userEmail = data;
			}
		}catch (FileNotFoundException e) {
			System.out.println("An error occurred.");
		      e.printStackTrace();
		}
	}
	
	@SuppressWarnings("unchecked")
	@Parameters({"testCaseId"})
	@Test
	void ForgotPassword_TSC_001_004_005_006(String testId) throws InterruptedException, ParseException{
//		- Send valid & exist email to server
		
		logger.info("[ --- Started ForgotPassword_TSC_"+testId+" --- ]");	
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		if(testId.equals("001")) {
			RequestParams.put("email", userEmail);
		}else
		if(testId.equals("004")) {
			RequestParams.put("email", "nisarwd@dundermiflin.com");
		}else
		if(testId.equals("005")) {
			RequestParams.put("email", "caramiadundermiflin.com");
		}else
		if(testId.equals("006")) {
			RequestParams.put("email", null);
		}		
		
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointForgotPassword);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		String responseHeader = response.getHeader("Set-Cookie");
		String responseBody = response.getBody().asString();
		String responseCode = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		
		logger.info("Response Body : " + responseBody);
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		
		if(responseCode.equals("300")) {
			
			String responseUserId = json.get("userId").toString();
			String cookieId = between(responseHeader, "=", "; Path=/; Secure; HttpOnly");
			
			try {
                FileWriter generateSessionId = new FileWriter("assets/generated-data/sessionId.txt");
                generateSessionId.write(cookieId);
                generateSessionId.close();
            } catch (IOException e) {
                System.err.format("IOException: %s%n", e);
            }
			
			// ASSERTION
			Assert.assertEquals(responseDescription, "Success");
			Assert.assertEquals(responseCode.toString(), "300");
			Assert.assertEquals(responseUserId.toString(),  String.valueOf(userId));
			
			RestAssured.baseURI = "https://dana-bussiness-member.herokuapp.com/v1";
			httpRequest = RestAssured.given();
			
			// PROVIDE JSON DATA AT REQUEST BODY
			RequestParams = new JSONObject();
			RequestParams.put("userId", userId);
			
			// SET COOKIE, REQUEST HEADER AND REQUEST BODY
			httpRequest.params("id", String.valueOf(userId));
			httpRequest.header("Content-Type", "application/json");
			httpRequest.body(RequestParams.toJSONString());
			
			// SET REQUEST METHOD AND API ENDPOINT
			response = httpRequest.request(Method.GET, endpointDBProfile);
			
			// MANIPULATING RESPONSE DATA
			parser = new JSONParser();
			json = (JSONObject) parser.parse(response.getBody().asString());
			
			JSONObject tempData = new JSONObject();
			tempData = (JSONObject) json.get("userData");
			String email = tempData.get("email").toString();
			
			Assert.assertEquals(email, userEmail);
			
		}else
		if(responseCode.equals("201")) {
			
			// ASSERTION
			Assert.assertEquals(responseDescription, "User not found");
			Assert.assertEquals(responseCode.toString(), "201");
						
		}

		Thread.sleep(5);
		logger.info("[ --- Finished ForgotPassword_TSC_"+testId+" --- ]");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void ForgotPassword_TSC_002() throws InterruptedException, ParseException{
//		- Send valid & exist email to server
		
		logger.info("[ --- Started ForgotPassword_TSC_002 --- ]");
		
		try {
			getSessionId = new File("./assets/generated-data/sessionId.txt");
			sessionIdReader = new Scanner(getSessionId);
			
			while (sessionIdReader.hasNextLine()) {
				String data = sessionIdReader.nextLine();
				sessionId = data;
			}
		}catch (FileNotFoundException e) {
			System.out.println("An error occurred.");
		      e.printStackTrace();
		}
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
		
		RequestParams.put("newPassword", "Password!123");	
		
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointChangePassword);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		
		String responseBody = response.getBody().asString();
		String responseCode = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Response Body : " + responseBody);
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		
		if(responseCode.equals("300")) {
			try {
                FileWriter generateSessionId = new FileWriter("assets/generated-data/userPassword.txt");
                generateSessionId.write("Password!123");
                generateSessionId.close();
            } catch (IOException e) {
                System.err.format("IOException: %s%n", e);
            }
			
			// ASSERTION
			Assert.assertEquals(responseDescription, "Success");
			Assert.assertEquals(responseCode.toString(), "300");
			
			RestAssured.baseURI = "https://dana-bussiness-member.herokuapp.com/v1";
			httpRequest = RestAssured.given();
			
			// PROVIDE JSON DATA AT REQUEST BODY
			RequestParams = new JSONObject();
			RequestParams.put("userId", userId);
			
			// SET COOKIE, REQUEST HEADER AND REQUEST BODY
			httpRequest.params("id", String.valueOf(userId));
			httpRequest.header("Content-Type", "application/json");
			httpRequest.body(RequestParams.toJSONString());
			
			// SET REQUEST METHOD AND API ENDPOINT
			response = httpRequest.request(Method.GET, endpointDBProfile);
			
			// MANIPULATING RESPONSE DATA
			parser = new JSONParser();
			json = (JSONObject) parser.parse(response.getBody().asString());
			
			JSONObject tempData = new JSONObject();
			tempData = (JSONObject) json.get("userData");
			String password = tempData.get("password").toString();
			
			Assert.assertEquals(password, "Password!123");
			
		}

		Thread.sleep(5);
		logger.info("[ --- Finished ForgotPassword_TSC_002 --- ]");
	}
	
	@AfterClass
	void tearDown() {
		logger.info("[ --- Finished TestSuite_ForgotPassword --- ]");
		System.out.println();
	}
}
