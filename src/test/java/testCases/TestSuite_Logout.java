package testCases;

import java.io.FileNotFoundException;
import java.util.Scanner;
import java.io.File;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import io.restassured.RestAssured;
import io.restassured.http.Method;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import testBase.TestBase;

public class TestSuite_Logout extends TestBase {
	final static Logger logger = Logger.getLogger(TestSuite_Logout.class);
	String baseUrl = "";
	String responseStatusSuccess ="";
	String responseStatusFailed = "";
	String endpointLogout = "/member/user/logout";
	long userId;
	String sessionId = "";
	File getUserId, getSessionId;
	Scanner userIdReader, sessionIdReader;
	
	
	@BeforeClass
	@Parameters({"baseUrl", "responseStatusSuccess", "responseStatusFailed"})
	void startTest(String definedBaseUrl, String definedResponseStatusSuccess, String definedResponseStatusFailed) {
		baseUrl = definedBaseUrl;
		responseStatusSuccess = definedResponseStatusSuccess;
		responseStatusFailed = definedResponseStatusFailed;
		logger.info("[ --- Started TestSuite_Logout --- ]");
		
		try {
			getUserId = new File("./assets/generated-data/userId.txt");
			getSessionId = new File("./assets/generated-data/sessionId.txt");
			userIdReader = new Scanner(getUserId);
			sessionIdReader = new Scanner(getSessionId);
			
			while (userIdReader.hasNextLine()) {
				String data = userIdReader.nextLine();
				userId = Long.parseLong(data);
			}
			
			while (sessionIdReader.hasNextLine()) {
				String data = sessionIdReader.nextLine();
				sessionId = data;
			}
		}catch (FileNotFoundException e) {
			System.out.println("An error occurred.");
		      e.printStackTrace();
		}
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void Logout_TSC_001() throws InterruptedException, ParseException{
//		- User already registered
//		- User logged in
		
		logger.info("[ --- Started Logout_TSC_001 --- ]");	
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
	
		logger.info("Logged out user with ID : " + userId + " and JSESSION ID " + sessionId);
		
		RequestParams.put("userId", userId);
		
		httpRequest.cookie("JSESSIONID", sessionId);
		
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointLogout);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		String responseHeader = response.getHeader("Set-Cookie");
		String responseBody = response.getBody().asString();
		logger.info("Response Body : " + responseBody);
		
		String responseCode = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Response Header : " + responseHeader);
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		
		
		// ASSERTION
		Assert.assertEquals(responseDescription, "SUCCESS");
		Assert.assertEquals(responseCode.toString(), "300");
		
		if(responseCode.toString().equals("300")) {	
			logger.info("LOGOUT SUCCESS");
		}else {
			logger.info("LOGOUT FAILED");
		}	

		Thread.sleep(5);
		logger.info("[ --- Finished Logout_TSC_001 --- ]");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void Logout_TSC_002() throws InterruptedException, ParseException{
//		- User already registered
//		- User logged in
//		- Not use session id
		
		logger.info("[ --- Started Logout_TSC_002 --- ]");	
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
	
		logger.info("Logged out user with ID : " + userId);
		
		RequestParams.put("userId", userId);
		
		httpRequest.cookie("JSESSIONID", sessionId);
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointLogout);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		String responseBody = response.getBody().asString();
		logger.info("Response Body : " + responseBody);
		
		String responseCode = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		
		
		// ASSERTION
		Assert.assertEquals(responseDescription, "USER_NOT_LOGGED_IN");
		Assert.assertEquals(responseCode.toString(), "301");

		Thread.sleep(5);
		logger.info("[ --- Finished Logout_TSC_002 --- ]");
	}
	
	@SuppressWarnings("unchecked")
	@Test
	void Logout_TSC_003() throws InterruptedException, ParseException{
//		- User already registered
//		- User logged in
//		- Not use session id
		
		logger.info("[ --- Started Logout_TSC_003 --- ]");	
		
		RestAssured.baseURI = baseUrl;
		httpRequest = RestAssured.given();
		
		JSONObject RequestParams = new JSONObject();
	
		logger.info("Logged out user with ID : " + userId);
		
		RequestParams.put("userId", userId);
		
		
		httpRequest.header("Content-Type", "application/json");
		httpRequest.body(RequestParams.toJSONString());
		response = httpRequest.request(Method.POST, endpointLogout);
		
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response.getBody().asString());
		String responseBody = response.getBody().asString();
		logger.info("Response Body : " + responseBody);
		
		String responseCode = json.get("code").toString();
		String responseDescription = json.get("description").toString();
		
		logger.info("Response code : " + responseCode);
		logger.info("Response Description : " + responseDescription);
		
		
		// ASSERTION
		Assert.assertEquals(responseDescription, "USER_NOT_LOGGED_IN");
		Assert.assertEquals(responseCode.toString(), "301");

		Thread.sleep(5);
		logger.info("[ --- Finished Logout_TSC_003 --- ]");
	}
	
	@AfterClass
	void tearDown() {
		logger.info("[ --- Finished TestSuite_Logout --- ]");
		System.out.println();
	}
}