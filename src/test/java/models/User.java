package models;

public class User {
	private final String name;
	private final String password;
	private final String confirmPassword;
	private final String phone;
	private final String email;
	
	private User(UserBuilder builder) {
		this.name = builder.name;
		this.password = builder.password;
		this.confirmPassword = builder.confirmPassword;
		this.phone = builder.phone;
		this.email = builder.email;
    }

	public String getName() {
		return name;
	}

	public String getPassword() {
		return password;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public String getPhone() {
		return phone;
	}

	public String getEmail() {
		return email;
	}
	 
	public static class UserBuilder 
    {
		private String name;
		private String password;
		private String confirmPassword;
		private String phone;
		private String email;
     
        public UserBuilder name(String name) {
            this.name = name;
            return this;
        }
        
        public UserBuilder password(String password) {
            this.password = password;
            return this;
        }
        
        public UserBuilder confirmPassword(String confirmPassword) {
            this.confirmPassword = confirmPassword;
            return this;
        }
        
        public UserBuilder phone(String phone) {
            this.phone = phone;
            return this;
        }
        
        public UserBuilder email(String email) {
            this.email = email;
            return this;
        }
       
        public User build() {
            User user =  new User(this);
            return user;
        }
    }
}

